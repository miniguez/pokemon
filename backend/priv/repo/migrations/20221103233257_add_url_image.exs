defmodule App.Repo.Migrations.AddUrlImage do
  use Ecto.Migration

  def change do
    alter table("pokemons") do
      add :url_image, :string
    end
  end
end
