defmodule App.Repo.Migrations.CreatePokemons do
  use Ecto.Migration

  def change do
    create table(:pokemons) do
      add :name, :string
      add :weight, :integer
      add :height, :integer
      add :abilities, :integer
      add :chosen, :boolean, default: false, null: false
      add :url, :string

      timestamps()
    end
  end
end
