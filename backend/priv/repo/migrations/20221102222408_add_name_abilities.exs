defmodule App.Repo.Migrations.AddNameAbilities do
  use Ecto.Migration

  def change do
    alter table("pokemons") do
      add :name_abilities, :string
    end
  end
end
