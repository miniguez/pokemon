defmodule AppWeb.PokemonView do
  use AppWeb, :view
  alias AppWeb.PokemonView

  def render("index.json", %{pokemons: pokemons}) do
    render_many(pokemons, PokemonView, "pokemon.json")
  end

  def render("show.json", %{pokemon: pokemon}) do
    %{data: render_one(pokemon, PokemonView, "pokemon.json")}
  end

  def render("pokemon.json", %{pokemon: pokemon}) do
    %{
      id: pokemon.id,
      name: pokemon.name,
      weight: pokemon.weight,
      height: pokemon.height,
      abilities: pokemon.abilities,
      chosen: pokemon.chosen,
      url: pokemon.url,
      name_abilities: pokemon.name_abilities,
      url_image: pokemon.url_image
    }
  end

  def render("info.json", %{info: info}) do
    %{
      average_height: info.h,
      average_weight: info.w
    }
  end
end
