defmodule App.Pokemons.Pokemon do
  @moduledoc false
  
  use Ecto.Schema
  import Ecto.Changeset

  schema "pokemons" do
    field :abilities, :integer
    field :chosen, :boolean, default: false
    field :height, :integer
    field :name, :string
    field :url, :string
    field :weight, :integer
    field :name_abilities, :string
    field :url_image, :string

    timestamps()
  end

  @doc false
  def changeset(pokemon, attrs) do
    pokemon
    |> cast(attrs, [:name, :weight, :height, :abilities, :chosen, :url, :name_abilities, :url_image])
    |> validate_required([:name, :url])
  end
end
