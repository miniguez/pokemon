defmodule App.Pokemons do
  @moduledoc """
  The Pokemons context.
  """

  import Ecto.Query, warn: false
  alias App.Repo

  alias App.Pokemons.Pokemon

  @doc """
  Returns the list of pokemons.

  ## Examples

      iex> list_pokemons()
      [%Pokemon{}, ...]

  """
  def list_pokemons do    
    query = from(
      p in Pokemon,
      order_by: [desc: :chosen, asc: :id],
    )
    Repo.all(query)
  end

  @doc """
  Gets a single pokemon.

  Raises `Ecto.NoResultsError` if the Pokemon does not exist.

  ## Examples

      iex> get_pokemon!(123)
      %Pokemon{}

      iex> get_pokemon!(456)
      ** (Ecto.NoResultsError)

  """
  def get_pokemon!(id), do: Repo.get!(Pokemon, id)

  @doc """
  Creates a pokemon.

  ## Examples

      iex> create_pokemon(%{field: value})
      {:ok, %Pokemon{}}

      iex> create_pokemon(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_pokemon(attrs \\ %{}) do
    %Pokemon{}
    |> Pokemon.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a pokemon.

  ## Examples

      iex> update_pokemon(pokemon, %{field: new_value})
      {:ok, %Pokemon{}}

      iex> update_pokemon(pokemon, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_pokemon(%Pokemon{} = pokemon, attrs) do
    pokemon
    |> Pokemon.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a pokemon.

  ## Examples

      iex> delete_pokemon(pokemon)
      {:ok, %Pokemon{}}

      iex> delete_pokemon(pokemon)
      {:error, %Ecto.Changeset{}}

  """
  def delete_pokemon(%Pokemon{} = pokemon) do
    Repo.delete(pokemon)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking pokemon changes.

  ## Examples

      iex> change_pokemon(pokemon)
      %Ecto.Changeset{data: %Pokemon{}}

  """
  def change_pokemon(%Pokemon{} = pokemon, attrs \\ %{}) do
    Pokemon.changeset(pokemon, attrs)
  end

  @doc """
  Count pokemons.

  ## Examples

      iex> Pokemon.count_pokemons()
      1154
  """
  @spec count_pokemons() :: integer 
  def count_pokemons() do
    Repo.one(from p in Pokemon, select: count(p.id))
  end

  @doc """
  Sum weight pokemons.

  ## Examples

      iex> Pokemon.sum_weight()
      1139407
  """
  @spec sum_weight() :: integer
  def sum_weight() do
    Repo.one(from p in Pokemon, select: sum(p.weight))
  end

  @doc """
  Sum height pokemons.

  ## Examples

      iex> Pokemon.sum_height()
      24545
  """
  @spec sum_height() :: integer
  def sum_height() do
    Repo.one(from p in Pokemon, select: sum(p.height))
  end  

  @spec average(integer, integer) :: float 
  defp average(total, count) when count > 0, do: total / count
  defp average(_, _) , do: 0

  @doc """
  Average weight pokemons.

  ## Examples

      iex> Pokemons.average_weight()
      987.3544194107452
  """
  @spec average_weight() :: float
  def average_weight() do
    total = sum_weight()
    count = count_pokemons()
    total
    |> average(count)
    |> Float.round(2)
  end

  @doc """
  Average weight pokemons.

  ## Examples

      iex> Pokemons.average_height()
      21.26949740034662
  """
  @spec average_height() :: float
  def average_height() do 
    total = sum_height()
    count = count_pokemons()
    total  
    |> average(count)
    |> Float.round(2)
  end

end
