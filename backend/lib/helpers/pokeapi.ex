defmodule Helpers.Pokeapi do 
  @moduledoc """
  funciones para interactuar con pokeapi.co/api/v2
  """
  alias App.Pokemons

  @doc """
  Función para guardar en base de datos los registros obtenidos de la api pokeapi.co/api/v2
  """
  @spec save_api_list_pokemon() :: any
  def save_api_list_pokemon() do 
    pokemons = get_api_list_pokemon()
    if length(pokemons) > 0 do
      Enum.each(
        pokemons, 
        fn p -> 
        p
        |> add_info()
        |> Pokemons.create_pokemon() 
      end)
    end
  end

  @spec get_api_list_pokemon() :: list
  defp get_api_list_pokemon() do
    url = "https://pokeapi.co/api/v2/pokemon?offset=0&limit=1154"
 
    case HTTPoison.get(url) do
      {:ok, %HTTPoison.Response{status_code: 200, body: body}} ->        
        %{"results" => pokemons} = Jason.decode!(body)
        pokemons
      {:ok, %HTTPoison.Response{status_code: 404}} ->        
        []
      {:error, %HTTPoison.Error{reason: _reason}} ->        
        []
    end    
  end

  @spec get_api_info(String.t()) :: tuple
  defp get_api_info(url) do    
    case HTTPoison.get(url) do
      {:ok, %HTTPoison.Response{status_code: 200, body: body}} ->        
        %{"height" => height, "weight" => weight, "abilities" => abilities, "sprites" => sprites} = Jason.decode!(body)
        {height, weight, count_abilities(abilities), concat_abilities(abilities), get_url_image(sprites)}
      {:ok, %HTTPoison.Response{status_code: 404}} ->        
        {}
      {:error, %HTTPoison.Error{reason: _reason}} ->        
        {}
    end    
  end

  @spec count_abilities(list) :: integer 
  defp count_abilities(list) do
    list
    |> Enum.map(fn x -> Map.fetch!(x, "is_hidden") end)
    |> Enum.count(fn x -> x == true end)
  end

  @spec concat_abilities(map) :: String.t()
  defp concat_abilities(list) do
    list |> Enum.map(fn x -> get_ability(x) end) |> Enum.join
  end

  @spec get_ability(map) :: String.t() 
  defp get_ability(element) do
    %{"is_hidden"=> hidden, "ability" => %{"name"=> ability_name}} = element
    if hidden, do: "", else: ability_name 
  end

  @spec get_url_image(map) :: String.t() 
  defp get_url_image(sprites) do
    %{"other"=>%{"dream_world"=>%{"front_default"=> url_img}}} = sprites
    url_img
  end

  @spec add_info(map) :: map
  defp add_info(pokemon) do 
    url = Map.fetch!(pokemon, "url")
    info = get_api_info(url)
    {height, weight, abilities, name_abilities, url_image} = if tuple_size(info) > 0 do
      info
    else
      {0, 0, 0, "", ""}
    end
    
    pokemon 
    |> Map.put("height", height) 
    |> Map.put("weight", weight)
    |> Map.put("abilities", abilities)
    |> Map.put("name_abilities", name_abilities)
    |> Map.put("url_image", url_image)
  end
end