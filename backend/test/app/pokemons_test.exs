defmodule App.PokemonsTest do
  use App.DataCase

  alias App.Pokemons

  describe "pokemons" do
    alias App.Pokemons.Pokemon

    import App.PokemonsFixtures

    @invalid_attrs %{abilities: nil, chosen: nil, height: nil, name: nil, url: nil, weight: nil}

    test "list_pokemons/0 returns all pokemons" do
      pokemon = pokemon_fixture()
      assert Pokemons.list_pokemons() == [pokemon]
    end

    test "get_pokemon!/1 returns the pokemon with given id" do
      pokemon = pokemon_fixture()
      assert Pokemons.get_pokemon!(pokemon.id) == pokemon
    end

    test "create_pokemon/1 with valid data creates a pokemon" do
      valid_attrs = %{abilities: 42, chosen: true, height: 42, name: "some name", url: "some url", weight: 42}

      assert {:ok, %Pokemon{} = pokemon} = Pokemons.create_pokemon(valid_attrs)
      assert pokemon.abilities == 42
      assert pokemon.chosen == true
      assert pokemon.height == 42
      assert pokemon.name == "some name"
      assert pokemon.url == "some url"
      assert pokemon.weight == 42
    end

    test "create_pokemon/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Pokemons.create_pokemon(@invalid_attrs)
    end

    test "update_pokemon/2 with valid data updates the pokemon" do
      pokemon = pokemon_fixture()
      update_attrs = %{abilities: 43, chosen: false, height: 43, name: "some updated name", url: "some updated url", weight: 43}

      assert {:ok, %Pokemon{} = pokemon} = Pokemons.update_pokemon(pokemon, update_attrs)
      assert pokemon.abilities == 43
      assert pokemon.chosen == false
      assert pokemon.height == 43
      assert pokemon.name == "some updated name"
      assert pokemon.url == "some updated url"
      assert pokemon.weight == 43
    end

    test "update_pokemon/2 with invalid data returns error changeset" do
      pokemon = pokemon_fixture()
      assert {:error, %Ecto.Changeset{}} = Pokemons.update_pokemon(pokemon, @invalid_attrs)
      assert pokemon == Pokemons.get_pokemon!(pokemon.id)
    end

    test "delete_pokemon/1 deletes the pokemon" do
      pokemon = pokemon_fixture()
      assert {:ok, %Pokemon{}} = Pokemons.delete_pokemon(pokemon)
      assert_raise Ecto.NoResultsError, fn -> Pokemons.get_pokemon!(pokemon.id) end
    end

    test "change_pokemon/1 returns a pokemon changeset" do
      pokemon = pokemon_fixture()
      assert %Ecto.Changeset{} = Pokemons.change_pokemon(pokemon)
    end

    test "count pokemons" do
      pokemon_fixture()
      assert Pokemons.count_pokemons() > 0 
    end

    test "sum weight of pokemons" do 
      pokemon_fixture()
      assert Pokemons.sum_weight() == 42
    end

    test "sum height of pokemons" do 
      pokemon_fixture()
      assert Pokemons.sum_height() == 42
    end

    test "average of pokemon's weight" do
      pokemon_fixture()
      assert Pokemons.average_weight() == 42.0
    end

    test "average of pokemon's height" do
      pokemon_fixture()
      assert Pokemons.average_height() == 42.0
    end
  end
end
