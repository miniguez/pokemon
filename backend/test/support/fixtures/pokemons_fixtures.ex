defmodule App.PokemonsFixtures do
  @moduledoc """
  This module defines test helpers for creating
  entities via the `App.Pokemons` context.
  """

  @doc """
  Generate a pokemon.
  """
  def pokemon_fixture(attrs \\ %{}) do
    {:ok, pokemon} =
      attrs
      |> Enum.into(%{
        abilities: 42,
        chosen: true,
        height: 42,
        name: "some name",
        url: "some url",
        weight: 42
      })
      |> App.Pokemons.create_pokemon()

    pokemon
  end
end
