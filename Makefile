bootstrap:
	docker-compose up -d
	docker build -f Dockerfile-react --no-cache -t pokemon_react:latest .
	docker run -it -d --name pokemon_react_1 -p 3000:3000 pokemon_react:latest
	docker exec -i pokemon_db_1 /bin/bash -c "PGPASSWORD=postgres psql --username postgres dbpokemon" < db/dump_dbpokemon.sql

start:
	docker-compose start
	docker start pokemon_react_1

stop:
	docker-compose stop
	docker stop pokemon_react_1

logs.phx:
	docker-compose logs -f phoenix

test:
	ENV=test docker-compose run --rm  phoenix sh -c "mix test"

credo:
	ENV=test docker-compose run --rm --no-deps phoenix sh -c "mix credo"

remove:
	docker-compose down -v
	docker rm -f pokemon_react_1