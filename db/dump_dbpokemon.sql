--
-- PostgreSQL database dump
--

-- Dumped from database version 9.6.24
-- Dumped by pg_dump version 9.6.24

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

ALTER TABLE ONLY public.schema_migrations DROP CONSTRAINT schema_migrations_pkey;
ALTER TABLE ONLY public.pokemons DROP CONSTRAINT pokemons_pkey;
ALTER TABLE public.pokemons ALTER COLUMN id DROP DEFAULT;
DROP TABLE public.schema_migrations;
DROP SEQUENCE public.pokemons_id_seq;
DROP TABLE public.pokemons;
DROP EXTENSION plpgsql;
DROP SCHEMA public;
--
-- Name: public; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA public;


ALTER SCHEMA public OWNER TO postgres;

--
-- Name: SCHEMA public; Type: COMMENT; Schema: -; Owner: postgres
--

COMMENT ON SCHEMA public IS 'standard public schema';


--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: pokemons; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.pokemons (
    id bigint NOT NULL,
    name character varying(255),
    weight integer,
    height integer,
    abilities integer,
    chosen boolean DEFAULT false NOT NULL,
    url character varying(255),
    inserted_at timestamp(0) without time zone NOT NULL,
    updated_at timestamp(0) without time zone NOT NULL,
    name_abilities character varying(255),
    url_image character varying(255)
);


ALTER TABLE public.pokemons OWNER TO postgres;

--
-- Name: pokemons_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.pokemons_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.pokemons_id_seq OWNER TO postgres;

--
-- Name: pokemons_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.pokemons_id_seq OWNED BY public.pokemons.id;


--
-- Name: schema_migrations; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.schema_migrations (
    version bigint NOT NULL,
    inserted_at timestamp(0) without time zone
);


ALTER TABLE public.schema_migrations OWNER TO postgres;

--
-- Name: pokemons id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.pokemons ALTER COLUMN id SET DEFAULT nextval('public.pokemons_id_seq'::regclass);


--
-- Data for Name: pokemons; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.pokemons (id, name, weight, height, abilities, chosen, url, inserted_at, updated_at, name_abilities, url_image) FROM stdin;
1	bulbasaur	69	7	1	f	https://pokeapi.co/api/v2/pokemon/1/	2022-11-03 23:52:02	2022-11-03 23:52:02	overgrow	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/1.svg
3	venusaur	1000	20	1	f	https://pokeapi.co/api/v2/pokemon/3/	2022-11-03 23:52:02	2022-11-03 23:52:02	overgrow	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/3.svg
4	charmander	85	6	1	f	https://pokeapi.co/api/v2/pokemon/4/	2022-11-03 23:52:03	2022-11-03 23:52:03	blaze	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/4.svg
5	charmeleon	190	11	1	f	https://pokeapi.co/api/v2/pokemon/5/	2022-11-03 23:52:03	2022-11-03 23:52:03	blaze	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/5.svg
7	squirtle	90	5	1	f	https://pokeapi.co/api/v2/pokemon/7/	2022-11-03 23:52:03	2022-11-03 23:52:03	torrent	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/7.svg
8	wartortle	225	10	1	f	https://pokeapi.co/api/v2/pokemon/8/	2022-11-03 23:52:03	2022-11-03 23:52:03	torrent	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/8.svg
9	blastoise	855	16	1	f	https://pokeapi.co/api/v2/pokemon/9/	2022-11-03 23:52:03	2022-11-03 23:52:03	torrent	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/9.svg
10	caterpie	29	3	1	f	https://pokeapi.co/api/v2/pokemon/10/	2022-11-03 23:52:03	2022-11-03 23:52:03	shield-dust	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/10.svg
11	metapod	99	7	0	f	https://pokeapi.co/api/v2/pokemon/11/	2022-11-03 23:52:03	2022-11-03 23:52:03	shed-skin	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/11.svg
12	butterfree	320	11	1	f	https://pokeapi.co/api/v2/pokemon/12/	2022-11-03 23:52:03	2022-11-03 23:52:03	compound-eyes	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/12.svg
13	weedle	32	3	1	f	https://pokeapi.co/api/v2/pokemon/13/	2022-11-03 23:52:03	2022-11-03 23:52:03	shield-dust	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/13.svg
14	kakuna	100	6	0	f	https://pokeapi.co/api/v2/pokemon/14/	2022-11-03 23:52:03	2022-11-03 23:52:03	shed-skin	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/14.svg
15	beedrill	295	10	1	f	https://pokeapi.co/api/v2/pokemon/15/	2022-11-03 23:52:04	2022-11-03 23:52:04	swarm	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/15.svg
16	pidgey	18	3	1	f	https://pokeapi.co/api/v2/pokemon/16/	2022-11-03 23:52:04	2022-11-03 23:52:04	keen-eyetangled-feet	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/16.svg
17	pidgeotto	300	11	1	f	https://pokeapi.co/api/v2/pokemon/17/	2022-11-03 23:52:04	2022-11-03 23:52:04	keen-eyetangled-feet	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/17.svg
18	pidgeot	395	15	1	f	https://pokeapi.co/api/v2/pokemon/18/	2022-11-03 23:52:04	2022-11-03 23:52:04	keen-eyetangled-feet	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/18.svg
19	rattata	35	3	1	f	https://pokeapi.co/api/v2/pokemon/19/	2022-11-03 23:52:04	2022-11-03 23:52:04	run-awayguts	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/19.svg
20	raticate	185	7	1	f	https://pokeapi.co/api/v2/pokemon/20/	2022-11-03 23:52:04	2022-11-03 23:52:04	run-awayguts	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/20.svg
21	spearow	20	3	1	f	https://pokeapi.co/api/v2/pokemon/21/	2022-11-03 23:52:04	2022-11-03 23:52:04	keen-eye	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/21.svg
22	fearow	380	12	1	f	https://pokeapi.co/api/v2/pokemon/22/	2022-11-03 23:52:04	2022-11-03 23:52:04	keen-eye	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/22.svg
23	ekans	69	20	1	f	https://pokeapi.co/api/v2/pokemon/23/	2022-11-03 23:52:04	2022-11-03 23:52:04	intimidateshed-skin	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/23.svg
24	arbok	650	35	1	f	https://pokeapi.co/api/v2/pokemon/24/	2022-11-03 23:52:04	2022-11-03 23:52:04	intimidateshed-skin	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/24.svg
25	pikachu	60	4	1	f	https://pokeapi.co/api/v2/pokemon/25/	2022-11-03 23:52:05	2022-11-03 23:52:05	static	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/25.svg
26	raichu	300	8	1	f	https://pokeapi.co/api/v2/pokemon/26/	2022-11-03 23:52:05	2022-11-03 23:52:05	static	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/26.svg
27	sandshrew	120	6	1	f	https://pokeapi.co/api/v2/pokemon/27/	2022-11-03 23:52:05	2022-11-03 23:52:05	sand-veil	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/27.svg
28	sandslash	295	10	1	f	https://pokeapi.co/api/v2/pokemon/28/	2022-11-03 23:52:05	2022-11-03 23:52:05	sand-veil	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/28.svg
29	nidoran-f	70	4	1	f	https://pokeapi.co/api/v2/pokemon/29/	2022-11-03 23:52:05	2022-11-03 23:52:05	poison-pointrivalry	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/29.svg
30	nidorina	200	8	1	f	https://pokeapi.co/api/v2/pokemon/30/	2022-11-03 23:52:05	2022-11-03 23:52:05	poison-pointrivalry	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/30.svg
31	nidoqueen	600	13	1	f	https://pokeapi.co/api/v2/pokemon/31/	2022-11-03 23:52:05	2022-11-03 23:52:05	poison-pointrivalry	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/31.svg
32	nidoran-m	90	5	1	f	https://pokeapi.co/api/v2/pokemon/32/	2022-11-03 23:52:05	2022-11-03 23:52:05	poison-pointrivalry	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/32.svg
33	nidorino	195	9	1	f	https://pokeapi.co/api/v2/pokemon/33/	2022-11-03 23:52:05	2022-11-03 23:52:05	poison-pointrivalry	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/33.svg
34	nidoking	620	14	1	f	https://pokeapi.co/api/v2/pokemon/34/	2022-11-03 23:52:05	2022-11-03 23:52:05	poison-pointrivalry	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/34.svg
35	clefairy	75	6	1	f	https://pokeapi.co/api/v2/pokemon/35/	2022-11-03 23:52:06	2022-11-03 23:52:06	cute-charmmagic-guard	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/35.svg
6	charizard	905	17	1	f	https://pokeapi.co/api/v2/pokemon/6/	2022-11-03 23:52:03	2022-11-04 00:01:50	blaze	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/6.svg
36	clefable	400	13	1	f	https://pokeapi.co/api/v2/pokemon/36/	2022-11-03 23:52:06	2022-11-03 23:52:06	cute-charmmagic-guard	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/36.svg
45	vileplume	186	12	1	f	https://pokeapi.co/api/v2/pokemon/45/	2022-11-03 23:52:07	2022-11-03 23:52:07	chlorophyll	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/45.svg
55	golduck	766	17	1	f	https://pokeapi.co/api/v2/pokemon/55/	2022-11-03 23:52:08	2022-11-03 23:52:08	dampcloud-nine	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/55.svg
65	alakazam	480	15	1	f	https://pokeapi.co/api/v2/pokemon/65/	2022-11-03 23:52:09	2022-11-03 23:52:09	synchronizeinner-focus	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/65.svg
75	graveler	1050	10	1	f	https://pokeapi.co/api/v2/pokemon/75/	2022-11-03 23:52:10	2022-11-03 23:52:10	rock-headsturdy	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/75.svg
85	dodrio	852	18	1	f	https://pokeapi.co/api/v2/pokemon/85/	2022-11-03 23:52:11	2022-11-03 23:52:11	run-awayearly-bird	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/85.svg
95	onix	2100	88	1	f	https://pokeapi.co/api/v2/pokemon/95/	2022-11-03 23:52:12	2022-11-03 23:52:12	rock-headsturdy	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/95.svg
105	marowak	450	10	1	f	https://pokeapi.co/api/v2/pokemon/105/	2022-11-03 23:52:12	2022-11-03 23:52:12	rock-headlightning-rod	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/105.svg
119	seaking	390	13	1	f	https://pokeapi.co/api/v2/pokemon/119/	2022-11-03 23:52:14	2022-11-03 23:52:14	swift-swimwater-veil	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/119.svg
129	magikarp	100	9	1	f	https://pokeapi.co/api/v2/pokemon/129/	2022-11-03 23:52:15	2022-11-03 23:52:15	swift-swim	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/129.svg
139	omastar	350	10	1	f	https://pokeapi.co/api/v2/pokemon/139/	2022-11-03 23:52:16	2022-11-03 23:52:16	swift-swimshell-armor	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/139.svg
149	dragonite	2100	22	1	f	https://pokeapi.co/api/v2/pokemon/149/	2022-11-03 23:52:17	2022-11-03 23:52:17	inner-focus	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/149.svg
159	croconaw	250	11	1	f	https://pokeapi.co/api/v2/pokemon/159/	2022-11-03 23:52:18	2022-11-03 23:52:18	torrent	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/159.svg
37	vulpix	99	6	1	f	https://pokeapi.co/api/v2/pokemon/37/	2022-11-03 23:52:06	2022-11-03 23:52:06	flash-fire	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/37.svg
46	paras	54	3	1	f	https://pokeapi.co/api/v2/pokemon/46/	2022-11-03 23:52:07	2022-11-03 23:52:07	effect-sporedry-skin	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/46.svg
56	mankey	280	5	1	f	https://pokeapi.co/api/v2/pokemon/56/	2022-11-03 23:52:08	2022-11-03 23:52:08	vital-spiritanger-point	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/56.svg
66	machop	195	8	1	f	https://pokeapi.co/api/v2/pokemon/66/	2022-11-03 23:52:09	2022-11-03 23:52:09	gutsno-guard	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/66.svg
76	golem	3000	14	1	f	https://pokeapi.co/api/v2/pokemon/76/	2022-11-03 23:52:10	2022-11-03 23:52:10	rock-headsturdy	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/76.svg
86	seel	900	11	1	f	https://pokeapi.co/api/v2/pokemon/86/	2022-11-03 23:52:11	2022-11-03 23:52:11	thick-fathydration	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/86.svg
96	drowzee	324	10	1	f	https://pokeapi.co/api/v2/pokemon/96/	2022-11-03 23:52:12	2022-11-03 23:52:12	insomniaforewarn	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/96.svg
106	hitmonlee	498	15	1	f	https://pokeapi.co/api/v2/pokemon/106/	2022-11-03 23:52:13	2022-11-03 23:52:13	limberreckless	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/106.svg
111	rhyhorn	1150	10	1	f	https://pokeapi.co/api/v2/pokemon/111/	2022-11-03 23:52:13	2022-11-03 23:52:13	lightning-rodrock-head	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/111.svg
121	starmie	800	11	1	f	https://pokeapi.co/api/v2/pokemon/121/	2022-11-03 23:52:14	2022-11-03 23:52:14	illuminatenatural-cure	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/121.svg
131	lapras	2200	25	1	f	https://pokeapi.co/api/v2/pokemon/131/	2022-11-03 23:52:15	2022-11-03 23:52:15	water-absorbshell-armor	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/131.svg
141	kabutops	405	13	1	f	https://pokeapi.co/api/v2/pokemon/141/	2022-11-03 23:52:16	2022-11-03 23:52:16	swift-swimbattle-armor	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/141.svg
151	mew	40	4	0	f	https://pokeapi.co/api/v2/pokemon/151/	2022-11-03 23:52:17	2022-11-03 23:52:17	synchronize	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/151.svg
161	sentret	60	8	1	f	https://pokeapi.co/api/v2/pokemon/161/	2022-11-03 23:52:18	2022-11-03 23:52:18	run-awaykeen-eye	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/161.svg
38	ninetales	199	11	1	f	https://pokeapi.co/api/v2/pokemon/38/	2022-11-03 23:52:06	2022-11-03 23:52:06	flash-fire	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/38.svg
47	parasect	295	10	1	f	https://pokeapi.co/api/v2/pokemon/47/	2022-11-03 23:52:07	2022-11-03 23:52:07	effect-sporedry-skin	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/47.svg
57	primeape	320	10	1	f	https://pokeapi.co/api/v2/pokemon/57/	2022-11-03 23:52:08	2022-11-03 23:52:08	vital-spiritanger-point	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/57.svg
67	machoke	705	15	1	f	https://pokeapi.co/api/v2/pokemon/67/	2022-11-03 23:52:09	2022-11-03 23:52:09	gutsno-guard	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/67.svg
77	ponyta	300	10	1	f	https://pokeapi.co/api/v2/pokemon/77/	2022-11-03 23:52:10	2022-11-03 23:52:10	run-awayflash-fire	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/77.svg
87	dewgong	1200	17	1	f	https://pokeapi.co/api/v2/pokemon/87/	2022-11-03 23:52:11	2022-11-03 23:52:11	thick-fathydration	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/87.svg
97	hypno	756	16	1	f	https://pokeapi.co/api/v2/pokemon/97/	2022-11-03 23:52:12	2022-11-03 23:52:12	insomniaforewarn	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/97.svg
107	hitmonchan	502	14	1	f	https://pokeapi.co/api/v2/pokemon/107/	2022-11-03 23:52:13	2022-11-03 23:52:13	keen-eyeiron-fist	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/107.svg
112	rhydon	1200	19	1	f	https://pokeapi.co/api/v2/pokemon/112/	2022-11-03 23:52:14	2022-11-03 23:52:14	lightning-rodrock-head	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/112.svg
122	mr-mime	545	13	1	f	https://pokeapi.co/api/v2/pokemon/122/	2022-11-03 23:52:14	2022-11-03 23:52:14	soundprooffilter	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/122.svg
132	ditto	40	3	1	f	https://pokeapi.co/api/v2/pokemon/132/	2022-11-03 23:52:15	2022-11-03 23:52:15	limber	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/132.svg
142	aerodactyl	590	18	1	f	https://pokeapi.co/api/v2/pokemon/142/	2022-11-03 23:52:16	2022-11-03 23:52:16	rock-headpressure	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/142.svg
152	chikorita	64	9	1	f	https://pokeapi.co/api/v2/pokemon/152/	2022-11-03 23:52:17	2022-11-03 23:52:17	overgrow	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/152.svg
162	furret	325	18	1	f	https://pokeapi.co/api/v2/pokemon/162/	2022-11-03 23:52:18	2022-11-03 23:52:18	run-awaykeen-eye	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/162.svg
39	jigglypuff	55	5	1	f	https://pokeapi.co/api/v2/pokemon/39/	2022-11-03 23:52:06	2022-11-03 23:52:06	cute-charmcompetitive	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/39.svg
48	venonat	300	10	1	f	https://pokeapi.co/api/v2/pokemon/48/	2022-11-03 23:52:07	2022-11-03 23:52:07	compound-eyestinted-lens	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/48.svg
58	growlithe	190	7	1	f	https://pokeapi.co/api/v2/pokemon/58/	2022-11-03 23:52:08	2022-11-03 23:52:08	intimidateflash-fire	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/58.svg
68	machamp	1300	16	1	f	https://pokeapi.co/api/v2/pokemon/68/	2022-11-03 23:52:09	2022-11-03 23:52:09	gutsno-guard	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/68.svg
78	rapidash	950	17	1	f	https://pokeapi.co/api/v2/pokemon/78/	2022-11-03 23:52:10	2022-11-03 23:52:10	run-awayflash-fire	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/78.svg
88	grimer	300	9	1	f	https://pokeapi.co/api/v2/pokemon/88/	2022-11-03 23:52:11	2022-11-03 23:52:11	stenchsticky-hold	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/88.svg
98	krabby	65	4	1	f	https://pokeapi.co/api/v2/pokemon/98/	2022-11-03 23:52:12	2022-11-03 23:52:12	hyper-cuttershell-armor	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/98.svg
108	lickitung	655	12	1	f	https://pokeapi.co/api/v2/pokemon/108/	2022-11-03 23:52:13	2022-11-03 23:52:13	own-tempooblivious	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/108.svg
113	chansey	346	11	1	f	https://pokeapi.co/api/v2/pokemon/113/	2022-11-03 23:52:14	2022-11-03 23:52:14	natural-cureserene-grace	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/113.svg
123	scyther	560	15	1	f	https://pokeapi.co/api/v2/pokemon/123/	2022-11-03 23:52:15	2022-11-03 23:52:15	swarmtechnician	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/123.svg
133	eevee	65	3	1	f	https://pokeapi.co/api/v2/pokemon/133/	2022-11-03 23:52:15	2022-11-03 23:52:15	run-awayadaptability	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/133.svg
143	snorlax	4600	21	1	f	https://pokeapi.co/api/v2/pokemon/143/	2022-11-03 23:52:16	2022-11-03 23:52:16	immunitythick-fat	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/143.svg
153	bayleef	158	12	1	f	https://pokeapi.co/api/v2/pokemon/153/	2022-11-03 23:52:17	2022-11-03 23:52:17	overgrow	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/153.svg
163	hoothoot	212	7	1	f	https://pokeapi.co/api/v2/pokemon/163/	2022-11-03 23:52:18	2022-11-03 23:52:18	insomniakeen-eye	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/163.svg
40	wigglytuff	120	10	1	f	https://pokeapi.co/api/v2/pokemon/40/	2022-11-03 23:52:06	2022-11-03 23:52:06	cute-charmcompetitive	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/40.svg
49	venomoth	125	15	1	f	https://pokeapi.co/api/v2/pokemon/49/	2022-11-03 23:52:07	2022-11-03 23:52:07	shield-dusttinted-lens	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/49.svg
59	arcanine	1550	19	1	f	https://pokeapi.co/api/v2/pokemon/59/	2022-11-03 23:52:08	2022-11-03 23:52:08	intimidateflash-fire	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/59.svg
69	bellsprout	40	7	1	f	https://pokeapi.co/api/v2/pokemon/69/	2022-11-03 23:52:09	2022-11-03 23:52:09	chlorophyll	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/69.svg
79	slowpoke	360	12	1	f	https://pokeapi.co/api/v2/pokemon/79/	2022-11-03 23:52:10	2022-11-03 23:52:10	obliviousown-tempo	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/79.svg
89	muk	300	12	1	f	https://pokeapi.co/api/v2/pokemon/89/	2022-11-03 23:52:11	2022-11-03 23:52:11	stenchsticky-hold	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/89.svg
99	kingler	600	13	1	f	https://pokeapi.co/api/v2/pokemon/99/	2022-11-03 23:52:12	2022-11-03 23:52:12	hyper-cuttershell-armor	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/99.svg
109	koffing	10	6	1	f	https://pokeapi.co/api/v2/pokemon/109/	2022-11-03 23:52:13	2022-11-03 23:52:13	levitateneutralizing-gas	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/109.svg
114	tangela	350	10	1	f	https://pokeapi.co/api/v2/pokemon/114/	2022-11-03 23:52:14	2022-11-03 23:52:14	chlorophyllleaf-guard	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/114.svg
124	jynx	406	14	1	f	https://pokeapi.co/api/v2/pokemon/124/	2022-11-03 23:52:15	2022-11-03 23:52:15	obliviousforewarn	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/124.svg
134	vaporeon	290	10	1	f	https://pokeapi.co/api/v2/pokemon/134/	2022-11-03 23:52:15	2022-11-03 23:52:15	water-absorb	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/134.svg
144	articuno	554	17	1	f	https://pokeapi.co/api/v2/pokemon/144/	2022-11-03 23:52:16	2022-11-03 23:52:16	pressure	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/144.svg
154	meganium	1005	18	1	f	https://pokeapi.co/api/v2/pokemon/154/	2022-11-03 23:52:17	2022-11-03 23:52:17	overgrow	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/154.svg
164	noctowl	408	16	1	f	https://pokeapi.co/api/v2/pokemon/164/	2022-11-03 23:52:18	2022-11-03 23:52:18	insomniakeen-eye	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/164.svg
41	zubat	75	8	1	f	https://pokeapi.co/api/v2/pokemon/41/	2022-11-03 23:52:06	2022-11-03 23:52:06	inner-focus	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/41.svg
50	diglett	8	2	1	f	https://pokeapi.co/api/v2/pokemon/50/	2022-11-03 23:52:07	2022-11-03 23:52:07	sand-veilarena-trap	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/50.svg
60	poliwag	124	6	1	f	https://pokeapi.co/api/v2/pokemon/60/	2022-11-03 23:52:08	2022-11-03 23:52:08	water-absorbdamp	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/60.svg
70	weepinbell	64	10	1	f	https://pokeapi.co/api/v2/pokemon/70/	2022-11-03 23:52:09	2022-11-03 23:52:09	chlorophyll	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/70.svg
80	slowbro	785	16	1	f	https://pokeapi.co/api/v2/pokemon/80/	2022-11-03 23:52:10	2022-11-03 23:52:10	obliviousown-tempo	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/80.svg
90	shellder	40	3	1	f	https://pokeapi.co/api/v2/pokemon/90/	2022-11-03 23:52:11	2022-11-03 23:52:11	shell-armorskill-link	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/90.svg
100	voltorb	104	5	1	f	https://pokeapi.co/api/v2/pokemon/100/	2022-11-03 23:52:12	2022-11-03 23:52:12	soundproofstatic	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/100.svg
110	weezing	95	12	1	f	https://pokeapi.co/api/v2/pokemon/110/	2022-11-03 23:52:13	2022-11-03 23:52:13	levitateneutralizing-gas	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/110.svg
115	kangaskhan	800	22	1	f	https://pokeapi.co/api/v2/pokemon/115/	2022-11-03 23:52:14	2022-11-03 23:52:14	early-birdscrappy	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/115.svg
125	electabuzz	300	11	1	f	https://pokeapi.co/api/v2/pokemon/125/	2022-11-03 23:52:15	2022-11-03 23:52:15	static	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/125.svg
135	jolteon	245	8	1	f	https://pokeapi.co/api/v2/pokemon/135/	2022-11-03 23:52:16	2022-11-03 23:52:16	volt-absorb	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/135.svg
145	zapdos	526	16	1	f	https://pokeapi.co/api/v2/pokemon/145/	2022-11-03 23:52:16	2022-11-03 23:52:16	pressure	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/145.svg
155	cyndaquil	79	5	1	f	https://pokeapi.co/api/v2/pokemon/155/	2022-11-03 23:52:17	2022-11-03 23:52:17	blaze	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/155.svg
165	ledyba	108	10	1	f	https://pokeapi.co/api/v2/pokemon/165/	2022-11-03 23:52:18	2022-11-03 23:52:18	swarmearly-bird	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/165.svg
42	golbat	550	16	1	f	https://pokeapi.co/api/v2/pokemon/42/	2022-11-03 23:52:06	2022-11-03 23:52:06	inner-focus	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/42.svg
51	dugtrio	333	7	1	f	https://pokeapi.co/api/v2/pokemon/51/	2022-11-03 23:52:07	2022-11-03 23:52:07	sand-veilarena-trap	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/51.svg
61	poliwhirl	200	10	1	f	https://pokeapi.co/api/v2/pokemon/61/	2022-11-03 23:52:08	2022-11-03 23:52:08	water-absorbdamp	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/61.svg
71	victreebel	155	17	1	f	https://pokeapi.co/api/v2/pokemon/71/	2022-11-03 23:52:09	2022-11-03 23:52:09	chlorophyll	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/71.svg
81	magnemite	60	3	1	f	https://pokeapi.co/api/v2/pokemon/81/	2022-11-03 23:52:10	2022-11-03 23:52:10	magnet-pullsturdy	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/81.svg
91	cloyster	1325	15	1	f	https://pokeapi.co/api/v2/pokemon/91/	2022-11-03 23:52:11	2022-11-03 23:52:11	shell-armorskill-link	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/91.svg
101	electrode	666	12	1	f	https://pokeapi.co/api/v2/pokemon/101/	2022-11-03 23:52:12	2022-11-03 23:52:12	soundproofstatic	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/101.svg
116	horsea	80	4	1	f	https://pokeapi.co/api/v2/pokemon/116/	2022-11-03 23:52:14	2022-11-03 23:52:14	swift-swimsniper	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/116.svg
126	magmar	445	13	1	f	https://pokeapi.co/api/v2/pokemon/126/	2022-11-03 23:52:15	2022-11-03 23:52:15	flame-body	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/126.svg
136	flareon	250	9	1	f	https://pokeapi.co/api/v2/pokemon/136/	2022-11-03 23:52:16	2022-11-03 23:52:16	flash-fire	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/136.svg
146	moltres	600	20	1	f	https://pokeapi.co/api/v2/pokemon/146/	2022-11-03 23:52:17	2022-11-03 23:52:17	pressure	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/146.svg
156	quilava	190	9	1	f	https://pokeapi.co/api/v2/pokemon/156/	2022-11-03 23:52:18	2022-11-03 23:52:18	blaze	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/156.svg
166	ledian	356	14	1	f	https://pokeapi.co/api/v2/pokemon/166/	2022-11-03 23:52:18	2022-11-03 23:52:18	swarmearly-bird	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/166.svg
43	oddish	54	5	1	f	https://pokeapi.co/api/v2/pokemon/43/	2022-11-03 23:52:06	2022-11-03 23:52:06	chlorophyll	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/43.svg
53	persian	320	10	1	f	https://pokeapi.co/api/v2/pokemon/53/	2022-11-03 23:52:07	2022-11-03 23:52:07	limbertechnician	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/53.svg
63	abra	195	9	1	f	https://pokeapi.co/api/v2/pokemon/63/	2022-11-03 23:52:08	2022-11-03 23:52:08	synchronizeinner-focus	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/63.svg
73	tentacruel	550	16	1	f	https://pokeapi.co/api/v2/pokemon/73/	2022-11-03 23:52:09	2022-11-03 23:52:09	clear-bodyliquid-ooze	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/73.svg
83	farfetchd	150	8	1	f	https://pokeapi.co/api/v2/pokemon/83/	2022-11-03 23:52:10	2022-11-03 23:52:10	keen-eyeinner-focus	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/83.svg
93	haunter	1	16	0	f	https://pokeapi.co/api/v2/pokemon/93/	2022-11-03 23:52:11	2022-11-03 23:52:11	levitate	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/93.svg
103	exeggutor	1200	20	1	f	https://pokeapi.co/api/v2/pokemon/103/	2022-11-03 23:52:12	2022-11-03 23:52:12	chlorophyll	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/103.svg
120	staryu	345	8	1	f	https://pokeapi.co/api/v2/pokemon/120/	2022-11-03 23:52:14	2022-11-03 23:52:14	illuminatenatural-cure	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/120.svg
130	gyarados	2350	65	1	f	https://pokeapi.co/api/v2/pokemon/130/	2022-11-03 23:52:15	2022-11-03 23:52:15	intimidate	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/130.svg
140	kabuto	115	5	1	f	https://pokeapi.co/api/v2/pokemon/140/	2022-11-03 23:52:16	2022-11-03 23:52:16	swift-swimbattle-armor	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/140.svg
150	mewtwo	1220	20	1	f	https://pokeapi.co/api/v2/pokemon/150/	2022-11-03 23:52:17	2022-11-03 23:52:17	pressure	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/150.svg
160	feraligatr	888	23	1	f	https://pokeapi.co/api/v2/pokemon/160/	2022-11-03 23:52:18	2022-11-03 23:52:18	torrent	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/160.svg
44	gloom	86	8	1	f	https://pokeapi.co/api/v2/pokemon/44/	2022-11-03 23:52:07	2022-11-03 23:52:07	chlorophyll	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/44.svg
54	psyduck	196	8	1	f	https://pokeapi.co/api/v2/pokemon/54/	2022-11-03 23:52:08	2022-11-03 23:52:08	dampcloud-nine	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/54.svg
64	kadabra	565	13	1	f	https://pokeapi.co/api/v2/pokemon/64/	2022-11-03 23:52:09	2022-11-03 23:52:09	synchronizeinner-focus	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/64.svg
74	geodude	200	4	1	f	https://pokeapi.co/api/v2/pokemon/74/	2022-11-03 23:52:10	2022-11-03 23:52:10	rock-headsturdy	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/74.svg
84	doduo	392	14	1	f	https://pokeapi.co/api/v2/pokemon/84/	2022-11-03 23:52:10	2022-11-03 23:52:10	run-awayearly-bird	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/84.svg
94	gengar	405	15	0	f	https://pokeapi.co/api/v2/pokemon/94/	2022-11-03 23:52:11	2022-11-03 23:52:11	cursed-body	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/94.svg
104	cubone	65	4	1	f	https://pokeapi.co/api/v2/pokemon/104/	2022-11-03 23:52:12	2022-11-03 23:52:12	rock-headlightning-rod	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/104.svg
117	seadra	250	12	1	f	https://pokeapi.co/api/v2/pokemon/117/	2022-11-03 23:52:14	2022-11-03 23:52:14	poison-pointsniper	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/117.svg
127	pinsir	550	15	1	f	https://pokeapi.co/api/v2/pokemon/127/	2022-11-03 23:52:15	2022-11-03 23:52:15	hyper-cuttermold-breaker	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/127.svg
137	porygon	365	8	1	f	https://pokeapi.co/api/v2/pokemon/137/	2022-11-03 23:52:16	2022-11-03 23:52:16	tracedownload	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/137.svg
147	dratini	33	18	1	f	https://pokeapi.co/api/v2/pokemon/147/	2022-11-03 23:52:17	2022-11-03 23:52:17	shed-skin	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/147.svg
157	typhlosion	795	17	1	f	https://pokeapi.co/api/v2/pokemon/157/	2022-11-03 23:52:18	2022-11-03 23:52:18	blaze	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/157.svg
52	meowth	42	4	1	f	https://pokeapi.co/api/v2/pokemon/52/	2022-11-03 23:52:07	2022-11-03 23:52:07	pickuptechnician	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/52.svg
62	poliwrath	540	13	1	f	https://pokeapi.co/api/v2/pokemon/62/	2022-11-03 23:52:08	2022-11-03 23:52:08	water-absorbdamp	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/62.svg
72	tentacool	455	9	1	f	https://pokeapi.co/api/v2/pokemon/72/	2022-11-03 23:52:09	2022-11-03 23:52:09	clear-bodyliquid-ooze	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/72.svg
82	magneton	600	10	1	f	https://pokeapi.co/api/v2/pokemon/82/	2022-11-03 23:52:10	2022-11-03 23:52:10	magnet-pullsturdy	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/82.svg
92	gastly	1	13	0	f	https://pokeapi.co/api/v2/pokemon/92/	2022-11-03 23:52:11	2022-11-03 23:52:11	levitate	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/92.svg
102	exeggcute	25	4	1	f	https://pokeapi.co/api/v2/pokemon/102/	2022-11-03 23:52:12	2022-11-03 23:52:12	chlorophyll	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/102.svg
118	goldeen	150	6	1	f	https://pokeapi.co/api/v2/pokemon/118/	2022-11-03 23:52:14	2022-11-03 23:52:14	swift-swimwater-veil	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/118.svg
128	tauros	884	14	1	f	https://pokeapi.co/api/v2/pokemon/128/	2022-11-03 23:52:15	2022-11-03 23:52:15	intimidateanger-point	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/128.svg
138	omanyte	75	4	1	f	https://pokeapi.co/api/v2/pokemon/138/	2022-11-03 23:52:16	2022-11-03 23:52:16	swift-swimshell-armor	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/138.svg
148	dragonair	165	40	1	f	https://pokeapi.co/api/v2/pokemon/148/	2022-11-03 23:52:17	2022-11-03 23:52:17	shed-skin	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/148.svg
158	totodile	95	6	1	f	https://pokeapi.co/api/v2/pokemon/158/	2022-11-03 23:52:18	2022-11-03 23:52:18	torrent	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/158.svg
167	spinarak	85	5	1	f	https://pokeapi.co/api/v2/pokemon/167/	2022-11-03 23:52:18	2022-11-03 23:52:18	swarminsomnia	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/167.svg
168	ariados	335	11	1	f	https://pokeapi.co/api/v2/pokemon/168/	2022-11-03 23:52:19	2022-11-03 23:52:19	swarminsomnia	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/168.svg
169	crobat	750	18	1	f	https://pokeapi.co/api/v2/pokemon/169/	2022-11-03 23:52:19	2022-11-03 23:52:19	inner-focus	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/169.svg
170	chinchou	120	5	1	f	https://pokeapi.co/api/v2/pokemon/170/	2022-11-03 23:52:19	2022-11-03 23:52:19	volt-absorbilluminate	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/170.svg
171	lanturn	225	12	1	f	https://pokeapi.co/api/v2/pokemon/171/	2022-11-03 23:52:19	2022-11-03 23:52:19	volt-absorbilluminate	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/171.svg
172	pichu	20	3	1	f	https://pokeapi.co/api/v2/pokemon/172/	2022-11-03 23:52:19	2022-11-03 23:52:19	static	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/172.svg
173	cleffa	30	3	1	f	https://pokeapi.co/api/v2/pokemon/173/	2022-11-03 23:52:19	2022-11-03 23:52:19	cute-charmmagic-guard	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/173.svg
174	igglybuff	10	3	1	f	https://pokeapi.co/api/v2/pokemon/174/	2022-11-03 23:52:19	2022-11-03 23:52:19	cute-charmcompetitive	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/174.svg
175	togepi	15	3	1	f	https://pokeapi.co/api/v2/pokemon/175/	2022-11-03 23:52:19	2022-11-03 23:52:19	hustleserene-grace	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/175.svg
176	togetic	32	6	1	f	https://pokeapi.co/api/v2/pokemon/176/	2022-11-03 23:52:19	2022-11-03 23:52:19	hustleserene-grace	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/176.svg
177	natu	20	2	1	f	https://pokeapi.co/api/v2/pokemon/177/	2022-11-03 23:52:19	2022-11-03 23:52:19	synchronizeearly-bird	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/177.svg
178	xatu	150	15	1	f	https://pokeapi.co/api/v2/pokemon/178/	2022-11-03 23:52:19	2022-11-03 23:52:19	synchronizeearly-bird	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/178.svg
179	mareep	78	6	1	f	https://pokeapi.co/api/v2/pokemon/179/	2022-11-03 23:52:20	2022-11-03 23:52:20	static	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/179.svg
180	flaaffy	133	8	1	f	https://pokeapi.co/api/v2/pokemon/180/	2022-11-03 23:52:20	2022-11-03 23:52:20	static	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/180.svg
181	ampharos	615	14	1	f	https://pokeapi.co/api/v2/pokemon/181/	2022-11-03 23:52:20	2022-11-03 23:52:20	static	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/181.svg
182	bellossom	58	4	1	f	https://pokeapi.co/api/v2/pokemon/182/	2022-11-03 23:52:20	2022-11-03 23:52:20	chlorophyll	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/182.svg
183	marill	85	4	1	f	https://pokeapi.co/api/v2/pokemon/183/	2022-11-03 23:52:20	2022-11-03 23:52:20	thick-fathuge-power	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/183.svg
184	azumarill	285	8	1	f	https://pokeapi.co/api/v2/pokemon/184/	2022-11-03 23:52:20	2022-11-03 23:52:20	thick-fathuge-power	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/184.svg
185	sudowoodo	380	12	1	f	https://pokeapi.co/api/v2/pokemon/185/	2022-11-03 23:52:20	2022-11-03 23:52:20	sturdyrock-head	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/185.svg
186	politoed	339	11	1	f	https://pokeapi.co/api/v2/pokemon/186/	2022-11-03 23:52:20	2022-11-03 23:52:20	water-absorbdamp	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/186.svg
187	hoppip	5	4	1	f	https://pokeapi.co/api/v2/pokemon/187/	2022-11-03 23:52:20	2022-11-03 23:52:20	chlorophyllleaf-guard	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/187.svg
188	skiploom	10	6	1	f	https://pokeapi.co/api/v2/pokemon/188/	2022-11-03 23:52:20	2022-11-03 23:52:20	chlorophyllleaf-guard	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/188.svg
189	jumpluff	30	8	1	f	https://pokeapi.co/api/v2/pokemon/189/	2022-11-03 23:52:20	2022-11-03 23:52:20	chlorophyllleaf-guard	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/189.svg
200	misdreavus	10	7	0	f	https://pokeapi.co/api/v2/pokemon/200/	2022-11-03 23:52:21	2022-11-03 23:52:21	levitate	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/200.svg
210	granbull	487	14	1	f	https://pokeapi.co/api/v2/pokemon/210/	2022-11-03 23:52:22	2022-11-03 23:52:22	intimidatequick-feet	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/210.svg
220	swinub	65	4	1	f	https://pokeapi.co/api/v2/pokemon/220/	2022-11-03 23:52:23	2022-11-03 23:52:23	oblivioussnow-cloak	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/220.svg
230	kingdra	1520	18	1	f	https://pokeapi.co/api/v2/pokemon/230/	2022-11-03 23:52:24	2022-11-03 23:52:24	swift-swimsniper	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/230.svg
240	magby	214	7	1	f	https://pokeapi.co/api/v2/pokemon/240/	2022-11-03 23:52:25	2022-11-03 23:52:25	flame-body	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/240.svg
250	ho-oh	1990	38	1	f	https://pokeapi.co/api/v2/pokemon/250/	2022-11-03 23:52:26	2022-11-03 23:52:26	pressure	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/250.svg
260	swampert	819	15	1	f	https://pokeapi.co/api/v2/pokemon/260/	2022-11-03 23:52:27	2022-11-03 23:52:27	torrent	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/260.svg
270	lotad	26	5	1	f	https://pokeapi.co/api/v2/pokemon/270/	2022-11-03 23:52:28	2022-11-03 23:52:28	swift-swimrain-dish	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/270.svg
280	ralts	66	4	1	f	https://pokeapi.co/api/v2/pokemon/280/	2022-11-03 23:52:29	2022-11-03 23:52:29	synchronizetrace	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/280.svg
285	shroomish	45	4	1	f	https://pokeapi.co/api/v2/pokemon/285/	2022-11-03 23:52:30	2022-11-03 23:52:30	effect-sporepoison-heal	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/285.svg
296	makuhita	864	10	1	f	https://pokeapi.co/api/v2/pokemon/296/	2022-11-03 23:52:33	2022-11-03 23:52:33	thick-fatguts	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/296.svg
301	delcatty	326	11	1	f	https://pokeapi.co/api/v2/pokemon/301/	2022-11-03 23:52:34	2022-11-03 23:52:34	cute-charmnormalize	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/301.svg
315	roselia	20	3	1	f	https://pokeapi.co/api/v2/pokemon/315/	2022-11-03 23:52:39	2022-11-03 23:52:39	natural-curepoison-point	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/315.svg
317	swalot	800	17	1	f	https://pokeapi.co/api/v2/pokemon/317/	2022-11-03 23:52:40	2022-11-03 23:52:40	liquid-oozesticky-hold	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/317.svg
333	swablu	12	4	1	f	https://pokeapi.co/api/v2/pokemon/333/	2022-11-03 23:52:45	2022-11-03 23:52:45	natural-cure	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/333.svg
340	whiscash	236	9	1	f	https://pokeapi.co/api/v2/pokemon/340/	2022-11-03 23:52:46	2022-11-03 23:52:46	obliviousanticipation	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/340.svg
354	banette	125	11	1	f	https://pokeapi.co/api/v2/pokemon/354/	2022-11-03 23:52:49	2022-11-03 23:52:49	insomniafrisk	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/354.svg
362	glalie	2565	15	1	f	https://pokeapi.co/api/v2/pokemon/362/	2022-11-03 23:52:52	2022-11-03 23:52:52	inner-focusice-body	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/362.svg
371	bagon	421	6	1	f	https://pokeapi.co/api/v2/pokemon/371/	2022-11-03 23:52:55	2022-11-03 23:52:55	rock-head	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/371.svg
374	beldum	952	6	1	f	https://pokeapi.co/api/v2/pokemon/374/	2022-11-03 23:52:56	2022-11-03 23:52:56	clear-body	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/374.svg
393	piplup	52	4	1	f	https://pokeapi.co/api/v2/pokemon/393/	2022-11-03 23:53:03	2022-11-03 23:53:03	torrent	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/393.svg
403	shinx	95	5	1	f	https://pokeapi.co/api/v2/pokemon/403/	2022-11-03 23:53:04	2022-11-03 23:53:04	rivalryintimidate	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/403.svg
410	shieldon	570	5	1	f	https://pokeapi.co/api/v2/pokemon/410/	2022-11-03 23:53:05	2022-11-03 23:53:05	sturdy	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/410.svg
427	buneary	55	4	1	f	https://pokeapi.co/api/v2/pokemon/427/	2022-11-03 23:53:10	2022-11-03 23:53:10	run-awayklutz	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/427.svg
431	glameow	39	5	1	f	https://pokeapi.co/api/v2/pokemon/431/	2022-11-03 23:53:11	2022-11-03 23:53:11	limberown-tempo	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/431.svg
440	happiny	244	6	1	f	https://pokeapi.co/api/v2/pokemon/440/	2022-11-03 23:53:12	2022-11-03 23:53:12	natural-cureserene-grace	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/440.svg
451	skorupi	120	8	1	f	https://pokeapi.co/api/v2/pokemon/451/	2022-11-03 23:53:15	2022-11-03 23:53:15	battle-armorsniper	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/451.svg
465	tangrowth	1286	20	1	f	https://pokeapi.co/api/v2/pokemon/465/	2022-11-03 23:53:18	2022-11-03 23:53:18	chlorophyllleaf-guard	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/465.svg
190	aipom	115	8	1	f	https://pokeapi.co/api/v2/pokemon/190/	2022-11-03 23:52:20	2022-11-04 00:02:52	run-awaypickup	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/190.svg
191	sunkern	18	3	1	f	https://pokeapi.co/api/v2/pokemon/191/	2022-11-03 23:52:21	2022-11-03 23:52:21	chlorophyllsolar-power	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/191.svg
201	unown	50	5	0	f	https://pokeapi.co/api/v2/pokemon/201/	2022-11-03 23:52:21	2022-11-03 23:52:21	levitate	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/201.svg
211	qwilfish	39	5	1	f	https://pokeapi.co/api/v2/pokemon/211/	2022-11-03 23:52:22	2022-11-03 23:52:22	poison-pointswift-swim	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/211.svg
221	piloswine	558	11	1	f	https://pokeapi.co/api/v2/pokemon/221/	2022-11-03 23:52:23	2022-11-03 23:52:23	oblivioussnow-cloak	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/221.svg
231	phanpy	335	5	1	f	https://pokeapi.co/api/v2/pokemon/231/	2022-11-03 23:52:24	2022-11-03 23:52:24	pickup	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/231.svg
241	miltank	755	12	1	f	https://pokeapi.co/api/v2/pokemon/241/	2022-11-03 23:52:25	2022-11-03 23:52:25	thick-fatscrappy	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/241.svg
251	celebi	50	6	0	f	https://pokeapi.co/api/v2/pokemon/251/	2022-11-03 23:52:26	2022-11-03 23:52:26	natural-cure	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/251.svg
261	poochyena	136	5	1	f	https://pokeapi.co/api/v2/pokemon/261/	2022-11-03 23:52:27	2022-11-03 23:52:27	run-awayquick-feet	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/261.svg
271	lombre	325	12	1	f	https://pokeapi.co/api/v2/pokemon/271/	2022-11-03 23:52:28	2022-11-03 23:52:28	swift-swimrain-dish	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/271.svg
281	kirlia	202	8	1	f	https://pokeapi.co/api/v2/pokemon/281/	2022-11-03 23:52:29	2022-11-03 23:52:29	synchronizetrace	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/281.svg
289	slaking	1305	20	0	f	https://pokeapi.co/api/v2/pokemon/289/	2022-11-03 23:52:32	2022-11-03 23:52:32	truant	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/289.svg
302	sableye	110	5	1	f	https://pokeapi.co/api/v2/pokemon/302/	2022-11-03 23:52:34	2022-11-03 23:52:34	keen-eyestall	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/302.svg
307	meditite	112	6	1	f	https://pokeapi.co/api/v2/pokemon/307/	2022-11-03 23:52:36	2022-11-03 23:52:36	pure-power	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/307.svg
321	wailord	3980	145	1	f	https://pokeapi.co/api/v2/pokemon/321/	2022-11-03 23:52:41	2022-11-03 23:52:41	water-veiloblivious	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/321.svg
324	torkoal	804	5	1	f	https://pokeapi.co/api/v2/pokemon/324/	2022-11-03 23:52:42	2022-11-03 23:52:42	white-smokedrought	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/324.svg
337	lunatone	1680	10	0	f	https://pokeapi.co/api/v2/pokemon/337/	2022-11-03 23:52:45	2022-11-03 23:52:45	levitate	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/337.svg
348	armaldo	682	15	1	f	https://pokeapi.co/api/v2/pokemon/348/	2022-11-03 23:52:48	2022-11-03 23:52:48	battle-armor	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/348.svg
366	clamperl	525	4	1	f	https://pokeapi.co/api/v2/pokemon/366/	2022-11-03 23:52:53	2022-11-03 23:52:53	shell-armor	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/366.svg
368	gorebyss	226	18	1	f	https://pokeapi.co/api/v2/pokemon/368/	2022-11-03 23:52:54	2022-11-03 23:52:54	swift-swim	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/368.svg
385	jirachi	11	3	0	f	https://pokeapi.co/api/v2/pokemon/385/	2022-11-03 23:52:59	2022-11-03 23:52:59	serene-grace	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/385.svg
389	torterra	3100	22	1	f	https://pokeapi.co/api/v2/pokemon/389/	2022-11-03 23:53:02	2022-11-03 23:53:02	overgrow	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/389.svg
398	staraptor	249	12	1	f	https://pokeapi.co/api/v2/pokemon/398/	2022-11-03 23:53:03	2022-11-03 23:53:03	intimidate	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/398.svg
412	burmy	34	2	1	f	https://pokeapi.co/api/v2/pokemon/412/	2022-11-03 23:53:06	2022-11-03 23:53:06	shed-skin	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/412.svg
418	buizel	295	7	1	f	https://pokeapi.co/api/v2/pokemon/418/	2022-11-03 23:53:07	2022-11-03 23:53:07	swift-swim	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/418.svg
428	lopunny	333	12	1	f	https://pokeapi.co/api/v2/pokemon/428/	2022-11-03 23:53:10	2022-11-03 23:53:10	cute-charmklutz	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/428.svg
432	purugly	438	10	1	f	https://pokeapi.co/api/v2/pokemon/432/	2022-11-03 23:53:11	2022-11-03 23:53:11	thick-fatown-tempo	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/432.svg
446	munchlax	1050	6	1	f	https://pokeapi.co/api/v2/pokemon/446/	2022-11-03 23:53:14	2022-11-03 23:53:14	pickupthick-fat	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/446.svg
459	snover	505	10	1	f	https://pokeapi.co/api/v2/pokemon/459/	2022-11-03 23:53:17	2022-11-03 23:53:17	snow-warning	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/459.svg
467	magmortar	680	16	1	f	https://pokeapi.co/api/v2/pokemon/467/	2022-11-03 23:53:18	2022-11-03 23:53:18	flame-body	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/467.svg
192	sunflora	85	8	1	f	https://pokeapi.co/api/v2/pokemon/192/	2022-11-03 23:52:21	2022-11-03 23:52:21	chlorophyllsolar-power	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/192.svg
202	wobbuffet	285	13	1	f	https://pokeapi.co/api/v2/pokemon/202/	2022-11-03 23:52:22	2022-11-03 23:52:22	shadow-tag	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/202.svg
212	scizor	1180	18	1	f	https://pokeapi.co/api/v2/pokemon/212/	2022-11-03 23:52:22	2022-11-03 23:52:22	swarmtechnician	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/212.svg
222	corsola	50	6	1	f	https://pokeapi.co/api/v2/pokemon/222/	2022-11-03 23:52:23	2022-11-03 23:52:23	hustlenatural-cure	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/222.svg
232	donphan	1200	11	1	f	https://pokeapi.co/api/v2/pokemon/232/	2022-11-03 23:52:24	2022-11-03 23:52:24	sturdy	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/232.svg
242	blissey	468	15	1	f	https://pokeapi.co/api/v2/pokemon/242/	2022-11-03 23:52:25	2022-11-03 23:52:25	natural-cureserene-grace	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/242.svg
252	treecko	50	5	1	f	https://pokeapi.co/api/v2/pokemon/252/	2022-11-03 23:52:26	2022-11-03 23:52:26	overgrow	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/252.svg
262	mightyena	370	10	1	f	https://pokeapi.co/api/v2/pokemon/262/	2022-11-03 23:52:27	2022-11-03 23:52:27	intimidatequick-feet	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/262.svg
272	ludicolo	550	15	1	f	https://pokeapi.co/api/v2/pokemon/272/	2022-11-03 23:52:28	2022-11-03 23:52:28	swift-swimrain-dish	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/272.svg
282	gardevoir	484	16	1	f	https://pokeapi.co/api/v2/pokemon/282/	2022-11-03 23:52:29	2022-11-03 23:52:29	synchronizetrace	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/282.svg
290	nincada	55	5	1	f	https://pokeapi.co/api/v2/pokemon/290/	2022-11-03 23:52:32	2022-11-03 23:52:32	compound-eyes	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/290.svg
303	mawile	115	6	1	f	https://pokeapi.co/api/v2/pokemon/303/	2022-11-03 23:52:35	2022-11-03 23:52:35	hyper-cutterintimidate	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/303.svg
310	manectric	402	15	1	f	https://pokeapi.co/api/v2/pokemon/310/	2022-11-03 23:52:38	2022-11-03 23:52:38	staticlightning-rod	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/310.svg
320	wailmer	1300	20	1	f	https://pokeapi.co/api/v2/pokemon/320/	2022-11-03 23:52:41	2022-11-03 23:52:41	water-veiloblivious	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/320.svg
323	camerupt	2200	19	1	f	https://pokeapi.co/api/v2/pokemon/323/	2022-11-03 23:52:42	2022-11-03 23:52:42	magma-armorsolid-rock	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/323.svg
336	seviper	525	27	1	f	https://pokeapi.co/api/v2/pokemon/336/	2022-11-03 23:52:45	2022-11-03 23:52:45	shed-skin	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/336.svg
349	feebas	74	6	1	f	https://pokeapi.co/api/v2/pokemon/349/	2022-11-03 23:52:48	2022-11-03 23:52:48	swift-swimoblivious	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/349.svg
359	absol	470	12	1	f	https://pokeapi.co/api/v2/pokemon/359/	2022-11-03 23:52:51	2022-11-03 23:52:51	pressuresuper-luck	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/359.svg
363	spheal	395	8	1	f	https://pokeapi.co/api/v2/pokemon/363/	2022-11-03 23:52:52	2022-11-03 23:52:52	thick-fatice-body	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/363.svg
372	shelgon	1105	11	1	f	https://pokeapi.co/api/v2/pokemon/372/	2022-11-03 23:52:55	2022-11-03 23:52:55	rock-head	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/372.svg
378	regice	1750	18	1	f	https://pokeapi.co/api/v2/pokemon/378/	2022-11-03 23:52:58	2022-11-03 23:52:58	clear-body	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/378.svg
392	infernape	550	12	1	f	https://pokeapi.co/api/v2/pokemon/392/	2022-11-03 23:53:03	2022-11-03 23:53:03	blaze	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/392.svg
402	kricketune	255	10	1	f	https://pokeapi.co/api/v2/pokemon/402/	2022-11-03 23:53:04	2022-11-03 23:53:04	swarm	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/402.svg
409	rampardos	1025	16	1	f	https://pokeapi.co/api/v2/pokemon/409/	2022-11-03 23:53:05	2022-11-03 23:53:05	mold-breaker	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/409.svg
433	chingling	6	2	0	f	https://pokeapi.co/api/v2/pokemon/433/	2022-11-03 23:53:12	2022-11-03 23:53:12	levitate	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/433.svg
442	spiritomb	1080	10	1	f	https://pokeapi.co/api/v2/pokemon/442/	2022-11-03 23:53:13	2022-11-03 23:53:13	pressure	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/442.svg
455	carnivine	270	14	0	f	https://pokeapi.co/api/v2/pokemon/455/	2022-11-03 23:53:16	2022-11-03 23:53:16	levitate	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/455.svg
463	lickilicky	1400	17	1	f	https://pokeapi.co/api/v2/pokemon/463/	2022-11-03 23:53:17	2022-11-03 23:53:17	own-tempooblivious	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/463.svg
193	yanma	380	12	1	f	https://pokeapi.co/api/v2/pokemon/193/	2022-11-03 23:52:21	2022-11-03 23:52:21	speed-boostcompound-eyes	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/193.svg
203	girafarig	415	15	1	f	https://pokeapi.co/api/v2/pokemon/203/	2022-11-03 23:52:22	2022-11-03 23:52:22	inner-focusearly-bird	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/203.svg
213	shuckle	205	6	1	f	https://pokeapi.co/api/v2/pokemon/213/	2022-11-03 23:52:23	2022-11-03 23:52:23	sturdygluttony	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/213.svg
223	remoraid	120	6	1	f	https://pokeapi.co/api/v2/pokemon/223/	2022-11-03 23:52:23	2022-11-03 23:52:23	hustlesniper	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/223.svg
233	porygon2	325	6	1	f	https://pokeapi.co/api/v2/pokemon/233/	2022-11-03 23:52:24	2022-11-03 23:52:24	tracedownload	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/233.svg
243	raikou	1780	19	1	f	https://pokeapi.co/api/v2/pokemon/243/	2022-11-03 23:52:25	2022-11-03 23:52:25	pressure	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/243.svg
253	grovyle	216	9	1	f	https://pokeapi.co/api/v2/pokemon/253/	2022-11-03 23:52:26	2022-11-03 23:52:26	overgrow	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/253.svg
263	zigzagoon	175	4	1	f	https://pokeapi.co/api/v2/pokemon/263/	2022-11-03 23:52:27	2022-11-03 23:52:27	pickupgluttony	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/263.svg
273	seedot	40	5	1	f	https://pokeapi.co/api/v2/pokemon/273/	2022-11-03 23:52:28	2022-11-03 23:52:28	chlorophyllearly-bird	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/273.svg
283	surskit	17	5	1	f	https://pokeapi.co/api/v2/pokemon/283/	2022-11-03 23:52:29	2022-11-03 23:52:29	swift-swim	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/283.svg
291	ninjask	120	8	1	f	https://pokeapi.co/api/v2/pokemon/291/	2022-11-03 23:52:32	2022-11-03 23:52:32	speed-boost	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/291.svg
305	lairon	1200	9	1	f	https://pokeapi.co/api/v2/pokemon/305/	2022-11-03 23:52:35	2022-11-03 23:52:35	sturdyrock-head	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/305.svg
312	minun	42	4	1	f	https://pokeapi.co/api/v2/pokemon/312/	2022-11-03 23:52:38	2022-11-03 23:52:38	minus	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/312.svg
326	grumpig	715	9	1	f	https://pokeapi.co/api/v2/pokemon/326/	2022-11-03 23:52:43	2022-11-03 23:52:43	thick-fatown-tempo	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/326.svg
330	flygon	820	20	0	f	https://pokeapi.co/api/v2/pokemon/330/	2022-11-03 23:52:44	2022-11-03 23:52:44	levitate	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/330.svg
344	claydol	1080	15	0	f	https://pokeapi.co/api/v2/pokemon/344/	2022-11-03 23:52:47	2022-11-03 23:52:47	levitate	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/344.svg
351	castform	8	3	0	f	https://pokeapi.co/api/v2/pokemon/351/	2022-11-03 23:52:48	2022-11-03 23:52:48	forecast	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/351.svg
365	walrein	1506	14	1	f	https://pokeapi.co/api/v2/pokemon/365/	2022-11-03 23:52:53	2022-11-03 23:52:53	thick-fatice-body	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/365.svg
367	huntail	270	17	1	f	https://pokeapi.co/api/v2/pokemon/367/	2022-11-03 23:52:54	2022-11-03 23:52:54	swift-swim	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/367.svg
384	rayquaza	2065	70	0	f	https://pokeapi.co/api/v2/pokemon/384/	2022-11-03 23:52:59	2022-11-03 23:52:59	air-lock	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/384.svg
388	grotle	970	11	1	f	https://pokeapi.co/api/v2/pokemon/388/	2022-11-03 23:53:02	2022-11-03 23:53:02	overgrow	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/388.svg
397	staravia	155	6	1	f	https://pokeapi.co/api/v2/pokemon/397/	2022-11-03 23:53:03	2022-11-03 23:53:03	intimidate	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/397.svg
411	bastiodon	1495	13	1	f	https://pokeapi.co/api/v2/pokemon/411/	2022-11-03 23:53:05	2022-11-03 23:53:05	sturdy	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/411.svg
417	pachirisu	39	4	1	f	https://pokeapi.co/api/v2/pokemon/417/	2022-11-03 23:53:07	2022-11-03 23:53:07	run-awaypickup	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/417.svg
426	drifblim	150	12	1	f	https://pokeapi.co/api/v2/pokemon/426/	2022-11-03 23:53:09	2022-11-03 23:53:09	aftermathunburden	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/426.svg
430	honchkrow	273	9	1	f	https://pokeapi.co/api/v2/pokemon/430/	2022-11-03 23:53:11	2022-11-03 23:53:11	insomniasuper-luck	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/430.svg
439	mime-jr	130	6	1	f	https://pokeapi.co/api/v2/pokemon/439/	2022-11-03 23:53:12	2022-11-03 23:53:12	soundprooffilter	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/439.svg
450	hippowdon	3000	20	1	f	https://pokeapi.co/api/v2/pokemon/450/	2022-11-03 23:53:15	2022-11-03 23:53:15	sand-stream	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/450.svg
464	rhyperior	2828	24	1	f	https://pokeapi.co/api/v2/pokemon/464/	2022-11-03 23:53:18	2022-11-03 23:53:18	lightning-rodsolid-rock	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/464.svg
194	wooper	85	4	1	f	https://pokeapi.co/api/v2/pokemon/194/	2022-11-03 23:52:21	2022-11-03 23:52:21	dampwater-absorb	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/194.svg
204	pineco	72	6	1	f	https://pokeapi.co/api/v2/pokemon/204/	2022-11-03 23:52:22	2022-11-03 23:52:22	sturdy	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/204.svg
214	heracross	540	15	1	f	https://pokeapi.co/api/v2/pokemon/214/	2022-11-03 23:52:23	2022-11-03 23:52:23	swarmguts	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/214.svg
224	octillery	285	9	1	f	https://pokeapi.co/api/v2/pokemon/224/	2022-11-03 23:52:24	2022-11-03 23:52:24	suction-cupssniper	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/224.svg
234	stantler	712	14	1	f	https://pokeapi.co/api/v2/pokemon/234/	2022-11-03 23:52:24	2022-11-03 23:52:24	intimidatefrisk	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/234.svg
244	entei	1980	21	1	f	https://pokeapi.co/api/v2/pokemon/244/	2022-11-03 23:52:25	2022-11-03 23:52:25	pressure	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/244.svg
254	sceptile	522	17	1	f	https://pokeapi.co/api/v2/pokemon/254/	2022-11-03 23:52:26	2022-11-03 23:52:26	overgrow	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/254.svg
264	linoone	325	5	1	f	https://pokeapi.co/api/v2/pokemon/264/	2022-11-03 23:52:27	2022-11-03 23:52:27	pickupgluttony	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/264.svg
274	nuzleaf	280	10	1	f	https://pokeapi.co/api/v2/pokemon/274/	2022-11-03 23:52:28	2022-11-03 23:52:28	chlorophyllearly-bird	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/274.svg
288	vigoroth	465	14	0	f	https://pokeapi.co/api/v2/pokemon/288/	2022-11-03 23:52:31	2022-11-03 23:52:31	vital-spirit	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/288.svg
298	azurill	20	2	1	f	https://pokeapi.co/api/v2/pokemon/298/	2022-11-03 23:52:34	2022-11-03 23:52:34	thick-fathuge-power	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/298.svg
319	sharpedo	888	18	1	f	https://pokeapi.co/api/v2/pokemon/319/	2022-11-03 23:52:41	2022-11-03 23:52:41	rough-skin	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/319.svg
322	numel	240	7	1	f	https://pokeapi.co/api/v2/pokemon/322/	2022-11-03 23:52:42	2022-11-03 23:52:42	oblivioussimple	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/322.svg
335	zangoose	403	13	1	f	https://pokeapi.co/api/v2/pokemon/335/	2022-11-03 23:52:45	2022-11-03 23:52:45	immunity	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/335.svg
342	crawdaunt	328	11	1	f	https://pokeapi.co/api/v2/pokemon/342/	2022-11-03 23:52:46	2022-11-03 23:52:46	hyper-cuttershell-armor	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/342.svg
353	shuppet	23	6	1	f	https://pokeapi.co/api/v2/pokemon/353/	2022-11-03 23:52:49	2022-11-03 23:52:49	insomniafrisk	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/353.svg
358	chimecho	10	6	0	f	https://pokeapi.co/api/v2/pokemon/358/	2022-11-03 23:52:50	2022-11-03 23:52:50	levitate	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/358.svg
369	relicanth	234	10	1	f	https://pokeapi.co/api/v2/pokemon/369/	2022-11-03 23:52:54	2022-11-03 23:52:54	swift-swimrock-head	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/369.svg
376	metagross	5500	16	1	f	https://pokeapi.co/api/v2/pokemon/376/	2022-11-03 23:52:57	2022-11-03 23:52:57	clear-body	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/376.svg
380	latias	400	14	0	f	https://pokeapi.co/api/v2/pokemon/380/	2022-11-03 23:52:58	2022-11-03 23:52:58	levitate	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/380.svg
395	empoleon	845	17	1	f	https://pokeapi.co/api/v2/pokemon/395/	2022-11-03 23:53:03	2022-11-03 23:53:03	torrent	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/395.svg
405	luxray	420	14	1	f	https://pokeapi.co/api/v2/pokemon/405/	2022-11-03 23:53:04	2022-11-03 23:53:04	rivalryintimidate	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/405.svg
416	vespiquen	385	12	1	f	https://pokeapi.co/api/v2/pokemon/416/	2022-11-03 23:53:07	2022-11-03 23:53:07	pressure	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/416.svg
425	drifloon	12	4	1	f	https://pokeapi.co/api/v2/pokemon/425/	2022-11-03 23:53:09	2022-11-03 23:53:09	aftermathunburden	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/425.svg
438	bonsly	150	5	1	f	https://pokeapi.co/api/v2/pokemon/438/	2022-11-03 23:53:12	2022-11-03 23:53:12	sturdyrock-head	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/438.svg
449	hippopotas	495	8	1	f	https://pokeapi.co/api/v2/pokemon/449/	2022-11-03 23:53:15	2022-11-03 23:53:15	sand-stream	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/449.svg
457	lumineon	240	12	1	f	https://pokeapi.co/api/v2/pokemon/457/	2022-11-03 23:53:16	2022-11-03 23:53:16	swift-swimstorm-drain	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/457.svg
195	quagsire	750	14	1	f	https://pokeapi.co/api/v2/pokemon/195/	2022-11-03 23:52:21	2022-11-03 23:52:21	dampwater-absorb	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/195.svg
205	forretress	1258	12	1	f	https://pokeapi.co/api/v2/pokemon/205/	2022-11-03 23:52:22	2022-11-03 23:52:22	sturdy	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/205.svg
215	sneasel	280	9	1	f	https://pokeapi.co/api/v2/pokemon/215/	2022-11-03 23:52:23	2022-11-03 23:52:23	inner-focuskeen-eye	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/215.svg
225	delibird	160	9	1	f	https://pokeapi.co/api/v2/pokemon/225/	2022-11-03 23:52:24	2022-11-03 23:52:24	vital-spirithustle	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/225.svg
235	smeargle	580	12	1	f	https://pokeapi.co/api/v2/pokemon/235/	2022-11-03 23:52:24	2022-11-03 23:52:24	own-tempotechnician	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/235.svg
245	suicune	1870	20	1	f	https://pokeapi.co/api/v2/pokemon/245/	2022-11-03 23:52:25	2022-11-03 23:52:25	pressure	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/245.svg
255	torchic	25	4	1	f	https://pokeapi.co/api/v2/pokemon/255/	2022-11-03 23:52:26	2022-11-03 23:52:26	blaze	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/255.svg
265	wurmple	36	3	1	f	https://pokeapi.co/api/v2/pokemon/265/	2022-11-03 23:52:27	2022-11-03 23:52:27	shield-dust	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/265.svg
275	shiftry	596	13	1	f	https://pokeapi.co/api/v2/pokemon/275/	2022-11-03 23:52:28	2022-11-03 23:52:28	chlorophyllearly-bird	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/275.svg
294	loudred	405	10	1	f	https://pokeapi.co/api/v2/pokemon/294/	2022-11-03 23:52:33	2022-11-03 23:52:33	soundproof	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/294.svg
299	nosepass	970	10	1	f	https://pokeapi.co/api/v2/pokemon/299/	2022-11-03 23:52:34	2022-11-03 23:52:34	sturdymagnet-pull	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/299.svg
316	gulpin	103	4	1	f	https://pokeapi.co/api/v2/pokemon/316/	2022-11-03 23:52:39	2022-11-03 23:52:39	liquid-oozesticky-hold	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/316.svg
318	carvanha	208	8	1	f	https://pokeapi.co/api/v2/pokemon/318/	2022-11-03 23:52:40	2022-11-03 23:52:40	rough-skin	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/318.svg
328	trapinch	150	7	1	f	https://pokeapi.co/api/v2/pokemon/328/	2022-11-03 23:52:43	2022-11-03 23:52:43	hyper-cutterarena-trap	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/328.svg
338	solrock	1540	12	0	f	https://pokeapi.co/api/v2/pokemon/338/	2022-11-03 23:52:46	2022-11-03 23:52:46	levitate	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/338.svg
346	cradily	604	15	1	f	https://pokeapi.co/api/v2/pokemon/346/	2022-11-03 23:52:47	2022-11-03 23:52:47	suction-cups	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/346.svg
356	dusclops	306	16	1	f	https://pokeapi.co/api/v2/pokemon/356/	2022-11-03 23:52:50	2022-11-03 23:52:50	pressure	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/356.svg
383	groudon	9500	35	0	f	https://pokeapi.co/api/v2/pokemon/383/	2022-11-03 23:52:59	2022-11-03 23:52:59	drought	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/383.svg
387	turtwig	102	4	1	f	https://pokeapi.co/api/v2/pokemon/387/	2022-11-03 23:53:01	2022-11-03 23:53:01	overgrow	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/387.svg
396	starly	20	3	1	f	https://pokeapi.co/api/v2/pokemon/396/	2022-11-03 23:53:03	2022-11-03 23:53:03	keen-eye	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/396.svg
413	wormadam-plant	65	5	1	f	https://pokeapi.co/api/v2/pokemon/413/	2022-11-03 23:53:06	2022-11-03 23:53:06	anticipation	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/413.svg
419	floatzel	335	11	1	f	https://pokeapi.co/api/v2/pokemon/419/	2022-11-03 23:53:07	2022-11-03 23:53:07	swift-swim	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/419.svg
434	stunky	192	4	1	f	https://pokeapi.co/api/v2/pokemon/434/	2022-11-03 23:53:12	2022-11-03 23:53:12	stenchaftermath	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/434.svg
443	gible	205	7	1	f	https://pokeapi.co/api/v2/pokemon/443/	2022-11-03 23:53:13	2022-11-03 23:53:13	sand-veil	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/443.svg
452	drapion	615	13	1	f	https://pokeapi.co/api/v2/pokemon/452/	2022-11-03 23:53:15	2022-11-03 23:53:15	battle-armorsniper	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/452.svg
460	abomasnow	1355	22	1	f	https://pokeapi.co/api/v2/pokemon/460/	2022-11-03 23:53:17	2022-11-03 23:53:17	snow-warning	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/460.svg
196	espeon	265	9	1	f	https://pokeapi.co/api/v2/pokemon/196/	2022-11-03 23:52:21	2022-11-03 23:52:21	synchronize	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/196.svg
206	dunsparce	140	15	1	f	https://pokeapi.co/api/v2/pokemon/206/	2022-11-03 23:52:22	2022-11-03 23:52:22	serene-gracerun-away	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/206.svg
216	teddiursa	88	6	1	f	https://pokeapi.co/api/v2/pokemon/216/	2022-11-03 23:52:23	2022-11-03 23:52:23	pickupquick-feet	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/216.svg
226	mantine	2200	21	1	f	https://pokeapi.co/api/v2/pokemon/226/	2022-11-03 23:52:24	2022-11-03 23:52:24	swift-swimwater-absorb	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/226.svg
236	tyrogue	210	7	1	f	https://pokeapi.co/api/v2/pokemon/236/	2022-11-03 23:52:25	2022-11-03 23:52:25	gutssteadfast	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/236.svg
246	larvitar	720	6	1	f	https://pokeapi.co/api/v2/pokemon/246/	2022-11-03 23:52:25	2022-11-03 23:52:25	guts	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/246.svg
256	combusken	195	9	1	f	https://pokeapi.co/api/v2/pokemon/256/	2022-11-03 23:52:26	2022-11-03 23:52:26	blaze	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/256.svg
266	silcoon	100	6	0	f	https://pokeapi.co/api/v2/pokemon/266/	2022-11-03 23:52:27	2022-11-03 23:52:27	shed-skin	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/266.svg
276	taillow	23	3	1	f	https://pokeapi.co/api/v2/pokemon/276/	2022-11-03 23:52:28	2022-11-03 23:52:28	guts	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/276.svg
286	breloom	392	12	1	f	https://pokeapi.co/api/v2/pokemon/286/	2022-11-03 23:52:31	2022-11-03 23:52:31	effect-sporepoison-heal	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/286.svg
292	shedinja	12	8	0	f	https://pokeapi.co/api/v2/pokemon/292/	2022-11-03 23:52:32	2022-11-03 23:52:32	wonder-guard	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/292.svg
304	aron	600	4	1	f	https://pokeapi.co/api/v2/pokemon/304/	2022-11-03 23:52:35	2022-11-03 23:52:35	sturdyrock-head	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/304.svg
311	plusle	42	4	1	f	https://pokeapi.co/api/v2/pokemon/311/	2022-11-03 23:52:38	2022-11-03 23:52:38	plus	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/311.svg
327	spinda	50	11	1	f	https://pokeapi.co/api/v2/pokemon/327/	2022-11-03 23:52:43	2022-11-03 23:52:43	own-tempotangled-feet	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/327.svg
331	cacnea	513	4	1	f	https://pokeapi.co/api/v2/pokemon/331/	2022-11-03 23:52:44	2022-11-03 23:52:44	sand-veil	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/331.svg
343	baltoy	215	5	0	f	https://pokeapi.co/api/v2/pokemon/343/	2022-11-03 23:52:47	2022-11-03 23:52:47	levitate	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/343.svg
350	milotic	1620	62	1	f	https://pokeapi.co/api/v2/pokemon/350/	2022-11-03 23:52:48	2022-11-03 23:52:48	marvel-scalecompetitive	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/350.svg
360	wynaut	140	6	1	f	https://pokeapi.co/api/v2/pokemon/360/	2022-11-03 23:52:51	2022-11-03 23:52:51	shadow-tag	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/360.svg
364	sealeo	876	11	1	f	https://pokeapi.co/api/v2/pokemon/364/	2022-11-03 23:52:52	2022-11-03 23:52:52	thick-fatice-body	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/364.svg
375	metang	2025	12	1	f	https://pokeapi.co/api/v2/pokemon/375/	2022-11-03 23:52:56	2022-11-03 23:52:56	clear-body	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/375.svg
379	registeel	2050	19	1	f	https://pokeapi.co/api/v2/pokemon/379/	2022-11-03 23:52:58	2022-11-03 23:52:58	clear-body	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/379.svg
394	prinplup	230	8	1	f	https://pokeapi.co/api/v2/pokemon/394/	2022-11-03 23:53:03	2022-11-03 23:53:03	torrent	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/394.svg
404	luxio	305	9	1	f	https://pokeapi.co/api/v2/pokemon/404/	2022-11-03 23:53:04	2022-11-03 23:53:04	rivalryintimidate	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/404.svg
415	combee	55	3	1	f	https://pokeapi.co/api/v2/pokemon/415/	2022-11-03 23:53:07	2022-11-03 23:53:07	honey-gather	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/415.svg
429	mismagius	44	9	0	f	https://pokeapi.co/api/v2/pokemon/429/	2022-11-03 23:53:10	2022-11-03 23:53:10	levitate	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/429.svg
441	chatot	19	5	1	f	https://pokeapi.co/api/v2/pokemon/441/	2022-11-03 23:53:13	2022-11-03 23:53:13	keen-eyetangled-feet	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/441.svg
447	riolu	202	7	1	f	https://pokeapi.co/api/v2/pokemon/447/	2022-11-03 23:53:14	2022-11-03 23:53:14	steadfastinner-focus	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/447.svg
458	mantyke	650	10	1	f	https://pokeapi.co/api/v2/pokemon/458/	2022-11-03 23:53:16	2022-11-03 23:53:16	swift-swimwater-absorb	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/458.svg
466	electivire	1386	18	1	f	https://pokeapi.co/api/v2/pokemon/466/	2022-11-03 23:53:18	2022-11-03 23:53:18	motor-drive	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/466.svg
197	umbreon	270	10	1	f	https://pokeapi.co/api/v2/pokemon/197/	2022-11-03 23:52:21	2022-11-03 23:52:21	synchronize	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/197.svg
207	gligar	648	11	1	f	https://pokeapi.co/api/v2/pokemon/207/	2022-11-03 23:52:22	2022-11-03 23:52:22	hyper-cuttersand-veil	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/207.svg
217	ursaring	1258	18	1	f	https://pokeapi.co/api/v2/pokemon/217/	2022-11-03 23:52:23	2022-11-03 23:52:23	gutsquick-feet	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/217.svg
227	skarmory	505	17	1	f	https://pokeapi.co/api/v2/pokemon/227/	2022-11-03 23:52:24	2022-11-03 23:52:24	keen-eyesturdy	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/227.svg
237	hitmontop	480	14	1	f	https://pokeapi.co/api/v2/pokemon/237/	2022-11-03 23:52:25	2022-11-03 23:52:25	intimidatetechnician	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/237.svg
247	pupitar	1520	12	0	f	https://pokeapi.co/api/v2/pokemon/247/	2022-11-03 23:52:26	2022-11-03 23:52:26	shed-skin	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/247.svg
257	blaziken	520	19	1	f	https://pokeapi.co/api/v2/pokemon/257/	2022-11-03 23:52:26	2022-11-03 23:52:26	blaze	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/257.svg
267	beautifly	284	10	1	f	https://pokeapi.co/api/v2/pokemon/267/	2022-11-03 23:52:27	2022-11-03 23:52:27	swarm	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/267.svg
277	swellow	198	7	1	f	https://pokeapi.co/api/v2/pokemon/277/	2022-11-03 23:52:28	2022-11-03 23:52:28	guts	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/277.svg
287	slakoth	240	8	0	f	https://pokeapi.co/api/v2/pokemon/287/	2022-11-03 23:52:31	2022-11-03 23:52:31	truant	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/287.svg
293	whismur	163	6	1	f	https://pokeapi.co/api/v2/pokemon/293/	2022-11-03 23:52:32	2022-11-03 23:52:32	soundproof	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/293.svg
308	medicham	315	13	1	f	https://pokeapi.co/api/v2/pokemon/308/	2022-11-03 23:52:37	2022-11-03 23:52:37	pure-power	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/308.svg
313	volbeat	177	7	1	f	https://pokeapi.co/api/v2/pokemon/313/	2022-11-03 23:52:38	2022-11-03 23:52:38	illuminateswarm	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/313.svg
334	altaria	206	11	1	f	https://pokeapi.co/api/v2/pokemon/334/	2022-11-03 23:52:45	2022-11-03 23:52:45	natural-cure	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/334.svg
341	corphish	115	6	1	f	https://pokeapi.co/api/v2/pokemon/341/	2022-11-03 23:52:46	2022-11-03 23:52:46	hyper-cuttershell-armor	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/341.svg
355	duskull	150	8	1	f	https://pokeapi.co/api/v2/pokemon/355/	2022-11-03 23:52:49	2022-11-03 23:52:49	levitate	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/355.svg
361	snorunt	168	7	1	f	https://pokeapi.co/api/v2/pokemon/361/	2022-11-03 23:52:52	2022-11-03 23:52:52	inner-focusice-body	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/361.svg
377	regirock	2300	17	1	f	https://pokeapi.co/api/v2/pokemon/377/	2022-11-03 23:52:57	2022-11-03 23:52:57	clear-body	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/377.svg
381	latios	600	20	0	f	https://pokeapi.co/api/v2/pokemon/381/	2022-11-03 23:52:58	2022-11-03 23:52:58	levitate	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/381.svg
386	deoxys-normal	608	17	0	f	https://pokeapi.co/api/v2/pokemon/386/	2022-11-03 23:53:01	2022-11-03 23:53:01	pressure	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/386.svg
399	bidoof	200	5	1	f	https://pokeapi.co/api/v2/pokemon/399/	2022-11-03 23:53:03	2022-11-03 23:53:03	simpleunaware	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/399.svg
406	budew	12	2	1	f	https://pokeapi.co/api/v2/pokemon/406/	2022-11-03 23:53:04	2022-11-03 23:53:04	natural-curepoison-point	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/406.svg
414	mothim	233	9	1	f	https://pokeapi.co/api/v2/pokemon/414/	2022-11-03 23:53:06	2022-11-03 23:53:06	swarm	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/414.svg
422	shellos	63	3	1	f	https://pokeapi.co/api/v2/pokemon/422/	2022-11-03 23:53:08	2022-11-03 23:53:08	sticky-holdstorm-drain	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/422.svg
436	bronzor	605	5	1	f	https://pokeapi.co/api/v2/pokemon/436/	2022-11-03 23:53:12	2022-11-03 23:53:12	levitateheatproof	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/436.svg
445	garchomp	950	19	1	f	https://pokeapi.co/api/v2/pokemon/445/	2022-11-03 23:53:13	2022-11-03 23:53:13	sand-veil	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/445.svg
453	croagunk	230	7	1	f	https://pokeapi.co/api/v2/pokemon/453/	2022-11-03 23:53:16	2022-11-03 23:53:16	anticipationdry-skin	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/453.svg
461	weavile	340	11	1	f	https://pokeapi.co/api/v2/pokemon/461/	2022-11-03 23:53:17	2022-11-03 23:53:17	pressure	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/461.svg
198	murkrow	21	5	1	f	https://pokeapi.co/api/v2/pokemon/198/	2022-11-03 23:52:21	2022-11-03 23:52:21	insomniasuper-luck	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/198.svg
208	steelix	4000	92	1	f	https://pokeapi.co/api/v2/pokemon/208/	2022-11-03 23:52:22	2022-11-03 23:52:22	rock-headsturdy	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/208.svg
218	slugma	350	7	1	f	https://pokeapi.co/api/v2/pokemon/218/	2022-11-03 23:52:23	2022-11-03 23:52:23	magma-armorflame-body	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/218.svg
228	houndour	108	6	1	f	https://pokeapi.co/api/v2/pokemon/228/	2022-11-03 23:52:24	2022-11-03 23:52:24	early-birdflash-fire	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/228.svg
238	smoochum	60	4	1	f	https://pokeapi.co/api/v2/pokemon/238/	2022-11-03 23:52:25	2022-11-03 23:52:25	obliviousforewarn	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/238.svg
248	tyranitar	2020	20	1	f	https://pokeapi.co/api/v2/pokemon/248/	2022-11-03 23:52:26	2022-11-03 23:52:26	sand-stream	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/248.svg
258	mudkip	76	4	1	f	https://pokeapi.co/api/v2/pokemon/258/	2022-11-03 23:52:27	2022-11-03 23:52:27	torrent	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/258.svg
268	cascoon	115	7	0	f	https://pokeapi.co/api/v2/pokemon/268/	2022-11-03 23:52:28	2022-11-03 23:52:28	shed-skin	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/268.svg
278	wingull	95	6	1	f	https://pokeapi.co/api/v2/pokemon/278/	2022-11-03 23:52:28	2022-11-03 23:52:28	keen-eyehydration	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/278.svg
297	hariyama	2538	23	1	f	https://pokeapi.co/api/v2/pokemon/297/	2022-11-03 23:52:33	2022-11-03 23:52:33	thick-fatguts	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/297.svg
306	aggron	3600	21	1	f	https://pokeapi.co/api/v2/pokemon/306/	2022-11-03 23:52:36	2022-11-03 23:52:36	sturdyrock-head	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/306.svg
325	spoink	306	7	1	f	https://pokeapi.co/api/v2/pokemon/325/	2022-11-03 23:52:43	2022-11-03 23:52:43	thick-fatown-tempo	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/325.svg
329	vibrava	153	11	0	f	https://pokeapi.co/api/v2/pokemon/329/	2022-11-03 23:52:44	2022-11-03 23:52:44	levitate	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/329.svg
345	lileep	238	10	1	f	https://pokeapi.co/api/v2/pokemon/345/	2022-11-03 23:52:47	2022-11-03 23:52:47	suction-cups	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/345.svg
352	kecleon	220	10	1	f	https://pokeapi.co/api/v2/pokemon/352/	2022-11-03 23:52:48	2022-11-03 23:52:48	color-change	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/352.svg
391	monferno	220	9	1	f	https://pokeapi.co/api/v2/pokemon/391/	2022-11-03 23:53:03	2022-11-03 23:53:03	blaze	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/391.svg
401	kricketot	22	3	1	f	https://pokeapi.co/api/v2/pokemon/401/	2022-11-03 23:53:04	2022-11-03 23:53:04	shed-skin	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/401.svg
408	cranidos	315	9	1	f	https://pokeapi.co/api/v2/pokemon/408/	2022-11-03 23:53:05	2022-11-03 23:53:05	mold-breaker	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/408.svg
421	cherrim	93	5	0	f	https://pokeapi.co/api/v2/pokemon/421/	2022-11-03 23:53:08	2022-11-03 23:53:08	flower-gift	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/421.svg
424	ambipom	203	12	1	f	https://pokeapi.co/api/v2/pokemon/424/	2022-11-03 23:53:09	2022-11-03 23:53:09	technicianpickup	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/424.svg
437	bronzong	1870	13	1	f	https://pokeapi.co/api/v2/pokemon/437/	2022-11-03 23:53:12	2022-11-03 23:53:12	levitateheatproof	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/437.svg
448	lucario	540	12	1	f	https://pokeapi.co/api/v2/pokemon/448/	2022-11-03 23:53:15	2022-11-03 23:53:15	steadfastinner-focus	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/448.svg
456	finneon	70	4	1	f	https://pokeapi.co/api/v2/pokemon/456/	2022-11-03 23:53:16	2022-11-03 23:53:16	swift-swimstorm-drain	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/456.svg
199	slowking	795	20	1	f	https://pokeapi.co/api/v2/pokemon/199/	2022-11-03 23:52:21	2022-11-03 23:52:21	obliviousown-tempo	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/199.svg
209	snubbull	78	6	1	f	https://pokeapi.co/api/v2/pokemon/209/	2022-11-03 23:52:22	2022-11-03 23:52:22	intimidaterun-away	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/209.svg
219	magcargo	550	8	1	f	https://pokeapi.co/api/v2/pokemon/219/	2022-11-03 23:52:23	2022-11-03 23:52:23	magma-armorflame-body	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/219.svg
229	houndoom	350	14	1	f	https://pokeapi.co/api/v2/pokemon/229/	2022-11-03 23:52:24	2022-11-03 23:52:24	early-birdflash-fire	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/229.svg
239	elekid	235	6	1	f	https://pokeapi.co/api/v2/pokemon/239/	2022-11-03 23:52:25	2022-11-03 23:52:25	static	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/239.svg
249	lugia	2160	52	1	f	https://pokeapi.co/api/v2/pokemon/249/	2022-11-03 23:52:26	2022-11-03 23:52:26	pressure	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/249.svg
259	marshtomp	280	7	1	f	https://pokeapi.co/api/v2/pokemon/259/	2022-11-03 23:52:27	2022-11-03 23:52:27	torrent	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/259.svg
269	dustox	316	12	1	f	https://pokeapi.co/api/v2/pokemon/269/	2022-11-03 23:52:28	2022-11-03 23:52:28	shield-dust	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/269.svg
279	pelipper	280	12	1	f	https://pokeapi.co/api/v2/pokemon/279/	2022-11-03 23:52:28	2022-11-03 23:52:28	keen-eyedrizzle	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/279.svg
284	masquerain	36	8	1	f	https://pokeapi.co/api/v2/pokemon/284/	2022-11-03 23:52:30	2022-11-03 23:52:30	intimidate	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/284.svg
295	exploud	840	15	1	f	https://pokeapi.co/api/v2/pokemon/295/	2022-11-03 23:52:33	2022-11-03 23:52:33	soundproof	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/295.svg
300	skitty	110	6	1	f	https://pokeapi.co/api/v2/pokemon/300/	2022-11-03 23:52:34	2022-11-03 23:52:34	cute-charmnormalize	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/300.svg
309	electrike	152	6	1	f	https://pokeapi.co/api/v2/pokemon/309/	2022-11-03 23:52:37	2022-11-03 23:52:37	staticlightning-rod	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/309.svg
314	illumise	177	6	1	f	https://pokeapi.co/api/v2/pokemon/314/	2022-11-03 23:52:38	2022-11-03 23:52:38	oblivioustinted-lens	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/314.svg
332	cacturne	774	13	1	f	https://pokeapi.co/api/v2/pokemon/332/	2022-11-03 23:52:44	2022-11-03 23:52:44	sand-veil	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/332.svg
339	barboach	19	4	1	f	https://pokeapi.co/api/v2/pokemon/339/	2022-11-03 23:52:46	2022-11-03 23:52:46	obliviousanticipation	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/339.svg
347	anorith	125	7	1	f	https://pokeapi.co/api/v2/pokemon/347/	2022-11-03 23:52:47	2022-11-03 23:52:47	battle-armor	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/347.svg
357	tropius	1000	20	1	f	https://pokeapi.co/api/v2/pokemon/357/	2022-11-03 23:52:50	2022-11-03 23:52:50	chlorophyllsolar-power	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/357.svg
370	luvdisc	87	6	1	f	https://pokeapi.co/api/v2/pokemon/370/	2022-11-03 23:52:55	2022-11-03 23:52:55	swift-swim	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/370.svg
373	salamence	1026	15	1	f	https://pokeapi.co/api/v2/pokemon/373/	2022-11-03 23:52:56	2022-11-03 23:52:56	intimidate	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/373.svg
382	kyogre	3520	45	0	f	https://pokeapi.co/api/v2/pokemon/382/	2022-11-03 23:52:58	2022-11-03 23:52:58	drizzle	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/382.svg
390	chimchar	62	5	1	f	https://pokeapi.co/api/v2/pokemon/390/	2022-11-03 23:53:02	2022-11-03 23:53:02	blaze	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/390.svg
400	bibarel	315	10	1	f	https://pokeapi.co/api/v2/pokemon/400/	2022-11-03 23:53:04	2022-11-03 23:53:04	simpleunaware	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/400.svg
407	roserade	145	9	1	f	https://pokeapi.co/api/v2/pokemon/407/	2022-11-03 23:53:05	2022-11-03 23:53:05	natural-curepoison-point	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/407.svg
420	cherubi	33	4	0	f	https://pokeapi.co/api/v2/pokemon/420/	2022-11-03 23:53:08	2022-11-03 23:53:08	chlorophyll	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/420.svg
423	gastrodon	299	9	1	f	https://pokeapi.co/api/v2/pokemon/423/	2022-11-03 23:53:09	2022-11-03 23:53:09	sticky-holdstorm-drain	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/423.svg
435	skuntank	380	10	1	f	https://pokeapi.co/api/v2/pokemon/435/	2022-11-03 23:53:12	2022-11-03 23:53:12	stenchaftermath	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/435.svg
444	gabite	560	14	1	f	https://pokeapi.co/api/v2/pokemon/444/	2022-11-03 23:53:13	2022-11-03 23:53:13	sand-veil	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/444.svg
454	toxicroak	444	13	1	f	https://pokeapi.co/api/v2/pokemon/454/	2022-11-03 23:53:16	2022-11-03 23:53:16	anticipationdry-skin	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/454.svg
462	magnezone	1800	12	1	f	https://pokeapi.co/api/v2/pokemon/462/	2022-11-03 23:53:17	2022-11-03 23:53:17	magnet-pullsturdy	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/462.svg
468	togekiss	380	15	1	f	https://pokeapi.co/api/v2/pokemon/468/	2022-11-03 23:53:19	2022-11-03 23:53:19	hustleserene-grace	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/468.svg
469	yanmega	515	19	1	f	https://pokeapi.co/api/v2/pokemon/469/	2022-11-03 23:53:19	2022-11-03 23:53:19	speed-boosttinted-lens	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/469.svg
470	leafeon	255	10	1	f	https://pokeapi.co/api/v2/pokemon/470/	2022-11-03 23:53:19	2022-11-03 23:53:19	leaf-guard	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/470.svg
471	glaceon	259	8	1	f	https://pokeapi.co/api/v2/pokemon/471/	2022-11-03 23:53:20	2022-11-03 23:53:20	snow-cloak	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/471.svg
830	eldegoss	25	5	1	f	https://pokeapi.co/api/v2/pokemon/830/	2022-11-03 23:54:27	2022-11-03 23:54:27	cotton-downregenerator	\N
472	gliscor	425	20	1	f	https://pokeapi.co/api/v2/pokemon/472/	2022-11-03 23:53:20	2022-11-03 23:53:20	hyper-cuttersand-veil	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/472.svg
473	mamoswine	2910	25	1	f	https://pokeapi.co/api/v2/pokemon/473/	2022-11-03 23:53:20	2022-11-03 23:53:20	oblivioussnow-cloak	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/473.svg
474	porygon-z	340	9	1	f	https://pokeapi.co/api/v2/pokemon/474/	2022-11-03 23:53:21	2022-11-03 23:53:21	adaptabilitydownload	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/474.svg
475	gallade	520	16	1	f	https://pokeapi.co/api/v2/pokemon/475/	2022-11-03 23:53:21	2022-11-03 23:53:21	steadfast	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/475.svg
476	probopass	3400	14	1	f	https://pokeapi.co/api/v2/pokemon/476/	2022-11-03 23:53:21	2022-11-03 23:53:21	sturdymagnet-pull	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/476.svg
478	froslass	266	13	1	f	https://pokeapi.co/api/v2/pokemon/478/	2022-11-03 23:53:21	2022-11-03 23:53:21	snow-cloak	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/478.svg
483	dialga	6830	54	1	f	https://pokeapi.co/api/v2/pokemon/483/	2022-11-03 23:53:22	2022-11-03 23:53:22	pressure	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/483.svg
487	giratina-altered	7500	45	1	f	https://pokeapi.co/api/v2/pokemon/487/	2022-11-03 23:53:23	2022-11-03 23:53:23	pressure	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/487.svg
488	cresselia	856	15	0	f	https://pokeapi.co/api/v2/pokemon/488/	2022-11-03 23:53:24	2022-11-03 23:53:24	levitate	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/488.svg
489	phione	31	4	0	f	https://pokeapi.co/api/v2/pokemon/489/	2022-11-03 23:53:24	2022-11-03 23:53:24	hydration	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/489.svg
491	darkrai	505	15	0	f	https://pokeapi.co/api/v2/pokemon/491/	2022-11-03 23:53:24	2022-11-03 23:53:24	bad-dreams	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/491.svg
492	shaymin-land	21	2	0	f	https://pokeapi.co/api/v2/pokemon/492/	2022-11-03 23:53:25	2022-11-03 23:53:25	natural-cure	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/492.svg
493	arceus	3200	32	0	f	https://pokeapi.co/api/v2/pokemon/493/	2022-11-03 23:53:25	2022-11-03 23:53:25	multitype	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/493.svg
495	snivy	81	6	1	f	https://pokeapi.co/api/v2/pokemon/495/	2022-11-03 23:53:25	2022-11-03 23:53:25	overgrow	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/495.svg
496	servine	160	8	1	f	https://pokeapi.co/api/v2/pokemon/496/	2022-11-03 23:53:25	2022-11-03 23:53:25	overgrow	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/496.svg
500	emboar	1500	16	1	f	https://pokeapi.co/api/v2/pokemon/500/	2022-11-03 23:53:26	2022-11-03 23:53:26	blaze	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/500.svg
502	dewott	245	8	1	f	https://pokeapi.co/api/v2/pokemon/502/	2022-11-03 23:53:26	2022-11-03 23:53:26	torrent	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/502.svg
503	samurott	946	15	1	f	https://pokeapi.co/api/v2/pokemon/503/	2022-11-03 23:53:26	2022-11-03 23:53:26	torrent	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/503.svg
506	lillipup	41	4	1	f	https://pokeapi.co/api/v2/pokemon/506/	2022-11-03 23:53:27	2022-11-03 23:53:27	vital-spiritpickup	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/506.svg
508	stoutland	610	12	1	f	https://pokeapi.co/api/v2/pokemon/508/	2022-11-03 23:53:27	2022-11-03 23:53:27	intimidatesand-rush	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/508.svg
509	purrloin	101	4	1	f	https://pokeapi.co/api/v2/pokemon/509/	2022-11-03 23:53:27	2022-11-03 23:53:27	limberunburden	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/509.svg
511	pansage	105	6	1	f	https://pokeapi.co/api/v2/pokemon/511/	2022-11-03 23:53:28	2022-11-03 23:53:28	gluttony	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/511.svg
513	pansear	110	6	1	f	https://pokeapi.co/api/v2/pokemon/513/	2022-11-03 23:53:28	2022-11-03 23:53:28	gluttony	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/513.svg
515	panpour	135	6	1	f	https://pokeapi.co/api/v2/pokemon/515/	2022-11-03 23:53:28	2022-11-03 23:53:28	gluttony	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/515.svg
518	musharna	605	11	1	f	https://pokeapi.co/api/v2/pokemon/518/	2022-11-03 23:53:30	2022-11-03 23:53:30	forewarnsynchronize	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/518.svg
521	unfezant	290	12	1	f	https://pokeapi.co/api/v2/pokemon/521/	2022-11-03 23:53:30	2022-11-03 23:53:30	big-peckssuper-luck	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/521.svg
522	blitzle	298	8	1	f	https://pokeapi.co/api/v2/pokemon/522/	2022-11-03 23:53:31	2022-11-03 23:53:31	lightning-rodmotor-drive	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/522.svg
525	boldore	1020	9	1	f	https://pokeapi.co/api/v2/pokemon/525/	2022-11-03 23:53:31	2022-11-03 23:53:31	sturdyweak-armor	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/525.svg
527	woobat	21	4	1	f	https://pokeapi.co/api/v2/pokemon/527/	2022-11-03 23:53:32	2022-11-03 23:53:32	unawareklutz	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/527.svg
529	drilbur	85	3	1	f	https://pokeapi.co/api/v2/pokemon/529/	2022-11-03 23:53:32	2022-11-03 23:53:32	sand-rushsand-force	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/529.svg
530	excadrill	404	7	1	f	https://pokeapi.co/api/v2/pokemon/530/	2022-11-03 23:53:32	2022-11-03 23:53:32	sand-rushsand-force	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/530.svg
534	conkeldurr	870	14	1	f	https://pokeapi.co/api/v2/pokemon/534/	2022-11-03 23:53:33	2022-11-03 23:53:33	gutssheer-force	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/534.svg
535	tympole	45	5	1	f	https://pokeapi.co/api/v2/pokemon/535/	2022-11-03 23:53:33	2022-11-03 23:53:33	swift-swimhydration	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/535.svg
537	seismitoad	620	15	1	f	https://pokeapi.co/api/v2/pokemon/537/	2022-11-03 23:53:33	2022-11-03 23:53:33	swift-swimpoison-touch	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/537.svg
538	throh	555	13	1	f	https://pokeapi.co/api/v2/pokemon/538/	2022-11-03 23:53:34	2022-11-03 23:53:34	gutsinner-focus	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/538.svg
477	dusknoir	1066	22	1	f	https://pokeapi.co/api/v2/pokemon/477/	2022-11-03 23:53:21	2022-11-03 23:53:21	pressure	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/477.svg
490	manaphy	14	3	0	f	https://pokeapi.co/api/v2/pokemon/490/	2022-11-03 23:53:24	2022-11-03 23:53:24	hydration	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/490.svg
494	victini	40	4	0	f	https://pokeapi.co/api/v2/pokemon/494/	2022-11-03 23:53:25	2022-11-03 23:53:25	victory-star	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/494.svg
512	simisage	305	11	1	f	https://pokeapi.co/api/v2/pokemon/512/	2022-11-03 23:53:28	2022-11-03 23:53:28	gluttony	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/512.svg
516	simipour	290	10	1	f	https://pokeapi.co/api/v2/pokemon/516/	2022-11-03 23:53:29	2022-11-03 23:53:29	gluttony	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/516.svg
528	swoobat	105	9	1	f	https://pokeapi.co/api/v2/pokemon/528/	2022-11-03 23:53:32	2022-11-03 23:53:32	unawareklutz	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/528.svg
536	palpitoad	170	8	1	f	https://pokeapi.co/api/v2/pokemon/536/	2022-11-03 23:53:33	2022-11-03 23:53:33	swift-swimhydration	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/536.svg
549	lilligant	163	11	1	f	https://pokeapi.co/api/v2/pokemon/549/	2022-11-03 23:53:36	2022-11-03 23:53:36	chlorophyllown-tempo	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/549.svg
561	sigilyph	140	14	1	f	https://pokeapi.co/api/v2/pokemon/561/	2022-11-03 23:53:38	2022-11-03 23:53:38	wonder-skinmagic-guard	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/561.svg
569	garbodor	1073	19	1	f	https://pokeapi.co/api/v2/pokemon/569/	2022-11-03 23:53:40	2022-11-03 23:53:40	stenchweak-armor	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/569.svg
576	gothitelle	440	15	1	f	https://pokeapi.co/api/v2/pokemon/576/	2022-11-03 23:53:41	2022-11-03 23:53:41	friskcompetitive	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/576.svg
590	foongus	10	2	1	f	https://pokeapi.co/api/v2/pokemon/590/	2022-11-03 23:53:44	2022-11-03 23:53:44	effect-spore	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/590.svg
593	jellicent	1350	22	1	f	https://pokeapi.co/api/v2/pokemon/593/	2022-11-03 23:53:45	2022-11-03 23:53:45	water-absorbcursed-body	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/593.svg
603	eelektrik	220	12	0	f	https://pokeapi.co/api/v2/pokemon/603/	2022-11-03 23:53:46	2022-11-03 23:53:46	levitate	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/603.svg
607	litwick	31	3	1	f	https://pokeapi.co/api/v2/pokemon/607/	2022-11-03 23:53:49	2022-11-03 23:53:49	flash-fireflame-body	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/607.svg
617	accelgor	253	8	1	f	https://pokeapi.co/api/v2/pokemon/617/	2022-11-03 23:53:50	2022-11-03 23:53:50	hydrationsticky-hold	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/617.svg
621	druddigon	1390	16	1	f	https://pokeapi.co/api/v2/pokemon/621/	2022-11-03 23:53:51	2022-11-03 23:53:51	rough-skinsheer-force	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/621.svg
637	volcarona	460	16	1	f	https://pokeapi.co/api/v2/pokemon/637/	2022-11-03 23:53:53	2022-11-03 23:53:53	flame-body	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/637.svg
645	landorus-incarnate	680	15	1	f	https://pokeapi.co/api/v2/pokemon/645/	2022-11-03 23:53:55	2022-11-03 23:53:55	sand-force	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/645.svg
654	braixen	145	10	1	f	https://pokeapi.co/api/v2/pokemon/654/	2022-11-03 23:53:56	2022-11-03 23:53:56	blaze	\N
665	spewpa	84	3	1	f	https://pokeapi.co/api/v2/pokemon/665/	2022-11-03 23:53:59	2022-11-03 23:53:59	shed-skin	\N
675	pangoro	1360	21	1	f	https://pokeapi.co/api/v2/pokemon/675/	2022-11-03 23:54:00	2022-11-03 23:54:00	iron-fistmold-breaker	\N
681	aegislash-shield	530	17	0	f	https://pokeapi.co/api/v2/pokemon/681/	2022-11-03 23:54:01	2022-11-03 23:54:01	stance-change	\N
695	heliolisk	210	10	1	f	https://pokeapi.co/api/v2/pokemon/695/	2022-11-03 23:54:04	2022-11-03 23:54:04	dry-skinsand-veil	\N
699	aurorus	2250	27	1	f	https://pokeapi.co/api/v2/pokemon/699/	2022-11-03 23:54:05	2022-11-03 23:54:05	refrigerate	\N
709	trevenant	710	15	1	f	https://pokeapi.co/api/v2/pokemon/709/	2022-11-03 23:54:06	2022-11-03 23:54:06	natural-curefrisk	\N
715	noivern	850	15	1	f	https://pokeapi.co/api/v2/pokemon/715/	2022-11-03 23:54:07	2022-11-03 23:54:07	friskinfiltrator	\N
725	litten	43	4	1	f	https://pokeapi.co/api/v2/pokemon/725/	2022-11-03 23:54:08	2022-11-03 23:54:08	blaze	\N
739	crabrawler	70	6	1	f	https://pokeapi.co/api/v2/pokemon/739/	2022-11-03 23:54:10	2022-11-03 23:54:10	hyper-cutteriron-fist	\N
749	mudbray	1100	10	1	f	https://pokeapi.co/api/v2/pokemon/749/	2022-11-03 23:54:12	2022-11-03 23:54:12	own-tempostamina	\N
757	salandit	48	6	1	f	https://pokeapi.co/api/v2/pokemon/757/	2022-11-03 23:54:15	2022-11-03 23:54:15	corrosion	\N
767	wimpod	120	5	0	f	https://pokeapi.co/api/v2/pokemon/767/	2022-11-03 23:54:16	2022-11-03 23:54:16	wimp-out	\N
777	togedemaru	33	3	1	f	https://pokeapi.co/api/v2/pokemon/777/	2022-11-03 23:54:17	2022-11-03 23:54:17	iron-barbslightning-rod	\N
479	rotom	3	3	0	f	https://pokeapi.co/api/v2/pokemon/479/	2022-11-03 23:53:21	2022-11-03 23:53:21	levitate	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/479.svg
497	serperior	630	33	1	f	https://pokeapi.co/api/v2/pokemon/497/	2022-11-03 23:53:25	2022-11-03 23:53:25	overgrow	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/497.svg
510	liepard	375	11	1	f	https://pokeapi.co/api/v2/pokemon/510/	2022-11-03 23:53:27	2022-11-03 23:53:27	limberunburden	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/510.svg
520	tranquill	150	6	1	f	https://pokeapi.co/api/v2/pokemon/520/	2022-11-03 23:53:30	2022-11-03 23:53:30	big-peckssuper-luck	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/520.svg
533	gurdurr	400	12	1	f	https://pokeapi.co/api/v2/pokemon/533/	2022-11-03 23:53:32	2022-11-03 23:53:32	gutssheer-force	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/533.svg
540	sewaddle	25	3	1	f	https://pokeapi.co/api/v2/pokemon/540/	2022-11-03 23:53:34	2022-11-03 23:53:34	swarmchlorophyll	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/540.svg
553	krookodile	963	15	1	f	https://pokeapi.co/api/v2/pokemon/553/	2022-11-03 23:53:36	2022-11-03 23:53:36	intimidatemoxie	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/553.svg
559	scraggy	118	6	1	f	https://pokeapi.co/api/v2/pokemon/559/	2022-11-03 23:53:38	2022-11-03 23:53:38	shed-skinmoxie	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/559.svg
573	cinccino	75	5	1	f	https://pokeapi.co/api/v2/pokemon/573/	2022-11-03 23:53:41	2022-11-03 23:53:41	cute-charmtechnician	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/573.svg
582	vanillite	57	4	1	f	https://pokeapi.co/api/v2/pokemon/582/	2022-11-03 23:53:42	2022-11-03 23:53:42	ice-bodysnow-cloak	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/582.svg
588	karrablast	59	5	1	f	https://pokeapi.co/api/v2/pokemon/588/	2022-11-03 23:53:43	2022-11-03 23:53:43	swarmshed-skin	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/588.svg
595	joltik	6	1	1	f	https://pokeapi.co/api/v2/pokemon/595/	2022-11-03 23:53:45	2022-11-03 23:53:45	compound-eyesunnerve	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/595.svg
606	beheeyem	345	10	1	f	https://pokeapi.co/api/v2/pokemon/606/	2022-11-03 23:53:48	2022-11-03 23:53:48	telepathysynchronize	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/606.svg
611	fraxure	360	10	1	f	https://pokeapi.co/api/v2/pokemon/611/	2022-11-03 23:53:49	2022-11-03 23:53:49	rivalrymold-breaker	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/611.svg
623	golurk	3300	28	1	f	https://pokeapi.co/api/v2/pokemon/623/	2022-11-03 23:53:52	2022-11-03 23:53:52	iron-fistklutz	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/623.svg
631	heatmor	580	14	1	f	https://pokeapi.co/api/v2/pokemon/631/	2022-11-03 23:53:52	2022-11-03 23:53:52	gluttonyflash-fire	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/631.svg
639	terrakion	2600	19	0	f	https://pokeapi.co/api/v2/pokemon/639/	2022-11-03 23:53:54	2022-11-03 23:53:54	justified	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/639.svg
647	keldeo-ordinary	485	14	0	f	https://pokeapi.co/api/v2/pokemon/647/	2022-11-03 23:53:55	2022-11-03 23:53:55	justified	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/647.svg
656	froakie	70	3	1	f	https://pokeapi.co/api/v2/pokemon/656/	2022-11-03 23:53:56	2022-11-03 23:53:56	torrent	\N
664	scatterbug	25	3	1	f	https://pokeapi.co/api/v2/pokemon/664/	2022-11-03 23:53:59	2022-11-03 23:53:59	shield-dustcompound-eyes	\N
674	pancham	80	6	1	f	https://pokeapi.co/api/v2/pokemon/674/	2022-11-03 23:54:00	2022-11-03 23:54:00	iron-fistmold-breaker	\N
680	doublade	45	8	0	f	https://pokeapi.co/api/v2/pokemon/680/	2022-11-03 23:54:01	2022-11-03 23:54:01	no-guard	\N
690	skrelp	73	5	1	f	https://pokeapi.co/api/v2/pokemon/690/	2022-11-03 23:54:02	2022-11-03 23:54:02	poison-pointpoison-touch	\N
697	tyrantrum	2700	25	1	f	https://pokeapi.co/api/v2/pokemon/697/	2022-11-03 23:54:04	2022-11-03 23:54:04	strong-jaw	\N
707	klefki	30	2	1	f	https://pokeapi.co/api/v2/pokemon/707/	2022-11-03 23:54:06	2022-11-03 23:54:06	prankster	\N
713	avalugg	5050	20	1	f	https://pokeapi.co/api/v2/pokemon/713/	2022-11-03 23:54:07	2022-11-03 23:54:07	own-tempoice-body	\N
723	dartrix	160	7	1	f	https://pokeapi.co/api/v2/pokemon/723/	2022-11-03 23:54:08	2022-11-03 23:54:08	overgrow	\N
732	trumbeak	148	6	1	f	https://pokeapi.co/api/v2/pokemon/732/	2022-11-03 23:54:09	2022-11-03 23:54:09	keen-eyeskill-link	\N
746	wishiwashi-solo	3	2	0	f	https://pokeapi.co/api/v2/pokemon/746/	2022-11-03 23:54:11	2022-11-03 23:54:11	schooling	\N
764	comfey	3	1	1	f	https://pokeapi.co/api/v2/pokemon/764/	2022-11-03 23:54:16	2022-11-03 23:54:16	flower-veiltriage	\N
774	minior-red-meteor	400	3	0	f	https://pokeapi.co/api/v2/pokemon/774/	2022-11-03 23:54:17	2022-11-03 23:54:17	shields-down	\N
782	jangmo-o	297	6	1	f	https://pokeapi.co/api/v2/pokemon/782/	2022-11-03 23:54:18	2022-11-03 23:54:18	bulletproofsoundproof	\N
480	uxie	3	3	0	f	https://pokeapi.co/api/v2/pokemon/480/	2022-11-03 23:53:22	2022-11-03 23:53:22	levitate	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/480.svg
481	mesprit	3	3	0	f	https://pokeapi.co/api/v2/pokemon/481/	2022-11-03 23:53:22	2022-11-03 23:53:22	levitate	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/481.svg
484	palkia	3360	42	1	f	https://pokeapi.co/api/v2/pokemon/484/	2022-11-03 23:53:23	2022-11-03 23:53:23	pressure	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/484.svg
485	heatran	4300	17	1	f	https://pokeapi.co/api/v2/pokemon/485/	2022-11-03 23:53:23	2022-11-03 23:53:23	flash-fire	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/485.svg
498	tepig	99	5	1	f	https://pokeapi.co/api/v2/pokemon/498/	2022-11-03 23:53:26	2022-11-03 23:53:26	blaze	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/498.svg
499	pignite	555	10	1	f	https://pokeapi.co/api/v2/pokemon/499/	2022-11-03 23:53:26	2022-11-03 23:53:26	blaze	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/499.svg
504	patrat	116	5	1	f	https://pokeapi.co/api/v2/pokemon/504/	2022-11-03 23:53:26	2022-11-03 23:53:26	run-awaykeen-eye	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/504.svg
505	watchog	270	11	1	f	https://pokeapi.co/api/v2/pokemon/505/	2022-11-03 23:53:27	2022-11-03 23:53:27	illuminatekeen-eye	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/505.svg
514	simisear	280	10	1	f	https://pokeapi.co/api/v2/pokemon/514/	2022-11-03 23:53:28	2022-11-03 23:53:28	gluttony	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/514.svg
519	pidove	21	3	1	f	https://pokeapi.co/api/v2/pokemon/519/	2022-11-03 23:53:30	2022-11-03 23:53:30	big-peckssuper-luck	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/519.svg
523	zebstrika	795	16	1	f	https://pokeapi.co/api/v2/pokemon/523/	2022-11-03 23:53:31	2022-11-03 23:53:31	lightning-rodmotor-drive	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/523.svg
526	gigalith	2600	17	1	f	https://pokeapi.co/api/v2/pokemon/526/	2022-11-03 23:53:31	2022-11-03 23:53:31	sturdysand-stream	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/526.svg
531	audino	310	11	1	f	https://pokeapi.co/api/v2/pokemon/531/	2022-11-03 23:53:32	2022-11-03 23:53:32	healerregenerator	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/531.svg
539	sawk	510	14	1	f	https://pokeapi.co/api/v2/pokemon/539/	2022-11-03 23:53:34	2022-11-03 23:53:34	sturdyinner-focus	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/539.svg
543	venipede	53	4	1	f	https://pokeapi.co/api/v2/pokemon/543/	2022-11-03 23:53:35	2022-11-03 23:53:35	poison-pointswarm	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/543.svg
546	cottonee	6	3	1	f	https://pokeapi.co/api/v2/pokemon/546/	2022-11-03 23:53:35	2022-11-03 23:53:35	pranksterinfiltrator	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/546.svg
551	sandile	152	7	1	f	https://pokeapi.co/api/v2/pokemon/551/	2022-11-03 23:53:36	2022-11-03 23:53:36	intimidatemoxie	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/551.svg
557	dwebble	145	3	1	f	https://pokeapi.co/api/v2/pokemon/557/	2022-11-03 23:53:37	2022-11-03 23:53:37	sturdyshell-armor	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/557.svg
564	tirtouga	165	7	1	f	https://pokeapi.co/api/v2/pokemon/564/	2022-11-03 23:53:39	2022-11-03 23:53:39	solid-rocksturdy	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/564.svg
565	carracosta	810	12	1	f	https://pokeapi.co/api/v2/pokemon/565/	2022-11-03 23:53:39	2022-11-03 23:53:39	solid-rocksturdy	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/565.svg
578	duosion	80	6	1	f	https://pokeapi.co/api/v2/pokemon/578/	2022-11-03 23:53:41	2022-11-03 23:53:41	overcoatmagic-guard	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/578.svg
580	ducklett	55	5	1	f	https://pokeapi.co/api/v2/pokemon/580/	2022-11-03 23:53:42	2022-11-03 23:53:42	keen-eyebig-pecks	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/580.svg
584	vanilluxe	575	13	1	f	https://pokeapi.co/api/v2/pokemon/584/	2022-11-03 23:53:43	2022-11-03 23:53:43	ice-bodysnow-warning	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/584.svg
586	sawsbuck	925	19	1	f	https://pokeapi.co/api/v2/pokemon/586/	2022-11-03 23:53:43	2022-11-03 23:53:43	chlorophyllsap-sipper	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/586.svg
596	galvantula	143	8	1	f	https://pokeapi.co/api/v2/pokemon/596/	2022-11-03 23:53:45	2022-11-03 23:53:45	compound-eyesunnerve	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/596.svg
599	klink	210	3	1	f	https://pokeapi.co/api/v2/pokemon/599/	2022-11-03 23:53:46	2022-11-03 23:53:46	plusminus	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/599.svg
605	elgyem	90	5	1	f	https://pokeapi.co/api/v2/pokemon/605/	2022-11-03 23:53:47	2022-11-03 23:53:47	telepathysynchronize	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/605.svg
612	haxorus	1055	18	1	f	https://pokeapi.co/api/v2/pokemon/612/	2022-11-03 23:53:49	2022-11-03 23:53:49	rivalrymold-breaker	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/612.svg
613	cubchoo	85	5	1	f	https://pokeapi.co/api/v2/pokemon/613/	2022-11-03 23:53:49	2022-11-03 23:53:49	snow-cloakslush-rush	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/613.svg
624	pawniard	102	5	1	f	https://pokeapi.co/api/v2/pokemon/624/	2022-11-03 23:53:52	2022-11-03 23:53:52	defiantinner-focus	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/624.svg
626	bouffalant	946	16	1	f	https://pokeapi.co/api/v2/pokemon/626/	2022-11-03 23:53:52	2022-11-03 23:53:52	recklesssap-sipper	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/626.svg
632	durant	330	3	1	f	https://pokeapi.co/api/v2/pokemon/632/	2022-11-03 23:53:53	2022-11-03 23:53:53	swarmhustle	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/632.svg
634	zweilous	500	14	0	f	https://pokeapi.co/api/v2/pokemon/634/	2022-11-03 23:53:53	2022-11-03 23:53:53	hustle	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/634.svg
640	virizion	2000	20	0	f	https://pokeapi.co/api/v2/pokemon/640/	2022-11-03 23:53:54	2022-11-03 23:53:54	justified	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/640.svg
831	wooloo	60	6	1	f	https://pokeapi.co/api/v2/pokemon/831/	2022-11-03 23:54:27	2022-11-03 23:54:27	fluffyrun-away	\N
482	azelf	3	3	0	f	https://pokeapi.co/api/v2/pokemon/482/	2022-11-03 23:53:22	2022-11-03 23:53:22	levitate	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/482.svg
486	regigigas	4200	37	0	f	https://pokeapi.co/api/v2/pokemon/486/	2022-11-03 23:53:23	2022-11-03 23:53:23	slow-start	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/486.svg
501	oshawott	59	5	1	f	https://pokeapi.co/api/v2/pokemon/501/	2022-11-03 23:53:26	2022-11-03 23:53:26	torrent	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/501.svg
507	herdier	147	9	1	f	https://pokeapi.co/api/v2/pokemon/507/	2022-11-03 23:53:27	2022-11-03 23:53:27	intimidatesand-rush	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/507.svg
517	munna	233	6	1	f	https://pokeapi.co/api/v2/pokemon/517/	2022-11-03 23:53:30	2022-11-03 23:53:30	forewarnsynchronize	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/517.svg
524	roggenrola	180	4	1	f	https://pokeapi.co/api/v2/pokemon/524/	2022-11-03 23:53:31	2022-11-03 23:53:31	sturdyweak-armor	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/524.svg
532	timburr	125	6	1	f	https://pokeapi.co/api/v2/pokemon/532/	2022-11-03 23:53:32	2022-11-03 23:53:32	gutssheer-force	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/532.svg
544	whirlipede	585	12	1	f	https://pokeapi.co/api/v2/pokemon/544/	2022-11-03 23:53:35	2022-11-03 23:53:35	poison-pointswarm	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/544.svg
552	krokorok	334	10	1	f	https://pokeapi.co/api/v2/pokemon/552/	2022-11-03 23:53:36	2022-11-03 23:53:36	intimidatemoxie	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/552.svg
562	yamask	15	5	0	f	https://pokeapi.co/api/v2/pokemon/562/	2022-11-03 23:53:39	2022-11-03 23:53:39	mummy	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/562.svg
570	zorua	125	7	0	f	https://pokeapi.co/api/v2/pokemon/570/	2022-11-03 23:53:40	2022-11-03 23:53:40	illusion	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/570.svg
577	solosis	10	3	1	f	https://pokeapi.co/api/v2/pokemon/577/	2022-11-03 23:53:41	2022-11-03 23:53:41	overcoatmagic-guard	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/577.svg
597	ferroseed	188	6	0	f	https://pokeapi.co/api/v2/pokemon/597/	2022-11-03 23:53:45	2022-11-03 23:53:45	iron-barbs	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/597.svg
616	shelmet	77	4	1	f	https://pokeapi.co/api/v2/pokemon/616/	2022-11-03 23:53:50	2022-11-03 23:53:50	hydrationshell-armor	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/616.svg
620	mienshao	355	14	1	f	https://pokeapi.co/api/v2/pokemon/620/	2022-11-03 23:53:51	2022-11-03 23:53:51	inner-focusregenerator	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/620.svg
630	mandibuzz	395	12	1	f	https://pokeapi.co/api/v2/pokemon/630/	2022-11-03 23:53:52	2022-11-03 23:53:52	big-pecksovercoat	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/630.svg
644	zekrom	3450	29	0	f	https://pokeapi.co/api/v2/pokemon/644/	2022-11-03 23:53:55	2022-11-03 23:53:55	teravolt	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/644.svg
653	fennekin	94	4	1	f	https://pokeapi.co/api/v2/pokemon/653/	2022-11-03 23:53:56	2022-11-03 23:53:56	blaze	\N
661	fletchling	17	3	1	f	https://pokeapi.co/api/v2/pokemon/661/	2022-11-03 23:53:57	2022-11-03 23:53:57	big-pecks	\N
669	flabebe	1	1	1	f	https://pokeapi.co/api/v2/pokemon/669/	2022-11-03 23:53:59	2022-11-03 23:53:59	flower-veil	\N
687	malamar	470	15	1	f	https://pokeapi.co/api/v2/pokemon/687/	2022-11-03 23:54:02	2022-11-03 23:54:02	contrarysuction-cups	\N
693	clawitzer	353	13	0	f	https://pokeapi.co/api/v2/pokemon/693/	2022-11-03 23:54:03	2022-11-03 23:54:03	mega-launcher	\N
701	hawlucha	215	8	1	f	https://pokeapi.co/api/v2/pokemon/701/	2022-11-03 23:54:05	2022-11-03 23:54:05	limberunburden	\N
716	xerneas	2150	30	0	f	https://pokeapi.co/api/v2/pokemon/716/	2022-11-03 23:54:07	2022-11-03 23:54:07	fairy-aura	\N
734	yungoos	60	4	1	f	https://pokeapi.co/api/v2/pokemon/734/	2022-11-03 23:54:09	2022-11-03 23:54:09	stakeoutstrong-jaw	\N
740	crabominable	1800	17	1	f	https://pokeapi.co/api/v2/pokemon/740/	2022-11-03 23:54:11	2022-11-03 23:54:11	hyper-cutteriron-fist	\N
750	mudsdale	9200	25	1	f	https://pokeapi.co/api/v2/pokemon/750/	2022-11-03 23:54:12	2022-11-03 23:54:12	own-tempostamina	\N
758	salazzle	222	12	1	f	https://pokeapi.co/api/v2/pokemon/758/	2022-11-03 23:54:15	2022-11-03 23:54:15	corrosion	\N
768	golisopod	1080	20	0	f	https://pokeapi.co/api/v2/pokemon/768/	2022-11-03 23:54:16	2022-11-03 23:54:16	emergency-exit	\N
778	mimikyu-disguised	7	2	0	f	https://pokeapi.co/api/v2/pokemon/778/	2022-11-03 23:54:17	2022-11-03 23:54:17	disguise	\N
541	swadloon	73	5	1	f	https://pokeapi.co/api/v2/pokemon/541/	2022-11-03 23:53:34	2022-11-03 23:53:34	leaf-guardchlorophyll	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/541.svg
554	darumaka	375	6	1	f	https://pokeapi.co/api/v2/pokemon/554/	2022-11-03 23:53:37	2022-11-03 23:53:37	hustle	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/554.svg
560	scrafty	300	11	1	f	https://pokeapi.co/api/v2/pokemon/560/	2022-11-03 23:53:38	2022-11-03 23:53:38	shed-skinmoxie	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/560.svg
572	minccino	58	4	1	f	https://pokeapi.co/api/v2/pokemon/572/	2022-11-03 23:53:41	2022-11-03 23:53:41	cute-charmtechnician	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/572.svg
581	swanna	242	13	1	f	https://pokeapi.co/api/v2/pokemon/581/	2022-11-03 23:53:42	2022-11-03 23:53:42	keen-eyebig-pecks	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/581.svg
587	emolga	50	4	1	f	https://pokeapi.co/api/v2/pokemon/587/	2022-11-03 23:53:43	2022-11-03 23:53:43	static	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/587.svg
601	klinklang	810	6	1	f	https://pokeapi.co/api/v2/pokemon/601/	2022-11-03 23:53:46	2022-11-03 23:53:46	plusminus	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/601.svg
608	lampent	130	6	1	f	https://pokeapi.co/api/v2/pokemon/608/	2022-11-03 23:53:49	2022-11-03 23:53:49	flash-fireflame-body	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/608.svg
625	bisharp	700	16	1	f	https://pokeapi.co/api/v2/pokemon/625/	2022-11-03 23:53:52	2022-11-03 23:53:52	defiantinner-focus	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/625.svg
633	deino	173	8	0	f	https://pokeapi.co/api/v2/pokemon/633/	2022-11-03 23:53:53	2022-11-03 23:53:53	hustle	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/633.svg
641	tornadus-incarnate	630	15	1	f	https://pokeapi.co/api/v2/pokemon/641/	2022-11-03 23:53:54	2022-11-03 23:53:54	prankster	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/641.svg
649	genesect	825	15	0	f	https://pokeapi.co/api/v2/pokemon/649/	2022-11-03 23:53:55	2022-11-03 23:53:55	download	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/649.svg
662	fletchinder	160	7	1	f	https://pokeapi.co/api/v2/pokemon/662/	2022-11-03 23:53:58	2022-11-03 23:53:58	flame-body	\N
667	litleo	135	6	1	f	https://pokeapi.co/api/v2/pokemon/667/	2022-11-03 23:53:59	2022-11-03 23:53:59	rivalryunnerve	\N
677	espurr	35	3	1	f	https://pokeapi.co/api/v2/pokemon/677/	2022-11-03 23:54:00	2022-11-03 23:54:00	keen-eyeinfiltrator	\N
683	aromatisse	155	8	1	f	https://pokeapi.co/api/v2/pokemon/683/	2022-11-03 23:54:01	2022-11-03 23:54:01	healer	\N
702	dedenne	22	2	1	f	https://pokeapi.co/api/v2/pokemon/702/	2022-11-03 23:54:05	2022-11-03 23:54:05	cheek-pouchpickup	\N
717	yveltal	2030	58	0	f	https://pokeapi.co/api/v2/pokemon/717/	2022-11-03 23:54:08	2022-11-03 23:54:08	dark-aura	\N
726	torracat	250	7	1	f	https://pokeapi.co/api/v2/pokemon/726/	2022-11-03 23:54:08	2022-11-03 23:54:08	blaze	\N
735	gumshoos	142	7	1	f	https://pokeapi.co/api/v2/pokemon/735/	2022-11-03 23:54:10	2022-11-03 23:54:10	stakeoutstrong-jaw	\N
741	oricorio-baile	34	6	0	f	https://pokeapi.co/api/v2/pokemon/741/	2022-11-03 23:54:11	2022-11-03 23:54:11	dancer	\N
751	dewpider	40	3	1	f	https://pokeapi.co/api/v2/pokemon/751/	2022-11-03 23:54:12	2022-11-03 23:54:12	water-bubble	\N
756	shiinotic	115	10	1	f	https://pokeapi.co/api/v2/pokemon/756/	2022-11-03 23:54:15	2022-11-03 23:54:15	illuminateeffect-spore	\N
766	passimian	828	20	1	f	https://pokeapi.co/api/v2/pokemon/766/	2022-11-03 23:54:16	2022-11-03 23:54:16	receiver	\N
776	turtonator	2120	20	0	f	https://pokeapi.co/api/v2/pokemon/776/	2022-11-03 23:54:17	2022-11-03 23:54:17	shell-armor	\N
542	leavanny	205	12	1	f	https://pokeapi.co/api/v2/pokemon/542/	2022-11-03 23:53:34	2022-11-03 23:53:34	swarmchlorophyll	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/542.svg
550	basculin-red-striped	180	10	1	f	https://pokeapi.co/api/v2/pokemon/550/	2022-11-03 23:53:36	2022-11-03 23:53:36	recklessadaptability	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/550.svg
563	cofagrigus	765	17	0	f	https://pokeapi.co/api/v2/pokemon/563/	2022-11-03 23:53:39	2022-11-03 23:53:39	mummy	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/563.svg
571	zoroark	811	16	0	f	https://pokeapi.co/api/v2/pokemon/571/	2022-11-03 23:53:40	2022-11-03 23:53:40	illusion	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/571.svg
583	vanillish	410	11	1	f	https://pokeapi.co/api/v2/pokemon/583/	2022-11-03 23:53:42	2022-11-03 23:53:42	ice-bodysnow-cloak	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/583.svg
598	ferrothorn	1100	10	1	f	https://pokeapi.co/api/v2/pokemon/598/	2022-11-03 23:53:45	2022-11-03 23:53:45	iron-barbs	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/598.svg
604	eelektross	805	21	0	f	https://pokeapi.co/api/v2/pokemon/604/	2022-11-03 23:53:47	2022-11-03 23:53:47	levitate	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/604.svg
615	cryogonal	1480	11	0	f	https://pokeapi.co/api/v2/pokemon/615/	2022-11-03 23:53:50	2022-11-03 23:53:50	levitate	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/615.svg
619	mienfoo	200	9	1	f	https://pokeapi.co/api/v2/pokemon/619/	2022-11-03 23:53:51	2022-11-03 23:53:51	inner-focusregenerator	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/619.svg
629	vullaby	90	5	1	f	https://pokeapi.co/api/v2/pokemon/629/	2022-11-03 23:53:52	2022-11-03 23:53:52	big-pecksovercoat	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/629.svg
643	reshiram	3300	32	0	f	https://pokeapi.co/api/v2/pokemon/643/	2022-11-03 23:53:55	2022-11-03 23:53:55	turboblaze	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/643.svg
652	chesnaught	900	16	1	f	https://pokeapi.co/api/v2/pokemon/652/	2022-11-03 23:53:56	2022-11-03 23:53:56	overgrow	\N
660	diggersby	424	10	1	f	https://pokeapi.co/api/v2/pokemon/660/	2022-11-03 23:53:57	2022-11-03 23:53:57	pickupcheek-pouch	\N
672	skiddo	310	9	1	f	https://pokeapi.co/api/v2/pokemon/672/	2022-11-03 23:54:00	2022-11-03 23:54:00	sap-sipper	\N
678	meowstic-male	85	6	1	f	https://pokeapi.co/api/v2/pokemon/678/	2022-11-03 23:54:00	2022-11-03 23:54:00	keen-eyeinfiltrator	\N
688	binacle	310	5	1	f	https://pokeapi.co/api/v2/pokemon/688/	2022-11-03 23:54:02	2022-11-03 23:54:02	tough-clawssniper	\N
694	helioptile	60	5	1	f	https://pokeapi.co/api/v2/pokemon/694/	2022-11-03 23:54:03	2022-11-03 23:54:03	dry-skinsand-veil	\N
705	sliggoo	175	8	1	f	https://pokeapi.co/api/v2/pokemon/705/	2022-11-03 23:54:06	2022-11-03 23:54:06	sap-sipperhydration	\N
711	gourgeist-average	125	9	1	f	https://pokeapi.co/api/v2/pokemon/711/	2022-11-03 23:54:07	2022-11-03 23:54:07	pickupfrisk	\N
721	volcanion	1950	17	0	f	https://pokeapi.co/api/v2/pokemon/721/	2022-11-03 23:54:08	2022-11-03 23:54:08	water-absorb	\N
730	primarina	440	18	1	f	https://pokeapi.co/api/v2/pokemon/730/	2022-11-03 23:54:09	2022-11-03 23:54:09	torrent	\N
748	toxapex	145	7	1	f	https://pokeapi.co/api/v2/pokemon/748/	2022-11-03 23:54:12	2022-11-03 23:54:12	mercilesslimber	\N
752	araquanid	820	18	1	f	https://pokeapi.co/api/v2/pokemon/752/	2022-11-03 23:54:13	2022-11-03 23:54:13	water-bubble	\N
763	tsareena	214	12	1	f	https://pokeapi.co/api/v2/pokemon/763/	2022-11-03 23:54:16	2022-11-03 23:54:16	leaf-guardqueenly-majesty	\N
773	silvally	1005	23	0	f	https://pokeapi.co/api/v2/pokemon/773/	2022-11-03 23:54:17	2022-11-03 23:54:17	rks-system	\N
781	dhelmise	2100	39	0	f	https://pokeapi.co/api/v2/pokemon/781/	2022-11-03 23:54:18	2022-11-03 23:54:18	steelworker	\N
545	scolipede	2005	25	1	f	https://pokeapi.co/api/v2/pokemon/545/	2022-11-03 23:53:35	2022-11-03 23:53:35	poison-pointswarm	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/545.svg
558	crustle	2000	14	1	f	https://pokeapi.co/api/v2/pokemon/558/	2022-11-03 23:53:38	2022-11-03 23:53:38	sturdyshell-armor	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/558.svg
566	archen	95	5	0	f	https://pokeapi.co/api/v2/pokemon/566/	2022-11-03 23:53:39	2022-11-03 23:53:39	defeatist	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/566.svg
579	reuniclus	201	10	1	f	https://pokeapi.co/api/v2/pokemon/579/	2022-11-03 23:53:42	2022-11-03 23:53:42	overcoatmagic-guard	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/579.svg
585	deerling	195	6	1	f	https://pokeapi.co/api/v2/pokemon/585/	2022-11-03 23:53:43	2022-11-03 23:53:43	chlorophyllsap-sipper	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/585.svg
600	klang	510	6	1	f	https://pokeapi.co/api/v2/pokemon/600/	2022-11-03 23:53:46	2022-11-03 23:53:46	plusminus	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/600.svg
609	chandelure	343	10	1	f	https://pokeapi.co/api/v2/pokemon/609/	2022-11-03 23:53:49	2022-11-03 23:53:49	flash-fireflame-body	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/609.svg
622	golett	920	10	1	f	https://pokeapi.co/api/v2/pokemon/622/	2022-11-03 23:53:51	2022-11-03 23:53:51	iron-fistklutz	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/622.svg
638	cobalion	2500	21	0	f	https://pokeapi.co/api/v2/pokemon/638/	2022-11-03 23:53:54	2022-11-03 23:53:54	justified	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/638.svg
646	kyurem	3250	30	0	f	https://pokeapi.co/api/v2/pokemon/646/	2022-11-03 23:53:55	2022-11-03 23:53:55	pressure	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/646.svg
655	delphox	390	15	1	f	https://pokeapi.co/api/v2/pokemon/655/	2022-11-03 23:53:56	2022-11-03 23:53:56	blaze	\N
666	vivillon	170	12	1	f	https://pokeapi.co/api/v2/pokemon/666/	2022-11-03 23:53:59	2022-11-03 23:53:59	shield-dustcompound-eyes	\N
676	furfrou	280	12	0	f	https://pokeapi.co/api/v2/pokemon/676/	2022-11-03 23:54:00	2022-11-03 23:54:00	fur-coat	\N
682	spritzee	5	2	1	f	https://pokeapi.co/api/v2/pokemon/682/	2022-11-03 23:54:01	2022-11-03 23:54:01	healer	\N
696	tyrunt	260	8	1	f	https://pokeapi.co/api/v2/pokemon/696/	2022-11-03 23:54:04	2022-11-03 23:54:04	strong-jaw	\N
700	sylveon	235	10	1	f	https://pokeapi.co/api/v2/pokemon/700/	2022-11-03 23:54:05	2022-11-03 23:54:05	cute-charm	\N
718	zygarde-50	3050	50	0	f	https://pokeapi.co/api/v2/pokemon/718/	2022-11-03 23:54:08	2022-11-03 23:54:08	aura-break	\N
727	incineroar	830	18	1	f	https://pokeapi.co/api/v2/pokemon/727/	2022-11-03 23:54:09	2022-11-03 23:54:09	blaze	\N
736	grubbin	44	4	0	f	https://pokeapi.co/api/v2/pokemon/736/	2022-11-03 23:54:10	2022-11-03 23:54:10	swarm	\N
742	cutiefly	2	1	1	f	https://pokeapi.co/api/v2/pokemon/742/	2022-11-03 23:54:11	2022-11-03 23:54:11	honey-gathershield-dust	\N
753	fomantis	15	3	1	f	https://pokeapi.co/api/v2/pokemon/753/	2022-11-03 23:54:14	2022-11-03 23:54:14	leaf-guard	\N
759	stufful	68	5	1	f	https://pokeapi.co/api/v2/pokemon/759/	2022-11-03 23:54:15	2022-11-03 23:54:15	fluffyklutz	\N
769	sandygast	700	5	1	f	https://pokeapi.co/api/v2/pokemon/769/	2022-11-03 23:54:16	2022-11-03 23:54:16	water-compaction	\N
784	kommo-o	782	16	1	f	https://pokeapi.co/api/v2/pokemon/784/	2022-11-03 23:54:19	2022-11-03 23:54:19	bulletproofsoundproof	\N
547	whimsicott	66	7	1	f	https://pokeapi.co/api/v2/pokemon/547/	2022-11-03 23:53:36	2022-11-03 23:53:36	pranksterinfiltrator	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/547.svg
555	darmanitan-standard	929	13	1	f	https://pokeapi.co/api/v2/pokemon/555/	2022-11-03 23:53:37	2022-11-03 23:53:37	sheer-force	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/555.svg
567	archeops	320	14	0	f	https://pokeapi.co/api/v2/pokemon/567/	2022-11-03 23:53:40	2022-11-03 23:53:40	defeatist	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/567.svg
574	gothita	58	4	1	f	https://pokeapi.co/api/v2/pokemon/574/	2022-11-03 23:53:41	2022-11-03 23:53:41	friskcompetitive	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/574.svg
589	escavalier	330	10	1	f	https://pokeapi.co/api/v2/pokemon/589/	2022-11-03 23:53:44	2022-11-03 23:53:44	swarmshell-armor	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/589.svg
592	frillish	330	12	1	f	https://pokeapi.co/api/v2/pokemon/592/	2022-11-03 23:53:44	2022-11-03 23:53:44	water-absorbcursed-body	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/592.svg
602	tynamo	3	2	0	f	https://pokeapi.co/api/v2/pokemon/602/	2022-11-03 23:53:46	2022-11-03 23:53:46	levitate	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/602.svg
610	axew	180	6	1	f	https://pokeapi.co/api/v2/pokemon/610/	2022-11-03 23:53:49	2022-11-03 23:53:49	rivalrymold-breaker	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/610.svg
627	rufflet	105	5	1	f	https://pokeapi.co/api/v2/pokemon/627/	2022-11-03 23:53:52	2022-11-03 23:53:52	keen-eyesheer-force	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/627.svg
635	hydreigon	1600	18	0	f	https://pokeapi.co/api/v2/pokemon/635/	2022-11-03 23:53:53	2022-11-03 23:53:53	levitate	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/635.svg
651	quilladin	290	7	1	f	https://pokeapi.co/api/v2/pokemon/651/	2022-11-03 23:53:56	2022-11-03 23:53:56	overgrow	\N
659	bunnelby	50	4	1	f	https://pokeapi.co/api/v2/pokemon/659/	2022-11-03 23:53:57	2022-11-03 23:53:57	pickupcheek-pouch	\N
670	floette	9	2	1	f	https://pokeapi.co/api/v2/pokemon/670/	2022-11-03 23:53:59	2022-11-03 23:53:59	flower-veil	\N
685	slurpuff	50	8	1	f	https://pokeapi.co/api/v2/pokemon/685/	2022-11-03 23:54:01	2022-11-03 23:54:01	sweet-veil	\N
691	dragalge	815	18	1	f	https://pokeapi.co/api/v2/pokemon/691/	2022-11-03 23:54:02	2022-11-03 23:54:02	poison-pointpoison-touch	\N
704	goomy	28	3	1	f	https://pokeapi.co/api/v2/pokemon/704/	2022-11-03 23:54:05	2022-11-03 23:54:05	sap-sipperhydration	\N
710	pumpkaboo-average	50	4	1	f	https://pokeapi.co/api/v2/pokemon/710/	2022-11-03 23:54:06	2022-11-03 23:54:06	pickupfrisk	\N
720	hoopa	90	5	0	f	https://pokeapi.co/api/v2/pokemon/720/	2022-11-03 23:54:08	2022-11-03 23:54:08	magician	\N
729	brionne	175	6	1	f	https://pokeapi.co/api/v2/pokemon/729/	2022-11-03 23:54:09	2022-11-03 23:54:09	torrent	\N
738	vikavolt	450	15	0	f	https://pokeapi.co/api/v2/pokemon/738/	2022-11-03 23:54:10	2022-11-03 23:54:10	levitate	\N
744	rockruff	92	5	1	f	https://pokeapi.co/api/v2/pokemon/744/	2022-11-03 23:54:11	2022-11-03 23:54:11	keen-eyevital-spirit	\N
762	steenee	82	7	1	f	https://pokeapi.co/api/v2/pokemon/762/	2022-11-03 23:54:15	2022-11-03 23:54:15	leaf-guardoblivious	\N
772	type-null	1205	19	0	f	https://pokeapi.co/api/v2/pokemon/772/	2022-11-03 23:54:17	2022-11-03 23:54:17	battle-armor	\N
780	drampa	1850	30	1	f	https://pokeapi.co/api/v2/pokemon/780/	2022-11-03 23:54:18	2022-11-03 23:54:18	berserksap-sipper	\N
548	petilil	66	5	1	f	https://pokeapi.co/api/v2/pokemon/548/	2022-11-03 23:53:36	2022-11-03 23:53:36	chlorophyllown-tempo	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/548.svg
556	maractus	280	10	1	f	https://pokeapi.co/api/v2/pokemon/556/	2022-11-03 23:53:37	2022-11-03 23:53:37	water-absorbchlorophyll	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/556.svg
568	trubbish	310	6	1	f	https://pokeapi.co/api/v2/pokemon/568/	2022-11-03 23:53:40	2022-11-03 23:53:40	stenchsticky-hold	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/568.svg
575	gothorita	180	7	1	f	https://pokeapi.co/api/v2/pokemon/575/	2022-11-03 23:53:41	2022-11-03 23:53:41	friskcompetitive	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/575.svg
591	amoonguss	105	6	1	f	https://pokeapi.co/api/v2/pokemon/591/	2022-11-03 23:53:44	2022-11-03 23:53:44	effect-spore	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/591.svg
594	alomomola	316	12	1	f	https://pokeapi.co/api/v2/pokemon/594/	2022-11-03 23:53:45	2022-11-03 23:53:45	healerhydration	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/594.svg
614	beartic	2600	26	1	f	https://pokeapi.co/api/v2/pokemon/614/	2022-11-03 23:53:50	2022-11-03 23:53:50	snow-cloakslush-rush	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/614.svg
618	stunfisk	110	7	1	f	https://pokeapi.co/api/v2/pokemon/618/	2022-11-03 23:53:51	2022-11-03 23:53:51	staticlimber	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/618.svg
628	braviary	410	15	1	f	https://pokeapi.co/api/v2/pokemon/628/	2022-11-03 23:53:52	2022-11-03 23:53:52	keen-eyesheer-force	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/628.svg
636	larvesta	288	11	1	f	https://pokeapi.co/api/v2/pokemon/636/	2022-11-03 23:53:53	2022-11-03 23:53:53	flame-body	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/636.svg
650	chespin	90	4	1	f	https://pokeapi.co/api/v2/pokemon/650/	2022-11-03 23:53:56	2022-11-03 23:53:56	overgrow	\N
658	greninja	400	15	1	f	https://pokeapi.co/api/v2/pokemon/658/	2022-11-03 23:53:57	2022-11-03 23:53:57	torrent	\N
673	gogoat	910	17	1	f	https://pokeapi.co/api/v2/pokemon/673/	2022-11-03 23:54:00	2022-11-03 23:54:00	sap-sipper	\N
679	honedge	20	8	0	f	https://pokeapi.co/api/v2/pokemon/679/	2022-11-03 23:54:01	2022-11-03 23:54:01	no-guard	\N
689	barbaracle	960	13	1	f	https://pokeapi.co/api/v2/pokemon/689/	2022-11-03 23:54:02	2022-11-03 23:54:02	tough-clawssniper	\N
698	amaura	252	13	1	f	https://pokeapi.co/api/v2/pokemon/698/	2022-11-03 23:54:05	2022-11-03 23:54:05	refrigerate	\N
708	phantump	70	4	1	f	https://pokeapi.co/api/v2/pokemon/708/	2022-11-03 23:54:06	2022-11-03 23:54:06	natural-curefrisk	\N
714	noibat	80	5	1	f	https://pokeapi.co/api/v2/pokemon/714/	2022-11-03 23:54:07	2022-11-03 23:54:07	friskinfiltrator	\N
724	decidueye	366	16	1	f	https://pokeapi.co/api/v2/pokemon/724/	2022-11-03 23:54:08	2022-11-03 23:54:08	overgrow	\N
733	toucannon	260	11	1	f	https://pokeapi.co/api/v2/pokemon/733/	2022-11-03 23:54:09	2022-11-03 23:54:09	keen-eyeskill-link	\N
747	mareanie	80	4	1	f	https://pokeapi.co/api/v2/pokemon/747/	2022-11-03 23:54:11	2022-11-03 23:54:11	mercilesslimber	\N
765	oranguru	760	15	1	f	https://pokeapi.co/api/v2/pokemon/765/	2022-11-03 23:54:16	2022-11-03 23:54:16	inner-focustelepathy	\N
775	komala	199	4	0	f	https://pokeapi.co/api/v2/pokemon/775/	2022-11-03 23:54:17	2022-11-03 23:54:17	comatose	\N
783	hakamo-o	470	12	1	f	https://pokeapi.co/api/v2/pokemon/783/	2022-11-03 23:54:18	2022-11-03 23:54:18	bulletproofsoundproof	\N
642	thundurus-incarnate	610	15	1	f	https://pokeapi.co/api/v2/pokemon/642/	2022-11-03 23:53:54	2022-11-03 23:53:54	prankster	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/642.svg
657	frogadier	109	6	1	f	https://pokeapi.co/api/v2/pokemon/657/	2022-11-03 23:53:57	2022-11-03 23:53:57	torrent	\N
671	florges	100	11	1	f	https://pokeapi.co/api/v2/pokemon/671/	2022-11-03 23:53:59	2022-11-03 23:53:59	flower-veil	\N
686	inkay	35	4	1	f	https://pokeapi.co/api/v2/pokemon/686/	2022-11-03 23:54:02	2022-11-03 23:54:02	contrarysuction-cups	\N
692	clauncher	83	5	0	f	https://pokeapi.co/api/v2/pokemon/692/	2022-11-03 23:54:03	2022-11-03 23:54:03	mega-launcher	\N
706	goodra	1505	20	1	f	https://pokeapi.co/api/v2/pokemon/706/	2022-11-03 23:54:06	2022-11-03 23:54:06	sap-sipperhydration	\N
712	bergmite	995	10	1	f	https://pokeapi.co/api/v2/pokemon/712/	2022-11-03 23:54:07	2022-11-03 23:54:07	own-tempoice-body	\N
722	rowlet	15	3	1	f	https://pokeapi.co/api/v2/pokemon/722/	2022-11-03 23:54:08	2022-11-03 23:54:08	overgrow	\N
731	pikipek	12	3	1	f	https://pokeapi.co/api/v2/pokemon/731/	2022-11-03 23:54:09	2022-11-03 23:54:09	keen-eyeskill-link	\N
745	lycanroc-midday	250	8	1	f	https://pokeapi.co/api/v2/pokemon/745/	2022-11-03 23:54:11	2022-11-03 23:54:11	keen-eyesand-rush	\N
754	lurantis	185	9	1	f	https://pokeapi.co/api/v2/pokemon/754/	2022-11-03 23:54:14	2022-11-03 23:54:14	leaf-guard	\N
760	bewear	1350	21	1	f	https://pokeapi.co/api/v2/pokemon/760/	2022-11-03 23:54:15	2022-11-03 23:54:15	fluffyklutz	\N
770	palossand	2500	13	1	f	https://pokeapi.co/api/v2/pokemon/770/	2022-11-03 23:54:16	2022-11-03 23:54:16	water-compaction	\N
785	tapu-koko	205	18	1	f	https://pokeapi.co/api/v2/pokemon/785/	2022-11-03 23:54:19	2022-11-03 23:54:19	electric-surge	\N
648	meloetta-aria	65	6	0	f	https://pokeapi.co/api/v2/pokemon/648/	2022-11-03 23:53:55	2022-11-03 23:53:55	serene-grace	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/648.svg
663	talonflame	245	12	1	f	https://pokeapi.co/api/v2/pokemon/663/	2022-11-03 23:53:58	2022-11-03 23:53:58	flame-body	\N
668	pyroar	815	15	1	f	https://pokeapi.co/api/v2/pokemon/668/	2022-11-03 23:53:59	2022-11-03 23:53:59	rivalryunnerve	\N
684	swirlix	35	4	1	f	https://pokeapi.co/api/v2/pokemon/684/	2022-11-03 23:54:01	2022-11-03 23:54:01	sweet-veil	\N
703	carbink	57	3	1	f	https://pokeapi.co/api/v2/pokemon/703/	2022-11-03 23:54:05	2022-11-03 23:54:05	clear-body	\N
719	diancie	88	7	0	f	https://pokeapi.co/api/v2/pokemon/719/	2022-11-03 23:54:08	2022-11-03 23:54:08	clear-body	\N
728	popplio	75	4	1	f	https://pokeapi.co/api/v2/pokemon/728/	2022-11-03 23:54:09	2022-11-03 23:54:09	torrent	\N
737	charjabug	105	5	0	f	https://pokeapi.co/api/v2/pokemon/737/	2022-11-03 23:54:10	2022-11-03 23:54:10	battery	\N
743	ribombee	5	2	1	f	https://pokeapi.co/api/v2/pokemon/743/	2022-11-03 23:54:11	2022-11-03 23:54:11	honey-gathershield-dust	\N
755	morelull	15	2	1	f	https://pokeapi.co/api/v2/pokemon/755/	2022-11-03 23:54:14	2022-11-03 23:54:14	illuminateeffect-spore	\N
761	bounsweet	32	3	1	f	https://pokeapi.co/api/v2/pokemon/761/	2022-11-03 23:54:15	2022-11-03 23:54:15	leaf-guardoblivious	\N
771	pyukumuku	12	3	1	f	https://pokeapi.co/api/v2/pokemon/771/	2022-11-03 23:54:16	2022-11-03 23:54:16	innards-out	\N
779	bruxish	190	9	1	f	https://pokeapi.co/api/v2/pokemon/779/	2022-11-03 23:54:18	2022-11-03 23:54:18	dazzlingstrong-jaw	\N
786	tapu-lele	186	12	1	f	https://pokeapi.co/api/v2/pokemon/786/	2022-11-03 23:54:19	2022-11-03 23:54:19	psychic-surge	\N
787	tapu-bulu	455	19	1	f	https://pokeapi.co/api/v2/pokemon/787/	2022-11-03 23:54:19	2022-11-03 23:54:19	grassy-surge	\N
788	tapu-fini	212	13	1	f	https://pokeapi.co/api/v2/pokemon/788/	2022-11-03 23:54:19	2022-11-03 23:54:19	misty-surge	\N
789	cosmog	1	2	0	f	https://pokeapi.co/api/v2/pokemon/789/	2022-11-03 23:54:19	2022-11-03 23:54:19	unaware	\N
790	cosmoem	9999	1	0	f	https://pokeapi.co/api/v2/pokemon/790/	2022-11-03 23:54:20	2022-11-03 23:54:20	sturdy	\N
791	solgaleo	2300	34	0	f	https://pokeapi.co/api/v2/pokemon/791/	2022-11-03 23:54:20	2022-11-03 23:54:20	full-metal-body	\N
792	lunala	1200	40	0	f	https://pokeapi.co/api/v2/pokemon/792/	2022-11-03 23:54:20	2022-11-03 23:54:20	shadow-shield	\N
793	nihilego	555	12	0	f	https://pokeapi.co/api/v2/pokemon/793/	2022-11-03 23:54:20	2022-11-03 23:54:20	beast-boost	\N
794	buzzwole	3336	24	0	f	https://pokeapi.co/api/v2/pokemon/794/	2022-11-03 23:54:20	2022-11-03 23:54:20	beast-boost	\N
795	pheromosa	250	18	0	f	https://pokeapi.co/api/v2/pokemon/795/	2022-11-03 23:54:20	2022-11-03 23:54:20	beast-boost	\N
796	xurkitree	1000	38	0	f	https://pokeapi.co/api/v2/pokemon/796/	2022-11-03 23:54:20	2022-11-03 23:54:20	beast-boost	\N
797	celesteela	9999	92	0	f	https://pokeapi.co/api/v2/pokemon/797/	2022-11-03 23:54:20	2022-11-03 23:54:20	beast-boost	\N
798	kartana	1	3	0	f	https://pokeapi.co/api/v2/pokemon/798/	2022-11-03 23:54:21	2022-11-03 23:54:21	beast-boost	\N
799	guzzlord	8880	55	0	f	https://pokeapi.co/api/v2/pokemon/799/	2022-11-03 23:54:21	2022-11-03 23:54:21	beast-boost	\N
800	necrozma	2300	24	0	f	https://pokeapi.co/api/v2/pokemon/800/	2022-11-03 23:54:21	2022-11-03 23:54:21	prism-armor	\N
801	magearna	805	10	0	f	https://pokeapi.co/api/v2/pokemon/801/	2022-11-03 23:54:21	2022-11-03 23:54:21	soul-heart	\N
802	marshadow	222	7	0	f	https://pokeapi.co/api/v2/pokemon/802/	2022-11-03 23:54:21	2022-11-03 23:54:21	technician	\N
803	poipole	18	6	0	f	https://pokeapi.co/api/v2/pokemon/803/	2022-11-03 23:54:21	2022-11-03 23:54:21	beast-boost	\N
804	naganadel	1500	36	0	f	https://pokeapi.co/api/v2/pokemon/804/	2022-11-03 23:54:21	2022-11-03 23:54:21	beast-boost	\N
805	stakataka	8200	55	0	f	https://pokeapi.co/api/v2/pokemon/805/	2022-11-03 23:54:21	2022-11-03 23:54:21	beast-boost	\N
806	blacephalon	130	18	0	f	https://pokeapi.co/api/v2/pokemon/806/	2022-11-03 23:54:22	2022-11-03 23:54:22	beast-boost	\N
807	zeraora	445	15	0	f	https://pokeapi.co/api/v2/pokemon/807/	2022-11-03 23:54:22	2022-11-03 23:54:22	volt-absorb	\N
808	meltan	80	2	0	f	https://pokeapi.co/api/v2/pokemon/808/	2022-11-03 23:54:22	2022-11-03 23:54:22	magnet-pull	\N
809	melmetal	8000	25	0	f	https://pokeapi.co/api/v2/pokemon/809/	2022-11-03 23:54:22	2022-11-03 23:54:22	iron-fist	\N
810	grookey	50	3	1	f	https://pokeapi.co/api/v2/pokemon/810/	2022-11-03 23:54:22	2022-11-03 23:54:22	overgrow	\N
811	thwackey	140	7	1	f	https://pokeapi.co/api/v2/pokemon/811/	2022-11-03 23:54:22	2022-11-03 23:54:22	overgrow	\N
812	rillaboom	900	21	1	f	https://pokeapi.co/api/v2/pokemon/812/	2022-11-03 23:54:22	2022-11-03 23:54:22	overgrow	\N
813	scorbunny	45	3	1	f	https://pokeapi.co/api/v2/pokemon/813/	2022-11-03 23:54:22	2022-11-03 23:54:22	blaze	\N
814	raboot	90	6	1	f	https://pokeapi.co/api/v2/pokemon/814/	2022-11-03 23:54:23	2022-11-03 23:54:23	blaze	\N
815	cinderace	330	14	1	f	https://pokeapi.co/api/v2/pokemon/815/	2022-11-03 23:54:23	2022-11-03 23:54:23	blaze	\N
816	sobble	40	3	1	f	https://pokeapi.co/api/v2/pokemon/816/	2022-11-03 23:54:23	2022-11-03 23:54:23	torrent	\N
817	drizzile	115	7	1	f	https://pokeapi.co/api/v2/pokemon/817/	2022-11-03 23:54:24	2022-11-03 23:54:24	torrent	\N
818	inteleon	452	19	1	f	https://pokeapi.co/api/v2/pokemon/818/	2022-11-03 23:54:24	2022-11-03 23:54:24	torrent	\N
819	skwovet	25	3	1	f	https://pokeapi.co/api/v2/pokemon/819/	2022-11-03 23:54:24	2022-11-03 23:54:24	cheek-pouch	\N
820	greedent	60	6	1	f	https://pokeapi.co/api/v2/pokemon/820/	2022-11-03 23:54:24	2022-11-03 23:54:24	cheek-pouch	\N
821	rookidee	18	2	1	f	https://pokeapi.co/api/v2/pokemon/821/	2022-11-03 23:54:25	2022-11-03 23:54:25	keen-eyeunnerve	\N
822	corvisquire	160	8	1	f	https://pokeapi.co/api/v2/pokemon/822/	2022-11-03 23:54:25	2022-11-03 23:54:25	keen-eyeunnerve	\N
823	corviknight	750	22	1	f	https://pokeapi.co/api/v2/pokemon/823/	2022-11-03 23:54:25	2022-11-03 23:54:25	pressureunnerve	\N
824	blipbug	80	4	1	f	https://pokeapi.co/api/v2/pokemon/824/	2022-11-03 23:54:26	2022-11-03 23:54:26	swarmcompound-eyes	\N
825	dottler	195	4	1	f	https://pokeapi.co/api/v2/pokemon/825/	2022-11-03 23:54:26	2022-11-03 23:54:26	swarmcompound-eyes	\N
826	orbeetle	408	4	1	f	https://pokeapi.co/api/v2/pokemon/826/	2022-11-03 23:54:26	2022-11-03 23:54:26	swarmfrisk	\N
827	nickit	89	6	1	f	https://pokeapi.co/api/v2/pokemon/827/	2022-11-03 23:54:27	2022-11-03 23:54:27	run-awayunburden	\N
828	thievul	199	12	1	f	https://pokeapi.co/api/v2/pokemon/828/	2022-11-03 23:54:27	2022-11-03 23:54:27	run-awayunburden	\N
829	gossifleur	22	4	1	f	https://pokeapi.co/api/v2/pokemon/829/	2022-11-03 23:54:27	2022-11-03 23:54:27	cotton-downregenerator	\N
832	dubwool	430	13	1	f	https://pokeapi.co/api/v2/pokemon/832/	2022-11-03 23:54:27	2022-11-03 23:54:27	fluffysteadfast	\N
842	appletun	130	4	1	f	https://pokeapi.co/api/v2/pokemon/842/	2022-11-03 23:54:29	2022-11-03 23:54:29	ripengluttony	\N
850	sizzlipede	10	7	1	f	https://pokeapi.co/api/v2/pokemon/850/	2022-11-03 23:54:30	2022-11-03 23:54:30	flash-firewhite-smoke	\N
866	mr-rime	582	15	1	f	https://pokeapi.co/api/v2/pokemon/866/	2022-11-03 23:54:33	2022-11-03 23:54:33	tangled-feetscreen-cleaner	\N
872	snom	38	3	1	f	https://pokeapi.co/api/v2/pokemon/872/	2022-11-03 23:54:34	2022-11-03 23:54:34	shield-dust	\N
884	duraludon	400	18	1	f	https://pokeapi.co/api/v2/pokemon/884/	2022-11-03 23:54:37	2022-11-03 23:54:37	light-metalheavy-metal	\N
894	regieleki	1450	12	0	f	https://pokeapi.co/api/v2/pokemon/894/	2022-11-03 23:54:38	2022-11-03 23:54:38	transistor	\N
898	calyrex	77	11	0	f	https://pokeapi.co/api/v2/pokemon/898/	2022-11-03 23:54:39	2022-11-03 23:54:39	unnerve	\N
910	wormadam-trash	65	5	1	f	https://pokeapi.co/api/v2/pokemon/10005/	2022-11-03 23:54:41	2022-11-03 23:54:41	anticipation	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/413-trash.svg
916	rotom-fan	3	3	0	f	https://pokeapi.co/api/v2/pokemon/10011/	2022-11-03 23:54:43	2022-11-03 23:54:43	levitate	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/479-fan.svg
929	keldeo-resolute	485	14	0	f	https://pokeapi.co/api/v2/pokemon/10024/	2022-11-03 23:54:46	2022-11-03 23:54:46	justified	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/647-resolute.svg
938	venusaur-mega	1555	24	0	f	https://pokeapi.co/api/v2/pokemon/10033/	2022-11-03 23:54:47	2022-11-03 23:54:47	thick-fat	\N
946	gyarados-mega	3050	65	0	f	https://pokeapi.co/api/v2/pokemon/10041/	2022-11-03 23:54:48	2022-11-03 23:54:48	mold-breaker	\N
966	floette-eternal	9	2	1	f	https://pokeapi.co/api/v2/pokemon/10061/	2022-11-03 23:54:52	2022-11-03 23:54:52	flower-veil	\N
980	diancie-mega	278	11	0	f	https://pokeapi.co/api/v2/pokemon/10075/	2022-11-03 23:54:55	2022-11-03 23:54:55	magic-bounce	\N
987	pikachu-pop-star	60	4	1	f	https://pokeapi.co/api/v2/pokemon/10082/	2022-11-03 23:54:56	2022-11-03 23:54:56	static	\N
1002	pikachu-unova-cap	60	4	1	f	https://pokeapi.co/api/v2/pokemon/10097/	2022-11-03 23:54:59	2022-11-03 23:54:59	static	\N
1012	meowth-alola	42	4	1	f	https://pokeapi.co/api/v2/pokemon/10107/	2022-11-03 23:55:00	2022-11-03 23:55:00	pickuptechnician	\N
1017	grimer-alola	420	7	1	f	https://pokeapi.co/api/v2/pokemon/10112/	2022-11-03 23:55:01	2022-11-03 23:55:01	poison-touchgluttony	\N
1030	oricorio-sensu	34	6	0	f	https://pokeapi.co/api/v2/pokemon/10125/	2022-11-03 23:55:03	2022-11-03 23:55:03	dancer	\N
1045	minior-blue	3	3	0	f	https://pokeapi.co/api/v2/pokemon/10140/	2022-11-03 23:55:06	2022-11-03 23:55:06	shields-down	\N
1051	kommo-o-totem	2075	24	1	f	https://pokeapi.co/api/v2/pokemon/10146/	2022-11-03 23:55:07	2022-11-03 23:55:07	bulletproofsoundproof	\N
1059	togedemaru-totem	130	6	1	f	https://pokeapi.co/api/v2/pokemon/10154/	2022-11-03 23:55:08	2022-11-03 23:55:08	iron-barbslightning-rod	\N
1072	weezing-galar	160	30	1	f	https://pokeapi.co/api/v2/pokemon/10167/	2022-11-03 23:55:11	2022-11-03 23:55:11	levitateneutralizing-gas	\N
1082	darmanitan-galar-standard	1200	17	1	f	https://pokeapi.co/api/v2/pokemon/10177/	2022-11-03 23:55:12	2022-11-03 23:55:12	gorilla-tactics	\N
1091	indeedee-female	280	9	1	f	https://pokeapi.co/api/v2/pokemon/10186/	2022-11-03 23:55:13	2022-11-03 23:55:13	own-temposynchronize	\N
1101	charizard-gmax	10000	280	1	f	https://pokeapi.co/api/v2/pokemon/10196/	2022-11-03 23:55:14	2022-11-03 23:55:14	blaze	\N
1107	gengar-gmax	10000	200	0	f	https://pokeapi.co/api/v2/pokemon/10202/	2022-11-03 23:55:15	2022-11-03 23:55:15	cursed-body	\N
1117	corviknight-gmax	10000	140	1	f	https://pokeapi.co/api/v2/pokemon/10212/	2022-11-03 23:55:16	2022-11-03 23:55:16	pressureunnerve	\N
1127	grimmsnarl-gmax	10000	320	1	f	https://pokeapi.co/api/v2/pokemon/10222/	2022-11-03 23:55:17	2022-11-03 23:55:17	pranksterfrisk	\N
1134	growlithe-hisui	227	8	1	f	https://pokeapi.co/api/v2/pokemon/10229/	2022-11-03 23:55:18	2022-11-03 23:55:18	intimidateflash-fire	\N
833	chewtle	85	3	1	f	https://pokeapi.co/api/v2/pokemon/833/	2022-11-03 23:54:28	2022-11-03 23:54:28	strong-jawshell-armor	\N
843	silicobra	76	22	1	f	https://pokeapi.co/api/v2/pokemon/843/	2022-11-03 23:54:29	2022-11-03 23:54:29	sand-spitshed-skin	\N
851	centiskorch	1200	30	1	f	https://pokeapi.co/api/v2/pokemon/851/	2022-11-03 23:54:30	2022-11-03 23:54:30	flash-firewhite-smoke	\N
867	runerigus	666	16	0	f	https://pokeapi.co/api/v2/pokemon/867/	2022-11-03 23:54:33	2022-11-03 23:54:33	wandering-spirit	\N
873	frosmoth	420	13	1	f	https://pokeapi.co/api/v2/pokemon/873/	2022-11-03 23:54:34	2022-11-03 23:54:34	shield-dust	\N
885	dreepy	20	5	1	f	https://pokeapi.co/api/v2/pokemon/885/	2022-11-03 23:54:37	2022-11-03 23:54:37	clear-bodyinfiltrator	\N
895	regidrago	2000	21	0	f	https://pokeapi.co/api/v2/pokemon/895/	2022-11-03 23:54:38	2022-11-03 23:54:38	dragons-maw	\N
899	wyrdeer	951	18	1	f	https://pokeapi.co/api/v2/pokemon/899/	2022-11-03 23:54:39	2022-11-03 23:54:39	intimidatefrisk	\N
912	giratina-origin	6500	69	0	f	https://pokeapi.co/api/v2/pokemon/10007/	2022-11-03 23:54:42	2022-11-03 23:54:42	levitate	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/487-origin.svg
924	tornadus-therian	630	14	0	f	https://pokeapi.co/api/v2/pokemon/10019/	2022-11-03 23:54:45	2022-11-03 23:54:45	regenerator	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/641-therian.svg
931	aegislash-blade	530	17	0	f	https://pokeapi.co/api/v2/pokemon/10026/	2022-11-03 23:54:46	2022-11-03 23:54:46	stance-change	\N
940	charizard-mega-y	1005	17	0	f	https://pokeapi.co/api/v2/pokemon/10035/	2022-11-03 23:54:47	2022-11-03 23:54:47	drought	\N
948	mewtwo-mega-x	1270	23	0	f	https://pokeapi.co/api/v2/pokemon/10043/	2022-11-03 23:54:48	2022-11-03 23:54:48	steadfast	\N
958	aggron-mega	3950	22	0	f	https://pokeapi.co/api/v2/pokemon/10053/	2022-11-03 23:54:50	2022-11-03 23:54:50	filter	\N
962	absol-mega	490	12	0	f	https://pokeapi.co/api/v2/pokemon/10057/	2022-11-03 23:54:51	2022-11-03 23:54:51	magic-bounce	\N
975	sharpedo-mega	1303	25	0	f	https://pokeapi.co/api/v2/pokemon/10070/	2022-11-03 23:54:54	2022-11-03 23:54:54	strong-jaw	\N
991	hoopa-unbound	4900	65	0	f	https://pokeapi.co/api/v2/pokemon/10086/	2022-11-03 23:54:57	2022-11-03 23:54:57	magician	\N
997	raticate-alola	255	7	1	f	https://pokeapi.co/api/v2/pokemon/10092/	2022-11-03 23:54:58	2022-11-03 23:54:58	gluttonyhustle	\N
1005	raichu-alola	210	7	0	f	https://pokeapi.co/api/v2/pokemon/10100/	2022-11-03 23:54:59	2022-11-03 23:54:59	surge-surfer	\N
1018	muk-alola	520	10	1	f	https://pokeapi.co/api/v2/pokemon/10113/	2022-11-03 23:55:01	2022-11-03 23:55:01	poison-touchgluttony	\N
1029	oricorio-pau	34	6	0	f	https://pokeapi.co/api/v2/pokemon/10124/	2022-11-03 23:55:03	2022-11-03 23:55:03	dancer	\N
1046	minior-indigo	3	3	0	f	https://pokeapi.co/api/v2/pokemon/10141/	2022-11-03 23:55:06	2022-11-03 23:55:06	shields-down	\N
1052	magearna-original	805	10	0	f	https://pokeapi.co/api/v2/pokemon/10147/	2022-11-03 23:55:07	2022-11-03 23:55:07	soul-heart	\N
1067	ponyta-galar	240	8	1	f	https://pokeapi.co/api/v2/pokemon/10162/	2022-11-03 23:55:10	2022-11-03 23:55:10	run-awaypastel-veil	\N
1074	articuno-galar	509	17	0	f	https://pokeapi.co/api/v2/pokemon/10169/	2022-11-03 23:55:11	2022-11-03 23:55:11	competitive	\N
1084	yamask-galar	15	5	0	f	https://pokeapi.co/api/v2/pokemon/10179/	2022-11-03 23:55:12	2022-11-03 23:55:12	wandering-spirit	\N
1093	zacian-crowned	3550	28	0	f	https://pokeapi.co/api/v2/pokemon/10188/	2022-11-03 23:55:13	2022-11-03 23:55:13	intrepid-sword	\N
1103	butterfree-gmax	10000	170	1	f	https://pokeapi.co/api/v2/pokemon/10198/	2022-11-03 23:55:14	2022-11-03 23:55:14	compound-eyes	\N
1109	lapras-gmax	10000	240	1	f	https://pokeapi.co/api/v2/pokemon/10204/	2022-11-03 23:55:15	2022-11-03 23:55:15	water-absorbshell-armor	\N
1119	drednaw-gmax	10000	240	1	f	https://pokeapi.co/api/v2/pokemon/10214/	2022-11-03 23:55:16	2022-11-03 23:55:16	strong-jawshell-armor	\N
1129	copperajah-gmax	10000	230	1	f	https://pokeapi.co/api/v2/pokemon/10224/	2022-11-03 23:55:17	2022-11-03 23:55:17	sheer-force	\N
1136	voltorb-hisui	130	5	1	f	https://pokeapi.co/api/v2/pokemon/10231/	2022-11-03 23:55:18	2022-11-03 23:55:18	soundproofstatic	\N
834	drednaw	1155	10	1	f	https://pokeapi.co/api/v2/pokemon/834/	2022-11-03 23:54:28	2022-11-03 23:54:28	strong-jawshell-armor	\N
844	sandaconda	655	38	1	f	https://pokeapi.co/api/v2/pokemon/844/	2022-11-03 23:54:29	2022-11-03 23:54:29	sand-spitshed-skin	\N
852	clobbopus	40	6	1	f	https://pokeapi.co/api/v2/pokemon/852/	2022-11-03 23:54:30	2022-11-03 23:54:30	limber	\N
868	milcery	3	2	1	f	https://pokeapi.co/api/v2/pokemon/868/	2022-11-03 23:54:33	2022-11-03 23:54:33	sweet-veil	\N
874	stonjourner	5200	25	0	f	https://pokeapi.co/api/v2/pokemon/874/	2022-11-03 23:54:34	2022-11-03 23:54:34	power-spot	\N
886	drakloak	110	14	1	f	https://pokeapi.co/api/v2/pokemon/886/	2022-11-03 23:54:37	2022-11-03 23:54:37	clear-bodyinfiltrator	\N
900	kleavor	890	18	1	f	https://pokeapi.co/api/v2/pokemon/900/	2022-11-03 23:54:39	2022-11-03 23:54:39	swarmsheer-force	\N
918	castform-sunny	8	3	0	f	https://pokeapi.co/api/v2/pokemon/10013/	2022-11-03 23:54:44	2022-11-03 23:54:44	forecast	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/351-sunny.svg
925	thundurus-therian	610	30	0	f	https://pokeapi.co/api/v2/pokemon/10020/	2022-11-03 23:54:45	2022-11-03 23:54:45	volt-absorb	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/642-therian.svg
932	pumpkaboo-small	35	3	1	f	https://pokeapi.co/api/v2/pokemon/10027/	2022-11-03 23:54:46	2022-11-03 23:54:46	pickupfrisk	\N
949	mewtwo-mega-y	330	15	0	f	https://pokeapi.co/api/v2/pokemon/10044/	2022-11-03 23:54:48	2022-11-03 23:54:48	insomnia	\N
959	medicham-mega	315	13	0	f	https://pokeapi.co/api/v2/pokemon/10054/	2022-11-03 23:54:51	2022-11-03 23:54:51	pure-power	\N
963	garchomp-mega	950	19	0	f	https://pokeapi.co/api/v2/pokemon/10058/	2022-11-03 23:54:52	2022-11-03 23:54:52	sand-force	\N
978	pidgeot-mega	505	22	0	f	https://pokeapi.co/api/v2/pokemon/10073/	2022-11-03 23:54:55	2022-11-03 23:54:55	no-guard	\N
985	pikachu-rock-star	60	4	1	f	https://pokeapi.co/api/v2/pokemon/10080/	2022-11-03 23:54:56	2022-11-03 23:54:56	static	\N
995	beedrill-mega	405	14	0	f	https://pokeapi.co/api/v2/pokemon/10090/	2022-11-03 23:54:57	2022-11-03 23:54:57	adaptability	\N
1008	vulpix-alola	99	6	1	f	https://pokeapi.co/api/v2/pokemon/10103/	2022-11-03 23:55:00	2022-11-03 23:55:00	snow-cloak	\N
1013	persian-alola	330	11	1	f	https://pokeapi.co/api/v2/pokemon/10108/	2022-11-03 23:55:01	2022-11-03 23:55:01	fur-coattechnician	\N
1023	zygarde-10-power-construct	335	12	0	f	https://pokeapi.co/api/v2/pokemon/10118/	2022-11-03 23:55:02	2022-11-03 23:55:02	power-construct	\N
1027	vikavolt-totem	1475	26	0	f	https://pokeapi.co/api/v2/pokemon/10122/	2022-11-03 23:55:03	2022-11-03 23:55:03	levitate	\N
1044	minior-green	3	3	0	f	https://pokeapi.co/api/v2/pokemon/10139/	2022-11-03 23:55:06	2022-11-03 23:55:06	shields-down	\N
1050	mimikyu-totem-busted	28	4	0	f	https://pokeapi.co/api/v2/pokemon/10145/	2022-11-03 23:55:07	2022-11-03 23:55:07	disguise	\N
1058	araquanid-totem	2175	31	1	f	https://pokeapi.co/api/v2/pokemon/10153/	2022-11-03 23:55:08	2022-11-03 23:55:08	water-bubble	\N
1073	mr-mime-galar	568	14	1	f	https://pokeapi.co/api/v2/pokemon/10168/	2022-11-03 23:55:11	2022-11-03 23:55:11	vital-spiritscreen-cleaner	\N
1083	darmanitan-galar-zen	1200	17	1	f	https://pokeapi.co/api/v2/pokemon/10178/	2022-11-03 23:55:12	2022-11-03 23:55:12	gorilla-tactics	\N
1092	morpeko-hangry	30	3	0	f	https://pokeapi.co/api/v2/pokemon/10187/	2022-11-03 23:55:13	2022-11-03 23:55:13	hunger-switch	\N
1102	blastoise-gmax	10000	250	1	f	https://pokeapi.co/api/v2/pokemon/10197/	2022-11-03 23:55:14	2022-11-03 23:55:14	torrent	\N
1108	kingler-gmax	10000	190	1	f	https://pokeapi.co/api/v2/pokemon/10203/	2022-11-03 23:55:15	2022-11-03 23:55:15	hyper-cuttershell-armor	\N
1118	orbeetle-gmax	10000	140	1	f	https://pokeapi.co/api/v2/pokemon/10213/	2022-11-03 23:55:16	2022-11-03 23:55:16	swarmfrisk	\N
1128	alcremie-gmax	10000	300	1	f	https://pokeapi.co/api/v2/pokemon/10223/	2022-11-03 23:55:17	2022-11-03 23:55:17	sweet-veil	\N
1135	arcanine-hisui	1680	20	1	f	https://pokeapi.co/api/v2/pokemon/10230/	2022-11-03 23:55:18	2022-11-03 23:55:18	intimidateflash-fire	\N
835	yamper	135	3	1	f	https://pokeapi.co/api/v2/pokemon/835/	2022-11-03 23:54:28	2022-11-03 23:54:28	ball-fetch	\N
853	grapploct	390	16	1	f	https://pokeapi.co/api/v2/pokemon/853/	2022-11-03 23:54:30	2022-11-03 23:54:30	limber	\N
859	impidimp	55	4	1	f	https://pokeapi.co/api/v2/pokemon/859/	2022-11-03 23:54:31	2022-11-03 23:54:31	pranksterfrisk	\N
869	alcremie	5	3	1	f	https://pokeapi.co/api/v2/pokemon/869/	2022-11-03 23:54:33	2022-11-03 23:54:33	sweet-veil	\N
881	arctozolt	1500	23	1	f	https://pokeapi.co/api/v2/pokemon/881/	2022-11-03 23:54:36	2022-11-03 23:54:36	volt-absorbstatic	\N
888	zacian	1100	28	0	f	https://pokeapi.co/api/v2/pokemon/888/	2022-11-03 23:54:37	2022-11-03 23:54:37	intrepid-sword	\N
901	ursaluna	2900	24	1	f	https://pokeapi.co/api/v2/pokemon/901/	2022-11-03 23:54:39	2022-11-03 23:54:39	gutsbulletproof	\N
911	shaymin-sky	52	4	0	f	https://pokeapi.co/api/v2/pokemon/10006/	2022-11-03 23:54:42	2022-11-03 23:54:42	serene-grace	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/492-sky.svg
917	rotom-mow	3	3	0	f	https://pokeapi.co/api/v2/pokemon/10012/	2022-11-03 23:54:43	2022-11-03 23:54:43	levitate	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/479-mow.svg
930	meowstic-female	85	6	1	f	https://pokeapi.co/api/v2/pokemon/10025/	2022-11-03 23:54:46	2022-11-03 23:54:46	keen-eyeinfiltrator	\N
939	charizard-mega-x	1105	17	0	f	https://pokeapi.co/api/v2/pokemon/10034/	2022-11-03 23:54:47	2022-11-03 23:54:47	tough-claws	\N
947	aerodactyl-mega	790	21	0	f	https://pokeapi.co/api/v2/pokemon/10042/	2022-11-03 23:54:48	2022-11-03 23:54:48	tough-claws	\N
969	swampert-mega	1020	19	0	f	https://pokeapi.co/api/v2/pokemon/10064/	2022-11-03 23:54:53	2022-11-03 23:54:53	swift-swim	\N
973	gallade-mega	564	16	0	f	https://pokeapi.co/api/v2/pokemon/10068/	2022-11-03 23:54:54	2022-11-03 23:54:54	inner-focus	\N
992	camerupt-mega	3205	25	0	f	https://pokeapi.co/api/v2/pokemon/10087/	2022-11-03 23:54:57	2022-11-03 23:54:57	sheer-force	\N
998	raticate-totem-alola	1050	14	1	f	https://pokeapi.co/api/v2/pokemon/10093/	2022-11-03 23:54:58	2022-11-03 23:54:58	gluttonyhustle	\N
1006	sandshrew-alola	400	7	1	f	https://pokeapi.co/api/v2/pokemon/10101/	2022-11-03 23:54:59	2022-11-03 23:54:59	snow-cloak	\N
1021	greninja-battle-bond	400	15	0	f	https://pokeapi.co/api/v2/pokemon/10116/	2022-11-03 23:55:02	2022-11-03 23:55:02	battle-bond	\N
1025	zygarde-complete	6100	45	0	f	https://pokeapi.co/api/v2/pokemon/10120/	2022-11-03 23:55:03	2022-11-03 23:55:03	power-construct	\N
1035	minior-orange-meteor	400	3	0	f	https://pokeapi.co/api/v2/pokemon/10130/	2022-11-03 23:55:04	2022-11-03 23:55:04	shields-down	\N
1041	minior-red	3	3	0	f	https://pokeapi.co/api/v2/pokemon/10136/	2022-11-03 23:55:05	2022-11-03 23:55:05	shields-down	\N
1056	rockruff-own-tempo	92	5	0	f	https://pokeapi.co/api/v2/pokemon/10151/	2022-11-03 23:55:08	2022-11-03 23:55:08	own-tempo	\N
1065	pikachu-world-cap	60	4	1	f	https://pokeapi.co/api/v2/pokemon/10160/	2022-11-03 23:55:09	2022-11-03 23:55:09	static	\N
1080	linoone-galar	325	5	1	f	https://pokeapi.co/api/v2/pokemon/10175/	2022-11-03 23:55:12	2022-11-03 23:55:12	pickupgluttony	\N
1089	toxtricity-low-key	400	16	1	f	https://pokeapi.co/api/v2/pokemon/10184/	2022-11-03 23:55:12	2022-11-03 23:55:12	punk-rockminus	\N
1099	calyrex-shadow	536	24	0	f	https://pokeapi.co/api/v2/pokemon/10194/	2022-11-03 23:55:13	2022-11-03 23:55:13	as-one-spectrier	\N
1105	meowth-gmax	10000	330	1	f	https://pokeapi.co/api/v2/pokemon/10200/	2022-11-03 23:55:14	2022-11-03 23:55:14	pickuptechnician	\N
1115	cinderace-gmax	10000	270	1	f	https://pokeapi.co/api/v2/pokemon/10210/	2022-11-03 23:55:16	2022-11-03 23:55:16	blaze	\N
1125	centiskorch-gmax	10000	750	1	f	https://pokeapi.co/api/v2/pokemon/10220/	2022-11-03 23:55:17	2022-11-03 23:55:17	flash-firewhite-smoke	\N
1132	urshifu-rapid-strike-gmax	10000	260	0	f	https://pokeapi.co/api/v2/pokemon/10227/	2022-11-03 23:55:18	2022-11-03 23:55:18	unseen-fist	\N
836	boltund	340	10	1	f	https://pokeapi.co/api/v2/pokemon/836/	2022-11-03 23:54:28	2022-11-03 23:54:28	strong-jaw	\N
854	sinistea	2	1	1	f	https://pokeapi.co/api/v2/pokemon/854/	2022-11-03 23:54:31	2022-11-03 23:54:31	weak-armor	\N
860	morgrem	125	8	1	f	https://pokeapi.co/api/v2/pokemon/860/	2022-11-03 23:54:32	2022-11-03 23:54:32	pranksterfrisk	\N
870	falinks	620	30	1	f	https://pokeapi.co/api/v2/pokemon/870/	2022-11-03 23:54:33	2022-11-03 23:54:33	battle-armor	\N
880	dracozolt	1900	18	1	f	https://pokeapi.co/api/v2/pokemon/880/	2022-11-03 23:54:36	2022-11-03 23:54:36	volt-absorbhustle	\N
887	dragapult	500	30	1	f	https://pokeapi.co/api/v2/pokemon/887/	2022-11-03 23:54:37	2022-11-03 23:54:37	clear-bodyinfiltrator	\N
902	basculegion-male	1100	30	1	f	https://pokeapi.co/api/v2/pokemon/902/	2022-11-03 23:54:39	2022-11-03 23:54:39	rattledadaptability	\N
920	castform-snowy	8	3	0	f	https://pokeapi.co/api/v2/pokemon/10015/	2022-11-03 23:54:44	2022-11-03 23:54:44	forecast	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/351-snowy.svg
927	kyurem-black	3250	33	0	f	https://pokeapi.co/api/v2/pokemon/10022/	2022-11-03 23:54:45	2022-11-03 23:54:45	teravolt	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/646-black.svg
941	blastoise-mega	1011	16	0	f	https://pokeapi.co/api/v2/pokemon/10036/	2022-11-03 23:54:47	2022-11-03 23:54:47	mega-launcher	\N
951	scizor-mega	1250	20	0	f	https://pokeapi.co/api/v2/pokemon/10046/	2022-11-03 23:54:49	2022-11-03 23:54:49	technician	\N
955	blaziken-mega	520	19	0	f	https://pokeapi.co/api/v2/pokemon/10050/	2022-11-03 23:54:50	2022-11-03 23:54:50	speed-boost	\N
967	latias-mega	520	18	0	f	https://pokeapi.co/api/v2/pokemon/10062/	2022-11-03 23:54:52	2022-11-03 23:54:52	levitate	\N
971	sableye-mega	1610	5	0	f	https://pokeapi.co/api/v2/pokemon/10066/	2022-11-03 23:54:53	2022-11-03 23:54:53	magic-bounce	\N
981	metagross-mega	9429	25	0	f	https://pokeapi.co/api/v2/pokemon/10076/	2022-11-03 23:54:55	2022-11-03 23:54:55	tough-claws	\N
988	pikachu-phd	60	4	1	f	https://pokeapi.co/api/v2/pokemon/10083/	2022-11-03 23:54:56	2022-11-03 23:54:56	static	\N
1001	pikachu-sinnoh-cap	60	4	1	f	https://pokeapi.co/api/v2/pokemon/10096/	2022-11-03 23:54:59	2022-11-03 23:54:59	static	\N
1011	dugtrio-alola	666	7	1	f	https://pokeapi.co/api/v2/pokemon/10106/	2022-11-03 23:55:00	2022-11-03 23:55:00	sand-veiltangling-hair	\N
1016	golem-alola	3160	17	1	f	https://pokeapi.co/api/v2/pokemon/10111/	2022-11-03 23:55:01	2022-11-03 23:55:01	magnet-pullsturdy	\N
1028	oricorio-pom-pom	34	6	0	f	https://pokeapi.co/api/v2/pokemon/10123/	2022-11-03 23:55:03	2022-11-03 23:55:03	dancer	\N
1043	minior-yellow	3	3	0	f	https://pokeapi.co/api/v2/pokemon/10138/	2022-11-03 23:55:05	2022-11-03 23:55:05	shields-down	\N
1049	mimikyu-totem-disguised	28	4	0	f	https://pokeapi.co/api/v2/pokemon/10144/	2022-11-03 23:55:07	2022-11-03 23:55:07	disguise	\N
1057	lycanroc-dusk	250	8	0	f	https://pokeapi.co/api/v2/pokemon/10152/	2022-11-03 23:55:08	2022-11-03 23:55:08	tough-claws	\N
1066	meowth-galar	75	4	1	f	https://pokeapi.co/api/v2/pokemon/10161/	2022-11-03 23:55:09	2022-11-03 23:55:09	pickuptough-claws	\N
1079	zigzagoon-galar	175	4	1	f	https://pokeapi.co/api/v2/pokemon/10174/	2022-11-03 23:55:11	2022-11-03 23:55:11	pickupgluttony	\N
1098	calyrex-ice	8091	24	0	f	https://pokeapi.co/api/v2/pokemon/10193/	2022-11-03 23:55:13	2022-11-03 23:55:13	as-one-glastrier	\N
1113	melmetal-gmax	10000	250	0	f	https://pokeapi.co/api/v2/pokemon/10208/	2022-11-03 23:55:15	2022-11-03 23:55:15	iron-fist	\N
1123	sandaconda-gmax	10000	220	1	f	https://pokeapi.co/api/v2/pokemon/10218/	2022-11-03 23:55:16	2022-11-03 23:55:16	sand-spitshed-skin	\N
1140	sneasel-hisui	270	9	1	f	https://pokeapi.co/api/v2/pokemon/10235/	2022-11-03 23:55:19	2022-11-03 23:55:19	inner-focuskeen-eye	\N
837	rolycoly	120	3	1	f	https://pokeapi.co/api/v2/pokemon/837/	2022-11-03 23:54:28	2022-11-03 23:54:28	steam-engineheatproof	\N
845	cramorant	180	8	0	f	https://pokeapi.co/api/v2/pokemon/845/	2022-11-03 23:54:29	2022-11-03 23:54:29	gulp-missile	\N
855	polteageist	4	2	1	f	https://pokeapi.co/api/v2/pokemon/855/	2022-11-03 23:54:31	2022-11-03 23:54:31	weak-armor	\N
861	grimmsnarl	610	15	1	f	https://pokeapi.co/api/v2/pokemon/861/	2022-11-03 23:54:32	2022-11-03 23:54:32	pranksterfrisk	\N
875	eiscue-ice	890	14	0	f	https://pokeapi.co/api/v2/pokemon/875/	2022-11-03 23:54:35	2022-11-03 23:54:35	ice-face	\N
882	dracovish	2150	23	1	f	https://pokeapi.co/api/v2/pokemon/882/	2022-11-03 23:54:36	2022-11-03 23:54:36	water-absorbstrong-jaw	\N
889	zamazenta	2100	29	0	f	https://pokeapi.co/api/v2/pokemon/889/	2022-11-03 23:54:37	2022-11-03 23:54:37	dauntless-shield	\N
904	overqwil	605	25	1	f	https://pokeapi.co/api/v2/pokemon/904/	2022-11-03 23:54:40	2022-11-03 23:54:40	poison-pointswift-swim	\N
909	wormadam-sandy	65	5	1	f	https://pokeapi.co/api/v2/pokemon/10004/	2022-11-03 23:54:41	2022-11-03 23:54:41	anticipation	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/413-sandy.svg
921	basculin-blue-striped	180	10	1	f	https://pokeapi.co/api/v2/pokemon/10016/	2022-11-03 23:54:44	2022-11-03 23:54:44	rock-headadaptability	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/550-blue-striped.svg
935	gourgeist-small	95	7	1	f	https://pokeapi.co/api/v2/pokemon/10030/	2022-11-03 23:54:47	2022-11-03 23:54:47	pickupfrisk	\N
943	gengar-mega	405	14	0	f	https://pokeapi.co/api/v2/pokemon/10038/	2022-11-03 23:54:48	2022-11-03 23:54:48	shadow-tag	\N
953	houndoom-mega	495	19	0	f	https://pokeapi.co/api/v2/pokemon/10048/	2022-11-03 23:54:49	2022-11-03 23:54:49	solar-power	\N
957	mawile-mega	235	10	0	f	https://pokeapi.co/api/v2/pokemon/10052/	2022-11-03 23:54:50	2022-11-03 23:54:50	huge-power	\N
970	sceptile-mega	552	19	0	f	https://pokeapi.co/api/v2/pokemon/10065/	2022-11-03 23:54:53	2022-11-03 23:54:53	lightning-rod	\N
974	audino-mega	320	15	0	f	https://pokeapi.co/api/v2/pokemon/10069/	2022-11-03 23:54:54	2022-11-03 23:54:54	healer	\N
990	pikachu-cosplay	60	4	1	f	https://pokeapi.co/api/v2/pokemon/10085/	2022-11-03 23:54:56	2022-11-03 23:54:56	static	\N
996	rattata-alola	38	3	1	f	https://pokeapi.co/api/v2/pokemon/10091/	2022-11-03 23:54:58	2022-11-03 23:54:58	gluttonyhustle	\N
1004	pikachu-alola-cap	60	4	1	f	https://pokeapi.co/api/v2/pokemon/10099/	2022-11-03 23:54:59	2022-11-03 23:54:59	static	\N
1022	greninja-ash	400	15	0	f	https://pokeapi.co/api/v2/pokemon/10117/	2022-11-03 23:55:02	2022-11-03 23:55:02	battle-bond	\N
1026	gumshoos-totem	600	14	1	f	https://pokeapi.co/api/v2/pokemon/10121/	2022-11-03 23:55:03	2022-11-03 23:55:03	stakeoutstrong-jaw	\N
1036	minior-yellow-meteor	400	3	0	f	https://pokeapi.co/api/v2/pokemon/10131/	2022-11-03 23:55:04	2022-11-03 23:55:04	shields-down	\N
1042	minior-orange	3	3	0	f	https://pokeapi.co/api/v2/pokemon/10137/	2022-11-03 23:55:05	2022-11-03 23:55:05	shields-down	\N
1055	ribombee-totem	20	4	1	f	https://pokeapi.co/api/v2/pokemon/10150/	2022-11-03 23:55:08	2022-11-03 23:55:08	honey-gathershield-dust	\N
1064	eevee-starter	65	3	1	f	https://pokeapi.co/api/v2/pokemon/10159/	2022-11-03 23:55:09	2022-11-03 23:55:09	run-awayadaptability	\N
1081	darumaka-galar	400	7	1	f	https://pokeapi.co/api/v2/pokemon/10176/	2022-11-03 23:55:12	2022-11-03 23:55:12	hustle	\N
1090	eiscue-noice	890	14	0	f	https://pokeapi.co/api/v2/pokemon/10185/	2022-11-03 23:55:13	2022-11-03 23:55:13	ice-face	\N
1100	venusaur-gmax	10000	240	1	f	https://pokeapi.co/api/v2/pokemon/10195/	2022-11-03 23:55:14	2022-11-03 23:55:14	overgrow	\N
1106	machamp-gmax	10000	250	1	f	https://pokeapi.co/api/v2/pokemon/10201/	2022-11-03 23:55:15	2022-11-03 23:55:15	gutsno-guard	\N
1116	inteleon-gmax	10000	400	1	f	https://pokeapi.co/api/v2/pokemon/10211/	2022-11-03 23:55:16	2022-11-03 23:55:16	torrent	\N
1126	hatterene-gmax	10000	260	1	f	https://pokeapi.co/api/v2/pokemon/10221/	2022-11-03 23:55:17	2022-11-03 23:55:17	healeranticipation	\N
1133	toxtricity-low-key-gmax	10000	240	1	f	https://pokeapi.co/api/v2/pokemon/10228/	2022-11-03 23:55:18	2022-11-03 23:55:18	punk-rockminus	\N
838	carkol	780	11	1	f	https://pokeapi.co/api/v2/pokemon/838/	2022-11-03 23:54:29	2022-11-03 23:54:29	steam-engineflame-body	\N
846	arrokuda	10	5	1	f	https://pokeapi.co/api/v2/pokemon/846/	2022-11-03 23:54:30	2022-11-03 23:54:30	swift-swim	\N
856	hatenna	34	4	1	f	https://pokeapi.co/api/v2/pokemon/856/	2022-11-03 23:54:31	2022-11-03 23:54:31	healeranticipation	\N
862	obstagoon	460	16	1	f	https://pokeapi.co/api/v2/pokemon/862/	2022-11-03 23:54:32	2022-11-03 23:54:32	recklessguts	\N
876	indeedee-male	280	9	1	f	https://pokeapi.co/api/v2/pokemon/876/	2022-11-03 23:54:35	2022-11-03 23:54:35	inner-focussynchronize	\N
883	arctovish	1750	20	1	f	https://pokeapi.co/api/v2/pokemon/883/	2022-11-03 23:54:36	2022-11-03 23:54:36	water-absorbice-body	\N
890	eternatus	9500	200	0	f	https://pokeapi.co/api/v2/pokemon/890/	2022-11-03 23:54:37	2022-11-03 23:54:37	pressure	\N
905	enamorus-incarnate	480	16	1	f	https://pokeapi.co/api/v2/pokemon/905/	2022-11-03 23:54:40	2022-11-03 23:54:40	healer	\N
913	rotom-heat	3	3	0	f	https://pokeapi.co/api/v2/pokemon/10008/	2022-11-03 23:54:43	2022-11-03 23:54:43	levitate	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/479-heat.svg
922	darmanitan-zen	929	13	1	f	https://pokeapi.co/api/v2/pokemon/10017/	2022-11-03 23:54:44	2022-11-03 23:54:44	sheer-force	\N
934	pumpkaboo-super	150	8	1	f	https://pokeapi.co/api/v2/pokemon/10029/	2022-11-03 23:54:47	2022-11-03 23:54:47	pickupfrisk	\N
942	alakazam-mega	480	12	0	f	https://pokeapi.co/api/v2/pokemon/10037/	2022-11-03 23:54:48	2022-11-03 23:54:48	trace	\N
952	heracross-mega	625	17	0	f	https://pokeapi.co/api/v2/pokemon/10047/	2022-11-03 23:54:49	2022-11-03 23:54:49	skill-link	\N
956	gardevoir-mega	484	16	0	f	https://pokeapi.co/api/v2/pokemon/10051/	2022-11-03 23:54:50	2022-11-03 23:54:50	pixilate	\N
968	latios-mega	700	23	0	f	https://pokeapi.co/api/v2/pokemon/10063/	2022-11-03 23:54:53	2022-11-03 23:54:53	levitate	\N
972	altaria-mega	206	15	0	f	https://pokeapi.co/api/v2/pokemon/10067/	2022-11-03 23:54:54	2022-11-03 23:54:54	pixilate	\N
982	kyogre-primal	4300	98	0	f	https://pokeapi.co/api/v2/pokemon/10077/	2022-11-03 23:54:55	2022-11-03 23:54:55	primordial-sea	\N
989	pikachu-libre	60	4	1	f	https://pokeapi.co/api/v2/pokemon/10084/	2022-11-03 23:54:56	2022-11-03 23:54:56	static	\N
1000	pikachu-hoenn-cap	60	4	1	f	https://pokeapi.co/api/v2/pokemon/10095/	2022-11-03 23:54:58	2022-11-03 23:54:58	static	\N
1010	diglett-alola	10	2	1	f	https://pokeapi.co/api/v2/pokemon/10105/	2022-11-03 23:55:00	2022-11-03 23:55:00	sand-veiltangling-hair	\N
1015	graveler-alola	1100	10	1	f	https://pokeapi.co/api/v2/pokemon/10110/	2022-11-03 23:55:01	2022-11-03 23:55:01	magnet-pullsturdy	\N
1031	lycanroc-midnight	250	11	1	f	https://pokeapi.co/api/v2/pokemon/10126/	2022-11-03 23:55:03	2022-11-03 23:55:03	keen-eyevital-spirit	\N
1037	minior-green-meteor	400	3	0	f	https://pokeapi.co/api/v2/pokemon/10132/	2022-11-03 23:55:04	2022-11-03 23:55:04	shields-down	\N
1047	minior-violet	3	3	0	f	https://pokeapi.co/api/v2/pokemon/10142/	2022-11-03 23:55:06	2022-11-03 23:55:06	shields-down	\N
1060	necrozma-dusk	4600	38	0	f	https://pokeapi.co/api/v2/pokemon/10155/	2022-11-03 23:55:08	2022-11-03 23:55:08	prism-armor	\N
1068	rapidash-galar	800	17	1	f	https://pokeapi.co/api/v2/pokemon/10163/	2022-11-03 23:55:10	2022-11-03 23:55:10	run-awaypastel-veil	\N
1075	zapdos-galar	582	16	0	f	https://pokeapi.co/api/v2/pokemon/10170/	2022-11-03 23:55:11	2022-11-03 23:55:11	defiant	\N
1085	stunfisk-galar	205	7	0	f	https://pokeapi.co/api/v2/pokemon/10180/	2022-11-03 23:55:12	2022-11-03 23:55:12	mimicry	\N
1094	zamazenta-crowned	7850	29	0	f	https://pokeapi.co/api/v2/pokemon/10189/	2022-11-03 23:55:13	2022-11-03 23:55:13	dauntless-shield	\N
1104	pikachu-gmax	10000	210	1	f	https://pokeapi.co/api/v2/pokemon/10199/	2022-11-03 23:55:14	2022-11-03 23:55:14	static	\N
1110	eevee-gmax	10000	180	1	f	https://pokeapi.co/api/v2/pokemon/10205/	2022-11-03 23:55:15	2022-11-03 23:55:15	run-awayadaptability	\N
1120	coalossal-gmax	10000	420	1	f	https://pokeapi.co/api/v2/pokemon/10215/	2022-11-03 23:55:16	2022-11-03 23:55:16	steam-engineflame-body	\N
1130	duraludon-gmax	10000	430	1	f	https://pokeapi.co/api/v2/pokemon/10225/	2022-11-03 23:55:17	2022-11-03 23:55:17	light-metalheavy-metal	\N
1137	electrode-hisui	710	12	1	f	https://pokeapi.co/api/v2/pokemon/10232/	2022-11-03 23:55:18	2022-11-03 23:55:18	soundproofstatic	\N
839	coalossal	3105	28	1	f	https://pokeapi.co/api/v2/pokemon/839/	2022-11-03 23:54:29	2022-11-03 23:54:29	steam-engineflame-body	\N
847	barraskewda	300	13	1	f	https://pokeapi.co/api/v2/pokemon/847/	2022-11-03 23:54:30	2022-11-03 23:54:30	swift-swim	\N
857	hattrem	48	6	1	f	https://pokeapi.co/api/v2/pokemon/857/	2022-11-03 23:54:31	2022-11-03 23:54:31	healeranticipation	\N
863	perrserker	280	8	1	f	https://pokeapi.co/api/v2/pokemon/863/	2022-11-03 23:54:32	2022-11-03 23:54:32	battle-armortough-claws	\N
878	cufant	1000	12	1	f	https://pokeapi.co/api/v2/pokemon/878/	2022-11-03 23:54:35	2022-11-03 23:54:35	sheer-force	\N
892	urshifu-single-strike	1050	19	0	f	https://pokeapi.co/api/v2/pokemon/892/	2022-11-03 23:54:38	2022-11-03 23:54:38	unseen-fist	\N
896	glastrier	8000	22	0	f	https://pokeapi.co/api/v2/pokemon/896/	2022-11-03 23:54:38	2022-11-03 23:54:38	chilling-neigh	\N
906	deoxys-attack	608	17	0	f	https://pokeapi.co/api/v2/pokemon/10001/	2022-11-03 23:54:40	2022-11-03 23:54:40	pressure	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/386-attack.svg
915	rotom-frost	3	3	0	f	https://pokeapi.co/api/v2/pokemon/10010/	2022-11-03 23:54:43	2022-11-03 23:54:43	levitate	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/479-frost.svg
928	kyurem-white	3250	36	0	f	https://pokeapi.co/api/v2/pokemon/10023/	2022-11-03 23:54:45	2022-11-03 23:54:45	turboblaze	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/646-white.svg
937	gourgeist-super	390	17	1	f	https://pokeapi.co/api/v2/pokemon/10032/	2022-11-03 23:54:47	2022-11-03 23:54:47	pickupfrisk	\N
945	pinsir-mega	590	17	0	f	https://pokeapi.co/api/v2/pokemon/10040/	2022-11-03 23:54:48	2022-11-03 23:54:48	aerilate	\N
960	manectric-mega	440	18	0	f	https://pokeapi.co/api/v2/pokemon/10055/	2022-11-03 23:54:51	2022-11-03 23:54:51	intimidate	\N
964	lucario-mega	575	13	0	f	https://pokeapi.co/api/v2/pokemon/10059/	2022-11-03 23:54:52	2022-11-03 23:54:52	adaptability	\N
977	steelix-mega	7400	105	0	f	https://pokeapi.co/api/v2/pokemon/10072/	2022-11-03 23:54:55	2022-11-03 23:54:55	sand-force	\N
984	rayquaza-mega	3920	108	0	f	https://pokeapi.co/api/v2/pokemon/10079/	2022-11-03 23:54:56	2022-11-03 23:54:56	delta-stream	\N
994	salamence-mega	1126	18	0	f	https://pokeapi.co/api/v2/pokemon/10089/	2022-11-03 23:54:57	2022-11-03 23:54:57	aerilate	\N
1009	ninetales-alola	199	11	1	f	https://pokeapi.co/api/v2/pokemon/10104/	2022-11-03 23:55:00	2022-11-03 23:55:00	snow-cloak	\N
1014	geodude-alola	203	4	1	f	https://pokeapi.co/api/v2/pokemon/10109/	2022-11-03 23:55:01	2022-11-03 23:55:01	magnet-pullsturdy	\N
1032	wishiwashi-school	786	82	0	f	https://pokeapi.co/api/v2/pokemon/10127/	2022-11-03 23:55:04	2022-11-03 23:55:04	schooling	\N
1038	minior-blue-meteor	400	3	0	f	https://pokeapi.co/api/v2/pokemon/10133/	2022-11-03 23:55:05	2022-11-03 23:55:05	shields-down	\N
1048	mimikyu-busted	7	2	0	f	https://pokeapi.co/api/v2/pokemon/10143/	2022-11-03 23:55:06	2022-11-03 23:55:06	disguise	\N
1061	necrozma-dawn	3500	42	0	f	https://pokeapi.co/api/v2/pokemon/10156/	2022-11-03 23:55:09	2022-11-03 23:55:09	prism-armor	\N
1069	slowpoke-galar	360	12	1	f	https://pokeapi.co/api/v2/pokemon/10164/	2022-11-03 23:55:10	2022-11-03 23:55:10	gluttonyown-tempo	\N
1076	moltres-galar	660	20	0	f	https://pokeapi.co/api/v2/pokemon/10171/	2022-11-03 23:55:11	2022-11-03 23:55:11	berserk	\N
1086	zygarde-10	335	12	0	f	https://pokeapi.co/api/v2/pokemon/10181/	2022-11-03 23:55:12	2022-11-03 23:55:12	aura-break	\N
1095	eternatus-eternamax	0	1000	0	f	https://pokeapi.co/api/v2/pokemon/10190/	2022-11-03 23:55:13	2022-11-03 23:55:13	pressure	\N
1111	snorlax-gmax	10000	350	1	f	https://pokeapi.co/api/v2/pokemon/10206/	2022-11-03 23:55:15	2022-11-03 23:55:15	immunitythick-fat	\N
1121	flapple-gmax	10000	240	1	f	https://pokeapi.co/api/v2/pokemon/10216/	2022-11-03 23:55:16	2022-11-03 23:55:16	ripengluttony	\N
1138	typhlosion-hisui	698	16	1	f	https://pokeapi.co/api/v2/pokemon/10233/	2022-11-03 23:55:18	2022-11-03 23:55:18	blaze	\N
840	applin	5	2	1	f	https://pokeapi.co/api/v2/pokemon/840/	2022-11-03 23:54:29	2022-11-03 23:54:29	ripengluttony	\N
848	toxel	110	4	1	f	https://pokeapi.co/api/v2/pokemon/848/	2022-11-03 23:54:30	2022-11-03 23:54:30	rattledstatic	\N
858	hatterene	51	21	1	f	https://pokeapi.co/api/v2/pokemon/858/	2022-11-03 23:54:31	2022-11-03 23:54:31	healeranticipation	\N
864	cursola	4	10	1	f	https://pokeapi.co/api/v2/pokemon/864/	2022-11-03 23:54:32	2022-11-03 23:54:32	weak-armor	\N
877	morpeko-full-belly	30	3	0	f	https://pokeapi.co/api/v2/pokemon/877/	2022-11-03 23:54:35	2022-11-03 23:54:35	hunger-switch	\N
891	kubfu	120	6	0	f	https://pokeapi.co/api/v2/pokemon/891/	2022-11-03 23:54:37	2022-11-03 23:54:37	inner-focus	\N
903	sneasler	430	13	1	f	https://pokeapi.co/api/v2/pokemon/903/	2022-11-03 23:54:40	2022-11-03 23:54:40	pressure	\N
908	deoxys-speed	608	17	0	f	https://pokeapi.co/api/v2/pokemon/10003/	2022-11-03 23:54:41	2022-11-03 23:54:41	pressure	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/386-speed.svg
919	castform-rainy	8	3	0	f	https://pokeapi.co/api/v2/pokemon/10014/	2022-11-03 23:54:44	2022-11-03 23:54:44	forecast	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/351-rainy.svg
926	landorus-therian	680	13	0	f	https://pokeapi.co/api/v2/pokemon/10021/	2022-11-03 23:54:45	2022-11-03 23:54:45	intimidate	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/645-therian.svg
933	pumpkaboo-large	75	5	1	f	https://pokeapi.co/api/v2/pokemon/10028/	2022-11-03 23:54:46	2022-11-03 23:54:46	pickupfrisk	\N
950	ampharos-mega	615	14	0	f	https://pokeapi.co/api/v2/pokemon/10045/	2022-11-03 23:54:49	2022-11-03 23:54:49	mold-breaker	\N
954	tyranitar-mega	2550	25	0	f	https://pokeapi.co/api/v2/pokemon/10049/	2022-11-03 23:54:50	2022-11-03 23:54:50	sand-stream	\N
979	glalie-mega	3502	21	0	f	https://pokeapi.co/api/v2/pokemon/10074/	2022-11-03 23:54:55	2022-11-03 23:54:55	refrigerate	\N
986	pikachu-belle	60	4	1	f	https://pokeapi.co/api/v2/pokemon/10081/	2022-11-03 23:54:56	2022-11-03 23:54:56	static	\N
1003	pikachu-kalos-cap	60	4	1	f	https://pokeapi.co/api/v2/pokemon/10098/	2022-11-03 23:54:59	2022-11-03 23:54:59	static	\N
1020	marowak-alola	340	10	1	f	https://pokeapi.co/api/v2/pokemon/10115/	2022-11-03 23:55:02	2022-11-03 23:55:02	cursed-bodylightning-rod	\N
1024	zygarde-50-power-construct	3050	50	0	f	https://pokeapi.co/api/v2/pokemon/10119/	2022-11-03 23:55:03	2022-11-03 23:55:03	power-construct	\N
1034	salazzle-totem	810	21	1	f	https://pokeapi.co/api/v2/pokemon/10129/	2022-11-03 23:55:04	2022-11-03 23:55:04	corrosion	\N
1040	minior-violet-meteor	400	3	0	f	https://pokeapi.co/api/v2/pokemon/10135/	2022-11-03 23:55:05	2022-11-03 23:55:05	shields-down	\N
1053	pikachu-partner-cap	60	4	1	f	https://pokeapi.co/api/v2/pokemon/10148/	2022-11-03 23:55:07	2022-11-03 23:55:07	static	\N
1062	necrozma-ultra	2300	75	0	f	https://pokeapi.co/api/v2/pokemon/10157/	2022-11-03 23:55:09	2022-11-03 23:55:09	neuroforce	\N
1070	slowbro-galar	705	16	1	f	https://pokeapi.co/api/v2/pokemon/10165/	2022-11-03 23:55:10	2022-11-03 23:55:10	quick-drawown-tempo	\N
1077	slowking-galar	795	18	1	f	https://pokeapi.co/api/v2/pokemon/10172/	2022-11-03 23:55:11	2022-11-03 23:55:11	curious-medicineown-tempo	\N
1087	cramorant-gulping	180	8	0	f	https://pokeapi.co/api/v2/pokemon/10182/	2022-11-03 23:55:12	2022-11-03 23:55:12	gulp-missile	\N
1096	urshifu-rapid-strike	1050	19	0	f	https://pokeapi.co/api/v2/pokemon/10191/	2022-11-03 23:55:13	2022-11-03 23:55:13	unseen-fist	\N
1114	rillaboom-gmax	10000	280	1	f	https://pokeapi.co/api/v2/pokemon/10209/	2022-11-03 23:55:15	2022-11-03 23:55:15	overgrow	\N
1124	toxtricity-amped-gmax	10000	240	1	f	https://pokeapi.co/api/v2/pokemon/10219/	2022-11-03 23:55:17	2022-11-03 23:55:17	punk-rockplus	\N
1131	urshifu-single-strike-gmax	10000	290	0	f	https://pokeapi.co/api/v2/pokemon/10226/	2022-11-03 23:55:17	2022-11-03 23:55:17	unseen-fist	\N
841	flapple	10	3	1	f	https://pokeapi.co/api/v2/pokemon/841/	2022-11-03 23:54:29	2022-11-03 23:54:29	ripengluttony	\N
849	toxtricity-amped	400	16	1	f	https://pokeapi.co/api/v2/pokemon/849/	2022-11-03 23:54:30	2022-11-03 23:54:30	punk-rockplus	\N
865	sirfetchd	1170	8	1	f	https://pokeapi.co/api/v2/pokemon/865/	2022-11-03 23:54:33	2022-11-03 23:54:33	steadfast	\N
871	pincurchin	10	3	1	f	https://pokeapi.co/api/v2/pokemon/871/	2022-11-03 23:54:34	2022-11-03 23:54:34	lightning-rod	\N
879	copperajah	6500	30	1	f	https://pokeapi.co/api/v2/pokemon/879/	2022-11-03 23:54:35	2022-11-03 23:54:35	sheer-force	\N
893	zarude	700	18	0	f	https://pokeapi.co/api/v2/pokemon/893/	2022-11-03 23:54:38	2022-11-03 23:54:38	leaf-guard	\N
897	spectrier	445	20	0	f	https://pokeapi.co/api/v2/pokemon/897/	2022-11-03 23:54:39	2022-11-03 23:54:39	grim-neigh	\N
907	deoxys-defense	608	17	0	f	https://pokeapi.co/api/v2/pokemon/10002/	2022-11-03 23:54:40	2022-11-03 23:54:40	pressure	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/386-defense.svg
914	rotom-wash	3	3	0	f	https://pokeapi.co/api/v2/pokemon/10009/	2022-11-03 23:54:43	2022-11-03 23:54:43	levitate	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/479-wash.svg
923	meloetta-pirouette	65	6	0	f	https://pokeapi.co/api/v2/pokemon/10018/	2022-11-03 23:54:44	2022-11-03 23:54:44	serene-grace	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/648-pirouette.svg
936	gourgeist-large	140	11	1	f	https://pokeapi.co/api/v2/pokemon/10031/	2022-11-03 23:54:47	2022-11-03 23:54:47	pickupfrisk	\N
944	kangaskhan-mega	1000	22	0	f	https://pokeapi.co/api/v2/pokemon/10039/	2022-11-03 23:54:48	2022-11-03 23:54:48	parental-bond	\N
961	banette-mega	130	12	0	f	https://pokeapi.co/api/v2/pokemon/10056/	2022-11-03 23:54:51	2022-11-03 23:54:51	prankster	\N
965	abomasnow-mega	1850	27	0	f	https://pokeapi.co/api/v2/pokemon/10060/	2022-11-03 23:54:52	2022-11-03 23:54:52	snow-warning	\N
976	slowbro-mega	1200	20	0	f	https://pokeapi.co/api/v2/pokemon/10071/	2022-11-03 23:54:55	2022-11-03 23:54:55	shell-armor	\N
983	groudon-primal	9997	50	0	f	https://pokeapi.co/api/v2/pokemon/10078/	2022-11-03 23:54:56	2022-11-03 23:54:56	desolate-land	\N
993	lopunny-mega	283	13	0	f	https://pokeapi.co/api/v2/pokemon/10088/	2022-11-03 23:54:57	2022-11-03 23:54:57	scrappy	\N
999	pikachu-original-cap	60	4	1	f	https://pokeapi.co/api/v2/pokemon/10094/	2022-11-03 23:54:58	2022-11-03 23:54:58	static	\N
1007	sandslash-alola	550	12	1	f	https://pokeapi.co/api/v2/pokemon/10102/	2022-11-03 23:54:59	2022-11-03 23:54:59	snow-cloak	\N
1019	exeggutor-alola	4156	109	1	f	https://pokeapi.co/api/v2/pokemon/10114/	2022-11-03 23:55:01	2022-11-03 23:55:01	frisk	\N
1033	lurantis-totem	580	15	1	f	https://pokeapi.co/api/v2/pokemon/10128/	2022-11-03 23:55:04	2022-11-03 23:55:04	leaf-guard	\N
1039	minior-indigo-meteor	400	3	0	f	https://pokeapi.co/api/v2/pokemon/10134/	2022-11-03 23:55:05	2022-11-03 23:55:05	shields-down	\N
1054	marowak-totem	980	17	1	f	https://pokeapi.co/api/v2/pokemon/10149/	2022-11-03 23:55:08	2022-11-03 23:55:08	cursed-bodylightning-rod	\N
1063	pikachu-starter	60	4	1	f	https://pokeapi.co/api/v2/pokemon/10158/	2022-11-03 23:55:09	2022-11-03 23:55:09	static	\N
1071	farfetchd-galar	420	8	1	f	https://pokeapi.co/api/v2/pokemon/10166/	2022-11-03 23:55:10	2022-11-03 23:55:10	steadfast	\N
1078	corsola-galar	5	6	1	f	https://pokeapi.co/api/v2/pokemon/10173/	2022-11-03 23:55:11	2022-11-03 23:55:11	weak-armor	\N
1088	cramorant-gorging	180	8	0	f	https://pokeapi.co/api/v2/pokemon/10183/	2022-11-03 23:55:12	2022-11-03 23:55:12	gulp-missile	\N
1097	zarude-dada	700	18	0	f	https://pokeapi.co/api/v2/pokemon/10192/	2022-11-03 23:55:13	2022-11-03 23:55:13	leaf-guard	\N
1112	garbodor-gmax	10000	210	1	f	https://pokeapi.co/api/v2/pokemon/10207/	2022-11-03 23:55:15	2022-11-03 23:55:15	stenchweak-armor	\N
1122	appletun-gmax	10000	240	1	f	https://pokeapi.co/api/v2/pokemon/10217/	2022-11-03 23:55:16	2022-11-03 23:55:16	ripengluttony	\N
1139	qwilfish-hisui	39	5	1	f	https://pokeapi.co/api/v2/pokemon/10234/	2022-11-03 23:55:19	2022-11-03 23:55:19	poison-pointswift-swim	\N
1141	samurott-hisui	582	15	1	f	https://pokeapi.co/api/v2/pokemon/10236/	2022-11-03 23:55:19	2022-11-03 23:55:19	torrent	\N
1142	lilligant-hisui	192	12	1	f	https://pokeapi.co/api/v2/pokemon/10237/	2022-11-03 23:55:19	2022-11-03 23:55:19	chlorophyllhustle	\N
1143	zorua-hisui	125	7	0	f	https://pokeapi.co/api/v2/pokemon/10238/	2022-11-03 23:55:19	2022-11-03 23:55:19	illusion	\N
1144	zoroark-hisui	730	16	0	f	https://pokeapi.co/api/v2/pokemon/10239/	2022-11-03 23:55:19	2022-11-03 23:55:19	illusion	\N
1145	braviary-hisui	434	17	1	f	https://pokeapi.co/api/v2/pokemon/10240/	2022-11-03 23:55:20	2022-11-03 23:55:20	keen-eyesheer-force	\N
1146	sliggoo-hisui	685	7	1	f	https://pokeapi.co/api/v2/pokemon/10241/	2022-11-03 23:55:20	2022-11-03 23:55:20	sap-sipperovercoat	\N
1147	goodra-hisui	3341	17	1	f	https://pokeapi.co/api/v2/pokemon/10242/	2022-11-03 23:55:20	2022-11-03 23:55:20	sap-sipperovercoat	\N
1148	avalugg-hisui	2624	14	1	f	https://pokeapi.co/api/v2/pokemon/10243/	2022-11-03 23:55:20	2022-11-03 23:55:20	strong-jawice-body	\N
1149	decidueye-hisui	370	16	1	f	https://pokeapi.co/api/v2/pokemon/10244/	2022-11-03 23:55:20	2022-11-03 23:55:20	overgrow	\N
1150	dialga-origin	8487	70	1	f	https://pokeapi.co/api/v2/pokemon/10245/	2022-11-03 23:55:21	2022-11-03 23:55:21	pressure	\N
1151	palkia-origin	6590	63	1	f	https://pokeapi.co/api/v2/pokemon/10246/	2022-11-03 23:55:21	2022-11-03 23:55:21	pressure	\N
1152	basculin-white-striped	180	10	1	f	https://pokeapi.co/api/v2/pokemon/10247/	2022-11-03 23:55:21	2022-11-03 23:55:21	rattledadaptability	\N
1153	basculegion-female	1100	30	1	f	https://pokeapi.co/api/v2/pokemon/10248/	2022-11-03 23:55:21	2022-11-03 23:55:21	rattledadaptability	\N
1154	enamorus-therian	480	16	0	f	https://pokeapi.co/api/v2/pokemon/10249/	2022-11-03 23:55:21	2022-11-03 23:55:21	overcoat	\N
2	ivysaur	130	10	1	f	https://pokeapi.co/api/v2/pokemon/2/	2022-11-03 23:52:02	2022-11-04 00:01:43	overgrow	https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/2.svg
\.


--
-- Name: pokemons_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.pokemons_id_seq', 1154, true);


--
-- Data for Name: schema_migrations; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.schema_migrations (version, inserted_at) FROM stdin;
20221101212100	2022-11-01 21:36:08
20221102222408	2022-11-02 22:25:51
20221103233257	2022-11-03 23:33:35
\.


--
-- Name: pokemons pokemons_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.pokemons
    ADD CONSTRAINT pokemons_pkey PRIMARY KEY (id);


--
-- Name: schema_migrations schema_migrations_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.schema_migrations
    ADD CONSTRAINT schema_migrations_pkey PRIMARY KEY (version);


--
-- Name: SCHEMA public; Type: ACL; Schema: -; Owner: postgres
--

GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

