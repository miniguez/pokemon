FROM elixir:1.12.3

# Create app directory.
RUN mkdir /app
WORKDIR /app

RUN apt-get update && apt-get install -y postgresql-client

# Install Hex package manager.
RUN mix local.hex --force
RUN mix local.rebar --force

# Install phoenix.
RUN mix archive.install hex phx_new

CMD ["/app/entrypoint.sh"]