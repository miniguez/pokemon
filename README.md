# pokemon api

## Requerimientos
* docker
* docker-compose
* make

## Opciones:

### Iniciar el proyecto
```
$ make bootstrap
```

### Acceder a la app
[localhost:3000](http://localhost:3000).

![](localhost.png "")

### Detener contenedores
```
$ make stop
```

### Arrancar proyecto
Una vez creados los contenedores (con el comando make bootstrap)
```
$ make start
```

### Logs de phoenix
```
$ make logs.phx
```

### Correr pruebas phoenix
```
$ make test
```

### Correr análisis de código phoenix
```
$ make credo
```

### Eliminar el proyecto
```
$ make remove
```

