import * as React from 'react';
import { DataGrid } from '@mui/x-data-grid';
import axios from "axios";
import {useState, useEffect} from "react";
import { FormControlLabel, IconButton } from '@material-ui/core';
import FavoriteBorderOutlinedIcon from '@mui/icons-material/FavoriteBorderOutlined';
import FavoriteIcon from '@mui/icons-material/Favorite';
import PreviewIcon from '@mui/icons-material/Preview';
import { blue } from '@material-ui/core/colors';
import Box from "@mui/material/Box";
import Button from '@mui/material/Button';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogTitle from '@mui/material/DialogTitle'

function DataTable() {
  const [pokemons, setPokemons] = useState([])
  const [totalWeight, setTotalWeight] = React.useState(0);
  const [totalHeight, setTotalHeight] = React.useState(0);
  const [open, setOpen] = React.useState(false);
  const [pokemon, setPokemon] = useState({})

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };


  useEffect( () => {
    fetchData()
    fetchInfo()
  }, []
  )

  const fetchData = async () => {
    const {data} = await axios.get("http://localhost:4000/api/pokemons")

    setPokemons(data)
  }

  const MatLike = ({ index }) => {    
    const handleLikeClick = async () => {        
      await axios.put("http://localhost:4000/api/pokemons/like/"+index); 
      fetchData();       
    }

    return <FormControlLabel
               control={
                   <IconButton color="secondary" aria-label="add an alarm" onClick={handleLikeClick} >
                       <FavoriteBorderOutlinedIcon style={{ color: blue[500] }} />
                   </IconButton>                   
               }
           />
  };

  const MatUnLike = ({ index }) => {    
    const handleLikeClick = async () => {        
      await axios.put("http://localhost:4000/api/pokemons/unlike/"+index);       
      fetchData();
    }

    return <FormControlLabel
               control={
                   <IconButton color="secondary" aria-label="add an alarm" onClick={handleLikeClick} >
                       <FavoriteIcon style={{ color: blue[500] }} />
                   </IconButton>                   
               }
           />
  };

  
  const fetchInfo = async () => {
    const {data} = await axios.get("http://localhost:4000/api/getinfo")    
    
    setTotalWeight(data.average_weight);
    setTotalHeight(data.average_height);
  }

  const MatView = ({ index }) => {

    const handleViewClick = async () => {
      const {data} = await axios.get("http://localhost:4000/api/pokemons/"+index);
      setPokemon(data.data);
      handleClickOpen();
    }


    return <FormControlLabel
               control={
                   <IconButton color="secondary" aria-label="add an alarm" onClick={handleViewClick} >
                       <PreviewIcon style={{ color: blue[500] }} />
                   </IconButton>                   
               }
           />
  };


  const columns = [
    { field: 'id', headerName: 'ID', width: 70 },
    { field: 'name', headerName: 'Nombre', width: 130 },  
    { field: 'chosen', headerName: 'chosen', width: 1 },
    {
      field: 'weight',
      headerName: 'Peso',
      type: 'number',
      width: 90,
    },
    {
      field: 'height',
      headerName: 'Altura',
      type: 'number',
      width: 90,
    },
    {
      field: 'abilities',
      headerName: 'Cantidad de habilidades',
      type: 'number',
      width: 170,
    },
    { field: 'name_abilities', headerName: 'Habilidades', with: 200 },
    {
      field: "like",
      headerName: "Favorito",
      sortable: false,
      width: 140,
      disableClickEventBubbling: true,
      renderCell: (params) => {        
        if(params.row.chosen)
          return (
            <div className="d-flex justify-content-between align-items-center" style={{ cursor: "pointer" }}>
                <MatUnLike index={params.row.id} />
            </div>              
          );
        else 
          return (
              <div className="d-flex justify-content-between align-items-center" style={{ cursor: "pointer" }}>
                  <MatLike index={params.row.id} />
              </div>              
          );
      }
    },
    {
      field: "actions",
      headerName: "Acciones",
      sortable: false,
      width: 140,
      disableClickEventBubbling: true,
      renderCell: (params) => {
          return (
              <div className="d-flex justify-content-between align-items-center" style={{ cursor: "pointer" }}>
                  <MatView index={params.row.id} />
              </div>              
          );
       }
    }
  ];
  
  const rows = pokemons;

  return (
    <div style={{ height: 650, width: '100%' }}>      
      <Box sx={{ padding: "10px", display: "flex" }}>Promedio general de peso : {totalWeight}</Box>
      <Box sx={{ padding: "10px", display: "flex" }}>Promedio general de altura : {totalHeight}</Box>    
      <Dialog
        open={open}
        onClose={handleClose}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle id="alert-dialog-title">
          {pokemon.name}
        </DialogTitle>
        <DialogContent>
        <img src={pokemon.url_image} alt="pokemon"/>  
        <Box sx={{ padding: "10px", display: "flex" }}>Peso: {pokemon.weight}</Box>
        <Box sx={{ padding: "10px", display: "flex" }}>Altura: {pokemon.height}</Box>
        <Box sx={{ padding: "10px", display: "flex" }}>Cantidad de habilidades: {pokemon.abilities}</Box>
        <Box sx={{ padding: "10px", display: "flex" }}>Habilidades: {pokemon.name_abilities}</Box>
        <Box sx={{ padding: "10px", display: "flex" }}>Url: {pokemon.url}</Box>                
        </DialogContent>
        <DialogActions>          
          <Button onClick={handleClose} autoFocus>
            Ok
          </Button>
        </DialogActions>
      </Dialog>

      <DataGrid
        rows={rows}
        columns={columns}
        pageSize={10}
        rowsPerPageOptions={[10]}        
        columnVisibilityModel={{
          id: false,
          chosen: false
        }}                   
        disableSelectionOnClick
      />
    </div>
  );
}

export default DataTable;